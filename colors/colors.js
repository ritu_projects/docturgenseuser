import React, {Component} from 'react';
var ReactNative = require('react-native');

module.exports = {
  COLOR: {
    lightBlue: '#6FB8EF',
    darkBlue: '#3A7CAF',
    blackClr: '#000000',
    whiteclr: '#FFFFFF',
  },
};
