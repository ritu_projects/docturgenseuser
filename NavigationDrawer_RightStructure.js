import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
  Text,
  ImageBackground,
  Platform,
} from 'react-native';
import React, {Component} from 'react';
import {
  createStackNavigator,
  createAppContainer,
  createSwitchNavigator,
  createDrawerNavigator,
} from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import Image from 'react-native-fast-image';
const {width, height} = Dimensions.get('window');
import normalize from 'react-native-normalize';
import Application_Constant from './screens/localization';

export default class NavigationDrawer_RightStructure extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'DoctUrgences',
      visibility: false,
    };
  }
  componentDidMount() {
    // AsyncStorage.getItem("title")
    //   .then(title => {
    //     console.log("Title is###"+title)
    //     this.setState({ title: title });
    //   })
  }
  //Top Navigation Header with Donute Button
  toggleDrawer = () => {
    this.props.navigationProps.toggleDrawer();
  };

  callLocation() {
    const { params = {} } = this.props.navigation.state;


    console.log('Toolbar Icon CLick  ' , 'Toolbar Icon Click ');
    params.handleSave()
    //this.props.navigationProps.navigation.setParams({ headerRight:'click' })
  }
  render() {
    return (
      //   <TouchableOpacity style={{ width:"30%", flexDirection: 'row', height: 50, justifyContent: "center", alignItems: "center",
      // }}
      // onPress={() => this.callLocation()}>

      //       <Image
      //         source={require('./assets/pin_icon.png')}
      //         style={{ width: 35, height: 35, }}
      //       />

      //   </TouchableOpacity>
      <LinearGradient
        // source={require('./assets/download.png')}

        style={{
          flexDirection: 'row',
          height: normalize(30),
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: Platform.OS == 'ios' ? normalize(0) : normalize(0),
          marginBottom: normalize(-5),
        }}
        // colors={['white', 'rgba(0,128,0,0.2)']}
        colors={['#e7eef1', 'rgba(203,229,216,255)']}>
        <TouchableOpacity
          style={{
            width: '15%',
            flexDirection: 'row',
            marginLeft: 10,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={this.toggleDrawer.bind(this)}>
          {/*Donute Button Image */}
          <Image
            source={require('./assets/drawer.png')}
            style={{width: 25, height: 25}}
          />
        </TouchableOpacity>
        <View
          style={{
            width: '70%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontFamily: 'Montserrat-Bold',
              color: '#2da9ff',
              fontSize: 22,
            }}>{Application_Constant.homeTitle} </Text>
        </View>
        <TouchableOpacity
          style={{
            width: '15%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 10,
          }}
          onPress={() => this.callLocation()}>
          <Image
            source={require('./assets/pin_icon.png')}
            style={{width: 25, height: 25}}
          />
        </TouchableOpacity>
      </LinearGradient>
    );
  }
}
