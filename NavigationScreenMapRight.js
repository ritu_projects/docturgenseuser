import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
  Text,
  Platform,
} from 'react-native';
import React, {Component} from 'react';

import {
  createStackNavigator,
  createAppContainer,
  createSwitchNavigator,
  createDrawerNavigator,
} from 'react-navigation';
import Image from 'react-native-fast-image';
import normalize from 'react-native-normalize';
import LinearGradient from 'react-native-linear-gradient';
import Application_Constant from './screens/localization';


export default class NavigationScreenMapRight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'DoctUrgences',
      visibility: false,
    };
  }
  componentDidMount() {}
  //Top Navigation Header with Donute Button
  toggleDrawer = () => {
    //Props to open/close the drawer
    this.props.navigationProps.toggleDrawer();
  };

  callLocation() {
    console.log('Toolbar Icon CLick Map  ' + 'Toolbar Icon Click Map ');

    this.props.navigationProps.navigate('MapExample');
  }
  render() {
     


    return (
      //   <TouchableOpacity style={{ width:"30%", flexDirection: 'row', height: 50, justifyContent: "center", alignItems: "center",
      // }}
      // onPress={() => this.callLocation()}>

      //       <Image
      //         source={require('./assets/pin_icon.png')}
      //         style={{ width: 35, height: 35, }}
      //       />

      //   </TouchableOpacity>

      <LinearGradient
        //  source={require('./assets/download.png')}

        style={{
          flexDirection: 'row',
          height: 30,
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: Platform.OS == 'ios' ? normalize(25) : normalize(0),
          marginBottom: normalize(-5),
        }}
        colors={['#e7eef1', 'rgba(203,229,216,255)']}>
        <TouchableOpacity
          style={{width: '10%', flexDirection: 'row', marginRight: 0}}
          onPress={this.toggleDrawer.bind(this)}>
          <Image
          source={require('./assets/drawer.png')}
          style={{width: 30, height: 30, marginRight: 10, marginLeft: 10}}
          />
        </TouchableOpacity>
        <View
          style={{

          width: '75%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontFamily: 'Poppins-Regular',
              color: '#25aff7',
              fontSize: 20,
            }}>
            {Application_Constant.homeTitle}
          </Text>
        </View>
        <TouchableOpacity
          style={{
            width: '15%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => this.callLocation()}>
          <Image
            source={require('./assets/pin_icon.png')}
            style={{width: 30, height: 30}}
          />
        </TouchableOpacity>
      </LinearGradient>
    );
  }
}
