import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Platform,
  header,
  DrawerLayoutAndroid
} from 'react-native';
import { createAppContainer } from "react-navigation";

import SplashScreen from './screens/SplashScrren'
import SplashScreenSecond from './screens/SplashScreenSecond'
import SplashScreenThird from './screens/SplashScreenThird'

import ViewPagerPage from './screens/ViewPagerPage'
import MapExample from './screens/MapExample'
import Inscription_Register from './screens/Inscription_Register'
import Connexion_Login from './screens/Connexion_Login'
import ForgotPassword_Screen from './screens/ForgotPassword_Screen'
import OtpVerfication from './screens/OtpVerfication'

import AfterLogin from './screens/AfterLogin'
import AfterLoginNew from './screens/AfterLoginNew'

import ProfileScreen from './screens/ProfileScreen'
import EditProfile from './screens/EditProfile'
import Premiers_Pas from './screens/Premiers_Pas'
import Premiers_Pas2 from './screens/Premiers_Pas2'
import Chat from './screens/Chat'

import Doctor_Information from './screens/Doctor_Information'

import NavigationDrawerStructure from './NavigationDrawerStructure'
import NavigationDrawer_RightStructure from './NavigationDrawer_RightStructure'

import NavigationScreenMapRight from './NavigationScreenMapRight'
import NavigationWhole from './NavigationWhole'


import SideBar from './screens/Sidebar'
import { createStackNavigator } from 'react-navigation-stack';
import { createSwitchNavigator } from 'react-navigation';
import { AppNavigator } from 'react-navigation';

import { createDrawerNavigator } from 'react-navigation-drawer';
import Colors from './colors/colors'

import String from './screens/localization'

import FAQ_LesQuestions from './screens/FAQ_LesQuestions'
import Webview from './screens/Webview'
import WebviewMention_legas from './screens/WebviewMention_legas'
import Image from 'react-native-fast-image';


const FirstActivity_StackNavigator3 = createStackNavigator({
  //All the screen from the First Option will be indexed here
  MapExample: {
    screen: MapExample,
    navigationOptions: ({ navigation }) => ({
      headerShown: false

      
    //   headerTransparent: true,
    //   //headerLeft: <NavigationDrawerStructure navigationProps={navigation} />, 
    //   header: <NavigationScreenMapRight navigationProps={navigation} />, 
    

    //   headerStyle: {
    //     backgroundColor: 'transparent',
        
    //  }
    }),

  },

  AfterLogin: {
    screen: AfterLogin,
    navigationOptions: ({ navigation }) => ({
    //  headerTitle:"DoctUrgences",

      headerTransparent: true,
      // headerLeft: <NavigationDrawerStructure navigationProps={navigation} />, 

      // headerRight: <NavigationDrawer_RightStructure navigationProps={navigation} />, 

    header: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: 'transparent',
       
        
         },

    }),
    
  },
  AfterLoginNew: {
    screen: AfterLoginNew,
  navigationOptions: ({ navigation }) => ({
   headerShown: false

  //     headerTransparent: true,
  //  header: <NavigationDrawer_RightStructure navigationProps={navigation} />, 

  //     headerStyle: {
  //       backgroundColor: 'transparent',
        



  //   },
  //   headerTitleStyle:
  //   {
  //     alignSelf:'center',
  //     color:'white',
  //     fontWeight:'bold',
  //     fontSize:18,
  //     marginLeft:20,
  //   }

     }),
    
  },
  ProfileScreen: {
    screen: ProfileScreen,
    navigationOptions: ({ navigation }) => ({
      title: String.myProfile,
      header: <NavigationWhole navigationProps={navigation} />,
      headerStyle: {
        justifyContent: "center",
        color:'black',

        alignItems: "center",
        textAlign: 'center',
        // fontFamily:Poppins-Bold,
        fontFamily: "Poppins-Medium",

        backgroundColor: Colors.whiteclr,
      },
      headerTintColor: Colors.blackClr,
      
    }),


  },

  EditProfile: {
    screen: EditProfile,
    navigationOptions: ({ navigation }) => ({
      title: String.myProfile,
      header: <NavigationWhole navigationProps={navigation} />,
      headerStyle: {
        justifyContent: "center",
        color:'black',

        alignItems: "center",
        textAlign: 'center',
        // fontFamily:Poppins-Bold,
        fontFamily: "Poppins-Medium",

        backgroundColor: Colors.whiteclr,
      },
      headerTintColor: Colors.blackClr,
    }),


  },

  Premiers_Pas: {
    screen: Premiers_Pas,
    navigationOptions: ({ navigation }) => ({
      title: String.premiers_pas,
      header: <NavigationWhole navigationProps={navigation} />,
      headerStyle: {
        justifyContent: "center",

        alignItems: "center",
        textAlign: 'center',
        // fontFamily:Poppins-Bold,
        fontFamily: "Poppins-Medium",

        backgroundColor: Colors.whiteclr,
      },

      headerTintColor: Colors.blackClr,
    }),
  },
    Premiers_Pas2: {
      screen: Premiers_Pas2,
      navigationOptions: ({ navigation }) => ({
        title: String.premiers_pas,
        header: <NavigationWhole navigationProps={navigation} />,
        headerStyle: {
          justifyContent: "center",
  
          alignItems: "center",
          textAlign: 'center',
          // fontFamily:Poppins-Bold,
          fontFamily: "Poppins-Medium",
  
          backgroundColor: 'transparent',
        },

        headerTintColor: Colors.blackClr,
      }),

  },
  FAQ_LesQuestions: {
    screen: FAQ_LesQuestions,
    navigationOptions: ({ navigation }) => ({
      title: 'Foire aux questions',
      header: <NavigationWhole navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: Colors.whiteclr,


      },
      headerTitleStyle: {
        color: 'black',
        alignSelf: 'center',
       
      },
    }),

  },
},

  {
    navigationOptions: {

    }
  
});

const Drer_StackNavigator33 = createStackNavigator({
  //All the screen from the First Option will be indexed here

  AfterLogin: {
    screen: AfterLogin


  },
});

const MainAppNavigator = createDrawerNavigator({
  
  // AfterLogin: {
  //   screen: AfterLogin,
  //   navigationOptions: {
  //     //Return Custom label with null in return.
  //     //It will hide the label from Navigation Drawer
  //     drawerLabel: "",
  //   },

  // },

  SplashScreen: {
    screen: SplashScreen,
    navigationOptions: {
      //Return Custom label with null in return.
      //It will hide the label from Navigation Drawer
      drawerLabel: "",
    },

  },
  SplashScreenSecond: {
    screen: SplashScreenSecond


  },

  SplashScreenThird: {
    screen: SplashScreenThird


  },

  ViewPagerPage: {
    screen: ViewPagerPage,
    navigationOptions:{
      drawerLockMode:'locked-closed'
    }


  },



  Inscription_Register: {
    screen: Inscription_Register,
    navigationOptions:{
      drawerLockMode:'locked-closed'
    }
  },
  Connexion_Login: {
    screen: Connexion_Login,
    navigationOptions:{
      drawerLockMode:'locked-closed'
    }
  },
  ForgotPassword_Screen:
  {
    screen: ForgotPassword_Screen,
    navigationOptions:{
      drawerLockMode:'locked-closed'
    }
  },
  OtpVerfication
  :
  {
    screen: OtpVerfication,
    navigationOptions:{
      drawerLockMode:'locked-closed'
    }
  },
  FirstActivity_StackNavigator3: {
    screen: FirstActivity_StackNavigator3
    
  },
  Doctor_Information: {
    screen: Doctor_Information
  },
  Chat: {
    screen: Chat
  },
  Webview: {
    screen: Webview
  },
  WebviewMention_legas: {
    screen: WebviewMention_legas
  },

},
  {
    // initialRouteName: "SplashScreen",
    // headerMode: "none"

    contentComponent: SideBar,
    drawerWidth: 300
  });
const AppNavigator3 = createAppContainer(MainAppNavigator)
export default AppNavigator3

