import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
  Text,
  Platform,
} from 'react-native';
import React, {Component} from 'react';
import {
  createStackNavigator,
  createAppContainer,
  createSwitchNavigator,
  createDrawerNavigator,
} from 'react-navigation';
import AppNavigator from './AppNavigator';
import SideBar from './screens/Sidebar';
//import afterlogin from './screens/AfterLogin';
import afterlogin from './screens/AfterLoginNew';
import {object} from 'prop-types';
import Application_Constant from './screens/localization';
import Image from 'react-native-fast-image';

export default class NavigationDrawerStructure extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'DoctUrgences',
      visibility: false,
    };
  }
  componentDidMount() {
    // AsyncStorage.getItem("title")
    //   .then(title => {
    //     console.log("Title is###"+title)
    //     this.setState({ title: title });
    //   })
  }
  //Top Navigation Header with Donute Button
  toggleDrawer = () => {
    //Props to open/close the drawer
    this.props.navigationProps.toggleDrawer();
  };

  callLocation() {
    console.log('Toolbar Icon CLick  ' + 'Toolbar Icon Click ');

    //this.props.navigationProps.navigate('AfterLogin');
  }
  render() {
    return (
      <TouchableOpacity
        style={{
          width: '30%',
          flexDirection: 'row',
          height: 50,
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: Platform.OS == 'ios' ? 30 : 1,
          backgroundColor: 'transparent',
          marginLeft: 20,
        }}
        onPress={this.toggleDrawer.bind(this)}>
        <Image
          source={require('./assets/drawer.png')}
          style={{width: 35, height: 35, marginRight: 10, marginLeft: 10}}
        />
      </TouchableOpacity>

      //        <View style={{ flexDirection: 'row', height: 50, justifyContent: "center", alignItems: "center", }}>
      //   <TouchableOpacity
      //     style={{ width: "10%", flexDirection: 'row', marginRight: 0, }}
      //     onPress={this.toggleDrawer.bind(this)}>
      //     <Image
      //       source={require('./assets/drawer.png')}
      //       style={{ width: 25, height: 25, marginRight: 10, marginLeft: 10, }}
      //     />

      //   </TouchableOpacity>
      //   <View
      //     style={{ width: "85%", flexDirection: 'row', justifyContent: 'center', alignItems: "center" }}>

      //     <Text
      //       style={{
      //         textAlign: "center",
      //         fontFamily: "Poppins-Bold",
      //       }}>DoctUrgences</Text>

      //   </View>

      // </View>
    );
  }
}
