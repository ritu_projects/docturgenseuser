import React, {Component} from 'react';
var ReactNative = require('react-native');
import {View, Text, StyleSheet, Dimensions, Platform} from 'react-native';

const win = Dimensions.get('window');
import MapView from 'react-native-maps';
import Colors from '../colors/colors';
import normalize from 'react-native-normalize';


export default {
  map_bg: {
    position: 'absolute',
    top: Platform.OS == 'ios' ? normalize(55) : normalize(27),
    left: 0,
    right: 0,
    bottom: 0,
  },

  crd_view: {
    position: 'absolute',

    bottom:
    Platform.OS == 'ios'
      ? normalize(30, 'height')
      : normalize(20, 'height'), 
         height: 80,
    width: '100%',
    justifyContent: 'center',
    padding: 10,

    flexDirection: 'column',
  },
  crd_view2: {
    position: 'absolute',

    bottom: 0,
    height: 100,
    width: '100%',
    justifyContent: 'center',
    padding: 50,
  },
  containerWhite: {
    flex: 1,
    flexDirection: 'column',
    // backgroundColor: 'transparent',
  },

  cardStle: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 15,
    bottom: 2,
  },
  cardItemStle: {
    width: '100%',
    marginTop: 5,
    marginBottom: 5,
    flexDirection: 'column',
  },
  cardThumbhnail_Stle: {
    width: 20,
    height: 20,
    marginTop: 10,
  },

  circle: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: 'red',
  },
  pinText: {
    color: Colors.COLOR.darkBlue,
    textAlign: 'center',
    fontSize: 12,
    marginBottom: 10,
    fontFamily: 'Poppins-Bold',
  },
};
