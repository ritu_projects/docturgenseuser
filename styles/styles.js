import React, {Component} from 'react';
var ReactNative = require('react-native');
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  Platform,
} from 'react-native';
const win = Dimensions.get('window');
import Colors from '../colors/colors';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default {
  splashbg_img: {
    flex: 1,
    flexDirection: 'column',
    marginTop: Platform.OS == 'ios' ? 0 : 0,
  },
  splashbg_imgg: {
    width: width,
    height: height,
    flexDirection: 'column',
    marginTop: Platform.OS == 'ios' ? 0 : 0,
  },

  splashbg_img2: {
    marginTop: height / 11.5,
    width: 250,
    height: 302,
  },
  viewpagerbg_img: {
  //  height: '100%',
  flex:1,
  },
  viewpager_txt_new: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center', // <-- the magic
    fontSize: 25,
    marginTop: 30,
    fontFamily: 'Montserrat-Bold',
    color: '#49b7f5',
  },
  viewpager_secondtxt_new: {
    textAlign: 'center',
    fontSize: 25,
    padding: 7,
    fontFamily: 'Montserrat-Bold',
    color: '#49b7f5',
    marginLeft: 10,
    marginRight: 10,
  },
  viewpager_txt: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center', // <-- the magic
    fontSize: 20,
    marginTop: height / 1.8,
    fontFamily: 'Poppins-Medium',
    color: '#52A6F2',
  },

  viewpager_secondtxt: {
    textAlign: 'center',
    fontSize: 16,
    padding: 10,
    fontFamily: 'Poppins-Medium',
    color: Colors.COLOR.blackClr,
    marginLeft: 10,
    marginRight: 10,
  },
  viewpager_thirdtxt: {
    textAlign: 'center',
    fontSize: 18,
    padding: 10,
    fontFamily: 'Poppins-Bold',
    fontFamily: 'Poppins-Medium',
    color: Colors.COLOR.lightBlue,
  },

  viewpager_backarrow: {
    alignContent: 'flex-end',
    marginBottom: 5,
  },
  viewpager_forwordarrow: {
    alignItems: 'center',
  },

  bg: {
    width: 30,
    height: 30,
    backgroundColor: Colors.COLOR.darkBlue,
  },
};
