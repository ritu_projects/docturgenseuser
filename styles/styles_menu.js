import React, {Component} from 'react';
var ReactNative = require('react-native');
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native';

const win = Dimensions.get('window');
import MapView from 'react-native-maps';

export default {
  headerWhite: {
    width: width,
    height: 50,
    backgroundColor: '#3f4d67',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {width: 5, height: 4},
    shadowOpacity: 2,
    shadowRadius: 2,
    elevation: 15,
    alignItems: 'center',
  },

  header: {
    height: 50,
    flexDirection: 'row',
    marginLeft: 100,
    shadowColor: '#000',
    shadowOffset: {width: 5, height: 4},
    shadowOpacity: 2,
    shadowRadius: 2,
    elevation: 15,
    alignItems: 'center',
  },
  container: {
    paddingTop: 20,
    flex: 1,
  },
  navItemStyle: {
    padding: 10,
  },
  navSectionStyle: {
    backgroundColor: 'lightgrey',
  },
  sectionHeadingStyle: {
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  footerContainer: {
    padding: 20,
    backgroundColor: 'lightgrey',
  },
};
