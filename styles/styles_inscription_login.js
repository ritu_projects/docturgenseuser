import React, {Component} from 'react';
var ReactNative = require('react-native');
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native';
const win = Dimensions.get('window');
import Colors from '../colors/colors';

export default {
  containerWhite: {
    flex: 1,
    flexDirection: 'column',
  },
  crd_view: {
    flexDirection: 'column',
    borderRadius: 35,
    left: 20,
    right: 20,
    paddingLeft: 25,
    paddingRight: 25,
  },
  backgroundImage: {
    flex: 1,
    flexDirection: 'column',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  scrollView: {
    backgroundColor: 'pink',
    marginHorizontal: 20,
  },

  bottom_btn_view: {
    position: 'absolute',

    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    bottom: 5,
  },
  crd_txt_input: {
    width: '100%',
    height: 40,
    borderBottomColor: Colors.COLOR.lightBlue,
    borderStyle: 'solid',
    padding: 10,
    fontFamily: 'Poppins-Regular',
  },
  img_cross_vew: {
    width: 40,
    height: 40,
    right: 5,
    position: 'absolute',
    top: 5,
    alignItems:'center',
    justifyContent:'center'
  },
  img_cross_vew2: {
    width: 30,
    height: 30,
   
  },
};
