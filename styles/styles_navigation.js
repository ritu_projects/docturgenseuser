import React, {Component} from 'react';
var ReactNative = require('react-native');
import {View, Text, StyleSheet, Dimensions, Platform} from 'react-native';

const win = Dimensions.get('window');
import MapView from 'react-native-maps';

export default {
  container: {
    flex: 1,
    marginTop: Platform.OS == 'ios' ? 0 : 0,
  },

  navItemStyle: {
    padding: 10,
  },
  navSectionStyle: {
    backgroundColor: 'lightgrey',
  },
  sectionHeadingStyle: {
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  footerContainer: {
    padding: 20,
    backgroundColor: 'lightgrey',
  },
};
