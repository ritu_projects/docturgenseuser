import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Button,
  Profile,
  FlatList,
  MenuItem,
  ScrollView,
  ActivityIndicator,
  Linking,
  Dimensions,
  TouchableHighlight,
  Platform,
} from 'react-native';
import styles from '../styles/styles_profile';
import {Card} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import {TextInput} from 'react-native-gesture-handler';
import Colors from '../colors/colors';
import PopupDialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import Image from 'react-native-fast-image';
import Application_Constant from './localization';

export default class Doctor_Information extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctor_name: 'Nom Complet',
      doctor_telephone: 'Numéro de téléphone médecin',
      animating: false,
      lodingDialog: false,
      phonenumber: '+33 998877667',
    };
  }
  componentDidMount() {
    // this.setState({
    //     animating: true,
    //     lodingDialog: true,
    //   });
  }

  callNumber = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            // width: "100%", height: 60, backgroundColor: 'white', flexDirection: 'row', shadowColor: '#000', shadowOffset: { width: 5, height: 4 },
            // shadowOpacity: 1, shadowRadius: 1, elevation: 10, alignItems: 'center'
            width: '100%',
            height: 60,
            backgroundColor: 'white',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: Platform.OS == 'ios' ? 0 : 0,
          }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('EditProfile')}
            style={{
              marginLeft: 10,
              justifyContent: 'center',
              alignItems: 'center',
              width: 40,
            }}>
            <Image
              source={require('../assets/back.png')}
              style={{
                width: 30,
                height: 30,
                borderRadius: 100 / 2,
                marginTop: 10,
                padding: 5,
              }}
            />
          </TouchableOpacity>

          <View
            style={{
              height: 50,
              flexDirection: 'row',
              width: '100%',
              shadowColor: '#000',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 17,
                fontFamily: 'Poppins-Regular',
                color: 'black',
                textAlign: 'center',
                marginTop: 10,
              }}>
              Renseigner mon medecin
            </Text>
          </View>
        </View>

        <ScrollView>
          <Card
            style={{
              padding: 15,
              marginTop: 20,
              marginLeft: 15,
              marginRight: 15,
              borderRadius: 15,
            }}>
            <Text
              style={{
                fontSize: 13,
                fontFamily: 'Poppins-Medium',
              }}>
              {this.state.doctor_name}
            </Text>
          </Card>
          <Card
            style={{
              padding: 15,
              marginTop: 20,
              marginLeft: 15,
              marginRight: 15,
              borderRadius: 15,
            }}>
            <Text
              style={{
                fontSize: 13,
                fontFamily: 'Poppins-Medium',
              }}>
              {this.state.doctor_telephone}
            </Text>
          </Card>
        </ScrollView>
        <TouchableOpacity
          style={{
            bottom: 0,
            height: 70,
          }}
          onPress={this.callNumber}>
          <LinearGradient
            style={{
              flexDirection: 'row',
              backgroundColor: 'white',
              borderTopLeftRadius: 27,
              borderTopRightRadius: 27,
              padding: 20,
              alignItems: 'center',
            }}
            colors={['#7CB5F8', '#4C94E6']}
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}>
            <TouchableOpacity
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginRight: 5,
                marginLeft: 5,
                paddingLeft: 10,
              }}
              onPress={this.callNumber}>
              <Image
                style={{width: 40, height: 40}}
                source={require('../assets/call_icon.png')}
              />
              <Text
                style={{
                  width: '100%',
                  fontFamily: 'Poppins-Regular',
                  paddingLeft: 0,
                  marginRight: 5,
                  color: 'white',
                  flex: 1,
                  fontSize: 13,
                  paddingRight: 5,
                  marginLeft: 15,
                }}>
                Je suis en situation d’urgence{' '}
              </Text>
            </TouchableOpacity>
          </LinearGradient>
        </TouchableOpacity>
        <PopupDialog
          onHardwareBackPress={() => {
            this.setState({lodingDialog: false});
          }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({slideFrom: 'bottom'})}>
          <DialogContent>
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[
                  {
                    height: 10,
                    marginBottom: 10,
                    marginTop: 30,
                    marginLeft: 20,
                    marginRight: 20,
                  },
                ]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>
      </View>
    );
  }
}
