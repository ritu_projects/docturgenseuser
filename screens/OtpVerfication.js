import React, {Component} from 'react';
import {
  StyleSheet,
  StatusBar,
  View,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Linking,
  AsyncStorage,
  Platform,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import Styles from '../styles/styles_inscription_login';
import {Card, CardItem, Icon, Thumbnail, Row} from 'native-base';
import Colors from '../colors/colors';
import LinearGradient from 'react-native-linear-gradient';
import PopupDialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import axios from 'axios';
import Application_Constant from './localization';
import Image from 'react-native-fast-image';
const STATUS_BAR_THEME = Platform.OS === 'ios' ? 'dark-content' : 'light-content';

export default class OtpVerfication extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      otpErr: '',
      enter_otp: Application_Constant.enter_otp,
      currentlatitude: '',
      currentlongitude: '',
      userId: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('opt').then(opt => {
      AsyncStorage.getItem('opt').then(opt => {
        AsyncStorage.getItem('useridd').then(useridd => {
          console.log('OTP Verification   ' + opt);
          console.log('OTP USerid   ' + useridd);

          this.setState({
            otp: opt,
            userId: useridd,
          });
        });
      });
    });
  }
  callNumber = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };
  moveToLogin = () => {
    this.props.navigation.navigate('Connexion_Login');
  };
  userOTPVerificationApi() {
    axios
      .post(
        Application_Constant.BaseUrl + 'api/otpVerificationuser',
        {
          user_id: this.state.userId,
          otp: this.state.otp,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            //other header fields
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('Data is  ' + data);
        var res_data = data.error;
        console.log('Data Res is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
          alert(res_data_msg);
        } else if (res_data == 'false') {
          // var usersDetailss = JSON.stringify(data.usersDetails);
          // var userid = JSON.stringify(data.usersDetails.id)
          // console.log("Response User Id $$$" + userid)
          // var token = JSON.stringify(data.token)
          // console.log("Response User token $$$" + token)

          // AsyncStorage.setItem("userid", userid);
          // AsyncStorage.setItem("usersDetails", usersDetailss);
          // AsyncStorage.setItem("token", token);

          // // this.storeItem("uservalue", user_value)
          this.setState({
            animating: false,
            lodingDialog: false,
          });

          this.props.navigation.navigate('Connexion_Login');
        }
      });
  }
  validation() {
    var isValidate = 0;

    if (this.state.otp != '') {
      isValidate += 1;
    } else {
      isValidate -= 1;
      this.setState({
        otpErr: Application_Constant.enter_valid_email,
      });
    }

    if (isValidate == 1) {
      console.log(
        'Use Login::::' +
          this.state.AddresssMail +
          '==' +
          this.state.ModdePassword,
      );
      // this.userLoginApi();
      // this.setState({
      //     animating: true,
      //     lodingDialog: true,

      // });
      this.userOTPVerificationApi();
    } else {
      // alert(strings.Pleasefillallthefields)
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  render() {
    return (
      <SafeAreaView
       style={{flex: 1, marginTop: Platform.OS == 'ios' ? 0 : 0}}>
           {Platform.OS == 'ios' ?
         
         <StatusBar
            translucent={true}
            backgroundColor={'#324048'}
            barStyle={STATUS_BAR_THEME} />
          :
          null
        }
        <ScrollView style={{flex: 1}}>
          <View>
            <Image
              source={require('../assets/login_bg.png')}
              style={Styles.backgroundImage}>
              <View style={{width: '100%', marginTop: 50}}>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    paddingLeft: 13,
                    paddingRight: 13,
                    paddingTop: 10,
                    paddingBottom: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Poppins-Bold',
                      textAlign: 'center',
                      color: Colors.COLOR.whiteclr,
                      fontSize: 18,
                    }}>
                    {Application_Constant.verify_otp}
                  </Text>

                  <Text
                    style={{
                      fontFamily: 'Poppins-Medium',
                      textAlign: 'center',
                      color: Colors.COLOR.whiteclr,
                      fontSize: 16,
                      marginTop: 10,
                    }}
                  />
                  <Card
                    style={{
                      width: '100%',
                      marginTop: 40,
                      borderRadius: 40,
                      padding: 25,
                    }}>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontFamily: 'Poppins-Bold',
                      }}>
                      {' '}
                      {this.state.enter_otp}
                    </Text>

                    <TextInput
                      style={{
                        fontFamily: 'Poppins-Regular',
                        padding: 10,
                        height: 40,
                        marginTop: 20,
                      }}
                      editable
                      underlineColorAndroid={Colors.COLOR.lightBlue}
                      placeholder="123456"
                      keyboardType="phone-pad"
                      onChangeText={otp => this.setState({otp, otpErr: ''})}>
                      {this.state.otp}
                    </TextInput>

                    {!!this.state.otpErr && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.otpErr}
                      </Text>
                    )}
                  </Card>
                  <LinearGradient
                    style={{
                      flexDirection: 'row',
                      height: 50,
                      marginRight: 10,
                      marginLeft: 10,
                      borderRadius: 13,
                      marginTop: 70,
                      marginBottom: 5,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    colors={['#4C94E6', '#7CB5F8']}>
                    <TouchableOpacity
                      onPress={() => this.validation()}
                      style={Styles.bottom_btn_view}>
                      <Text
                        style={{
                          fontSize: 19,
                          color: Colors.COLOR.whiteclr,
                          fontFamily: 'Poppins-Medium',
                          textAlign: 'center',
                          fontSize: 18,
                        }}>{Application_Constant.continue}</Text>
                    </TouchableOpacity>
                  </LinearGradient>
                </View>
              </View>
            </Image>
            <TouchableOpacity
              style={Styles.img_cross_vew}
              onPress={() => this.moveToLogin()}>
              <Image
                style={Styles.img_cross_vew}
                source={require('../assets/cross_icon.png')}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>

        {/* <TouchableOpacity
                    style={{
                        bottom: 0,
                        height: 70,
                    }}
                    onPress={this.callNumber}
                >
                    <LinearGradient
                        style={{
                            flexDirection: 'row',
                            backgroundColor: 'white',
                            borderTopLeftRadius: 27,
                            borderTopRightRadius: 27,
                            padding: 20,
                            alignItems: "center",

                        }}
                        colors={['#7CB5F8', '#4C94E6']}
                        start={{ x: 0, y: 1 }}
                        end={{ x: 1, y: 1 }}>
                        <TouchableOpacity style={{ width: '100%', flexDirection: 'row', alignItems: "center", marginRight: 5, marginLeft: 5, paddingLeft: 10, }}
                            onPress={this.callNumber}>

                            <Image style={{ width: 40, height: 40, }}
                                source={require('../assets/call_icon.png')} ></Image>
                            <Text style={{ width: "100%", fontFamily: "Poppins-Regular", paddingLeft: 0, marginRight: 5, color: "white", flex: 1, fontSize: 13, paddingRight: 5, marginLeft: 15 }}>Je suis en situation d’urgence </Text>
                        </TouchableOpacity>
                    </LinearGradient>

                </TouchableOpacity> */}
        <PopupDialog
          onHardwareBackPress={() => {
            this.setState({lodingDialog: false});
          }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({slideFrom: 'bottom'})}>
          <DialogContent>
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[
                  {
                    height: 10,
                    marginBottom: 10,
                    marginTop: 30,
                    marginLeft: 20,
                    marginRight: 20,
                  },
                ]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>
      </SafeAreaView>
    );
  }
}
