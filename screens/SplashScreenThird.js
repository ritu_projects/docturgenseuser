import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import Application_Constant from './localization';
import Image from 'react-native-fast-image';
import Styles from '../styles/styles';

export default class SplashScreenThird extends Component {
  constructor(props) {
    super(props);
  }

  performTimeConsumingTask = async () => {
    return new Promise(resolve =>
      setTimeout(() => {
        resolve('result');
      }, 2000),
    );
  };

  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    const data = await this.performTimeConsumingTask();

    if (data !== null) {
      this.props.navigation.navigate('ViewPagerPage');
    }
  }

  render() {
    return (
      <View style={Styles.splashbg_img}>
        <Image
          source={require('../assets/splash_three_screen.png')}
          style={Styles.splashbg_imgg}>
          {/* <Text>Hello World</Text> */}
        </Image>
      </View>
    );
  }
}
