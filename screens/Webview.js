import React, {Component} from 'react';
import {
  Alert,
  Text,
  AsyncStorage,
  ActivityIndicator,
  BackHandler,
  View,
  StyleSheet,
  Dimensions,
  Platform,
  TouchableOpacity,
} from 'react-native';
import {WebView} from 'react-native-webview';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import Image from 'react-native-fast-image';

class Webview extends React.Component {
  constructor(props) {
    super(props);
    this.webView = null;
    this.state = {
      userId: '',
      orderid: '',
    };
  }
  componentDidMount() {
    this.checkCondition();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  componentWillReceiveProps() {
    this.checkCondition();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  checkCondition() {
    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID Web$$$', userid);
      var useridd = JSON.parse(userid);
      console.log('Usec bbr ID@@@' + useridd);
      this.setState({
        userId: userid,
        session: '',
      });
    });
  }
  componentWillUnmount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  handleBackPress = () => {
    // console.log("User ID Web back$$$" , this.state.userId)

    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID Web Asyn$$$', userid);

      if (userid == 'null') {
        this.props.navigation.navigate('MapExample');
      } else {
        if (userid != '' && userid != null && userid != 'null') {
          // this.props.navigation.navigate('AfterLogin')
          this.props.navigation.navigate('AfterLoginNew');
        } else {
          this.props.navigation.navigate('MapExample');
        }
      }
    });

    //   if(this.state.userId == null)
    //   {
    //     this.props.navigation.navigate('MapExample')

    //   }
    //   else
    //   {
    //     if (this.state.userId!= '' && this.state.userId!= null && this.state.userId!= 'null') {
    //         this.props.navigation.navigate('AfterLogin')

    //     }
    //     else
    //     {
    //         this.props.navigation.navigate('MapExample')

    //     }

    //   }

    return true;
  };

  LoadingIndicatorView() {
    return (
      <View style={{flex: 1}}>
        <ActivityIndicator
          color="#009b88"
          size="large"
          style={styles.ActivityIndicatorStyle}
        />
      </View>
    );
  }
  render() {
    return (
      <View style={styles.container}>
          <View
          style={{
            flexDirection: 'row',
            height: 50,
            justifyContent:'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{ width: 30,height: 30,
            flexDirection: 'row', marginRight: 0, 
          position:'absolute',left: 10, }}
            onPress={() => this.handleBackPress()}>
            <Image
              source={require('../assets/back.png')}
              style={{ width: 25, height: 25, marginRight: 10, marginLeft: 0 }}
            />
          </TouchableOpacity>
       
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'Montserrat-Bold',
                color: '#2da9ff',
                fontSize: 22,
                textTransform: 'uppercase',

              }}>
              Docturgence
            </Text>
            </View>

        <WebView
          source={{
            uri: 'https://www.docturgences.com/how-it-work',
          }}
          renderLoading={this.LoadingIndicatorView}
          startInLoadingState={true}
          // onMessage={(event) => this.handleMessage(event)}
          //onNavigationStateChange={(event) => this.handleNavigation(event)}
          javaScriptEnabled={true}
        />
      </View>
    );
  }
}
export default Webview;
const styles = StyleSheet.create({
  container: {
    height: height,
    paddingBottom: 20,
    marginTop: Platform.OS == 'ios' ? 30 : 0,
  },
  ActivityIndicatorStyle: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    paddingTop: 40,
    paddingBottom: 10,
    backgroundColor: '#0c084c',
  },
  title: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
