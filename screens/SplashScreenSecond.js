import React, {Component} from 'react';
import {StyleSheet, View, Text, Platform} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import Application_Constant from './localization';
import Image from 'react-native-fast-image';
import Styles from '../styles/styles';
import SystemSetting from 'react-native-system-setting'


export default class SplashScreenSecond extends Component {
  constructor(props) {
    super(props);
  }

  performTimeConsumingTask = async () => {
    return new Promise(resolve =>
      setTimeout(() => {
        resolve('result');
      }, 2000),
    );
  };

  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    const data = await this.performTimeConsumingTask();

    if (data !== null) {
      this.props.navigation.navigate('SplashScreenThird');
    }
  }

  render() {
    return (
      <View style={Styles.splashbg_img}>
        <Image
          source={require('../assets/splash_two_screen.png')}
          style={{width: '100%', height: '100%'}}
          resizeMode="stretch">
          {/* <Text>Hello Groofl</Text> */}
        </Image>
      </View>
    );
  }
}
