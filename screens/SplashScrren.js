import React, { Component } from 'react';
import { StyleSheet, View, StatusBar, Dimensions, AsyncStorage, ActivityIndicator, Platform, ImageBackground, } from 'react-native'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import PopupDialog, {
  DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import axios from 'axios';
import Application_Constant from './localization'

import Styles from '../styles/styles';
import { EventRegister } from 'react-native-event-listeners'
import Image from 'react-native-fast-image'
const { width, height } = Dimensions.get("window");
import SystemSetting from 'react-native-system-setting'
import { call } from 'react-native-reanimated';
import { SafeAreaView } from 'react-native-safe-area-context';

const STATUS_BAR_THEME = Platform.OS === 'ios' ? 'dark-content' : 'light-content';



export default class SplashScrren extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentlatitude: '',
      currentlongitude: '',
      userId: '',
      token :'',
      animating: false,
      lodingDialog: false,

    }
  }
  componentDidUpdate(prevProps) {
    console.log('componentWillReceiveProps ', prevProps)
  }
  //   performTimeConsumingTask = async () => {
  //     return new Promise((resolve) =>
  //       setTimeout(
  //         () => { resolve('result') },
  //         2000
  //       )
  //     )
  //   }


  //   AsyncStorage.getItem("userid")
  //     .then(userid => {
  //       console.log("User ID$$$"+userid)

  //       var useridd = JSON.parse(userid);
  //       console.log("User ID^^^"+useridd)

  //       this.setState({ 
  //         userId: userid,
  //       });
  //     })



  // }
  //   async componentDidMount() {
  //     // Preload data from an external API
  //     // Preload data using AsyncStorage
  //     const data = await this.performTimeConsumingTask();

  //     console.log("User ID@@@"+this.state.userId)

  //       if(this.state.userId=='' || this.state.userId==null)
  //       {
  //         this.props.navigation.navigate('ViewPagerPage');
  //         // this.props.navigation.navigate('OtpVerfication');
  //     }
  //       else
  //       {
  //         this.props.navigation.navigate('AfterLogin');

  //       }

  //   }
  componentDidMount() {
    AsyncStorage.getItem("userid")
    .then(userid => {
      console.log("User ID$$$" + userid)

      var useridd = JSON.parse(userid);
      console.log("User ID^^^" + useridd)

      this.setState({
        userId: userid,
      });
    })
    //alert('New')
    SystemSetting.isLocationEnabled().then((enable) => {
      console.log('Current location is enable ' + enable);
      const state = enable ? 'On' : 'Off';
      console.log('Current location is ' + state);
      if (enable === true) {
   


                this.checkCondition()

      }
      else {
        console.log('Current location is enable ' + enable);


        if (Platform.OS == 'android') {



          SystemSetting.switchLocation(() => {
            console.log('switch location successfully');

            this.componentDidMount()
          })
        }

        else if (Platform.OS == 'ios') {
          this.checkCondition()
        }
      }
    })





  }
  checkCondition = () => {
  

    if (this.state.userId == '' || this.state.userId == null) {

      Application_Constant.setLanguage('fr');

      this.props.navigation.navigate('ViewPagerPage');


    }
    else
    {
      this.callAPI()
    }
  }

  callAPI = () => {


    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);

      var useridd = JSON.parse(userid);
      console.log('User ID^^^' + useridd);

      this.setState({
        userId: useridd,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      console.log('User Token^^^' + tokenn);

      this.setState({
        token: tokenn,
      });

      console.log('User Token%%%' + this.state.token);
  
      axios
        .post(
          Application_Constant.BaseUrl + 'api/getProfile',
          {
            user_id: this.state.userId,
          },
          {
            headers: {
              'user-id': this.state.userId,
              token: this.state.token,
              'Content-Type': 'application/json',

              //other header fields
            },
          },
        )
        .then(response => response.data)
        .then(data => {
          console.log(
            'Data GetProfile inside Profile is  ' + JSON.stringify(data),
          );
          var res_data = data.error;
          console.log('Data Res is  ' + res_data);

          console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            this.setState({
              animating: false,
              lodingDialog: false,
            });
            this.props.navigation.navigate('ViewPagerPage');


          } else if (res_data == 'false') {
            var lang = data.usersDetails.lang;
            Application_Constant.setLanguage(lang);


            this.setState({
              animating: false,
              lodingDialog: false,
            
            });
            this.props.navigation.navigate('AfterLoginNew');

          }
        });
    });
    this.listener = EventRegister.addEventListener('myCustomEvent', (data) => {
      console.log("Splash Data ", data)
      this.props.navigation.navigate('Chat', {
        appointment_id: data.appointment_id,
        userIdd_receiverId: data.userIdd_receiverId,

      })

    })

  }

  render() {
    return (
      <View
        style={Styles.splashbg_img}>

        {Platform.OS == 'ios' ?
          <StatusBar
            translucent={true}
            backgroundColor={'#324048'}
            barStyle={STATUS_BAR_THEME} />
          :
          null
        }


        {Platform.OS == 'android' ?
          <ImageBackground
            source={require("../assets/splash_screen_2.png")}

            style={{ width: width, height: '100%', }}>
          </ImageBackground>

          :
          <Image
            source={require("../assets/splash_screen_2.png")}

            style={{ width: width, height: '100%', }}>
          </Image>



        }




        <PopupDialog
          onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
          <DialogContent>
            <View style={{ alignItems: 'center', }}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>
      </View>
    );
  }
}