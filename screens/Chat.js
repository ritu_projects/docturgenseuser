import React from 'react';
import {
  StyleSheet,
  StatusBar,
  TextInput,
  FlatList,
  ActivityIndicator,
  Keyboard,
  View,
  ScrollView,
  Text,
  Button,
  Dimensions,
  TouchableOpacity,
  Linking,
  AsyncStorage,
  Platform,
  BackHandler
} from 'react-native';

import PopupDialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import axios from 'axios';
import Application_Constant from './localization';

import Image from 'react-native-fast-image';

let appointment_id, userIdd_receiverId;

export default class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deatailDialog: false,
      cancelDialog: false,
      chatuser: [],
      disabled: false,
      textmsgText: '',
      appointment_id: '',
      userId: '',
      order_id: '',
      token: '',
      textmsgTextErr: '',
      number: 5,
      animating: false,
      animating2: false,
    };
    this.index = 0;
    const {navigation} = this.props;
    appointment_id = navigation.getParam('appointment_id', '');
    userIdd_receiverId = navigation.getParam('userIdd_receiverId', '');
    console.log('Appointment ID Chat   ' + appointment_id);
    console.log('Appointment Receiver ID Details   ' + userIdd_receiverId);
  }

  imageLoadingStart(val) {
    this.setState({
      animating: val,
    });
  }
  imageLoadingStart2(val) {
    this.setState({
      animating2: false,
    });
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    // this.getChat();
    this.interval = setInterval(() => this.getChat(), 2000);

    console.log('select id ::::' + this.props.appointment_id);
  }
  // componentDidUpdate(){
  //     this.getChat();

  // }
  componentWillUnmount() {

    clearInterval(this.interval);
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);

  }

 
handleBackButtonClick() {
  this.props.navigation.navigate('AfterLoginNew')
    //this.props.navigation.navigate('AfterLoginNew')
 // return true;
}
  getChat() {
    // alert("Time out")

    AsyncStorage.getItem('userid').then(userid => {
      //  console.log("User ID$$$" + userid)

      var useridd = JSON.parse(userid);
      //  console.log("User ID^^^" + useridd)

      this.setState({
        userId: useridd,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      //   console.log("User Token^^^" + tokenn)

      this.setState({
        token: tokenn,
      });

      //   console.log("User Token%%%" + this.state.token)
      this.setState({
        animating: true,
        lodingDialog: true,
      });
      axios
        .post(
          Application_Constant.BaseUrl + 'api/getAppointmentchat',
          {
            appointment_id: appointment_id,
          },
          {
            headers: {
              'user-id': this.state.userId,
              token: this.state.token,
              'Content-Type': 'application/json',

              //other header fields
            },
          },
        )
        .then(response => response.data)
        .then(data => {
          // console.log("Data is getAppointmentchat  " +JSON.stringify(data));
          var res_data = data.error;
          //   console.log("Data Res is getAppointmentchat " + res_data);

          //  console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            //  console.log("Response True is  " + res_data_msg);

            var res_data_msg = data.errorMessage;
            //  console.log("Response True is  " + res_data_msg);

            this.setState({
              animating: false,
              lodingDialog: false,
              chatuser: [],
            });
            //alert(res_data_msg)
          } else if (res_data == 'false') {
            this.setState({
              chatuser: [],
            });
            this.setState({
              animating: false,
              lodingDialog: false,
              chatuser: data.record,
            });
          }
        });
    });
  }

  validation() {
    Keyboard.dismiss;

    var isValidate = 0;
    if (this.state.textmsgText != '') {
      isValidate += 1;
    } else {
      isValidate -= 1;
      this.setState({
        textmsgTextErr: Application_Constant.ShouldemptyText,
      });
    }

    if (isValidate == 1) {
      this.userSendChat();
    }
  }

  userSendChat() {
    console.log('Chat AppointmentId   ' + appointment_id);
    console.log('Chat ReceiverId   ' + userIdd_receiverId);
    console.log('Chat Message   ' + this.state.textmsgText);

    AsyncStorage.getItem('userid').then(userid => {
      var useridd = JSON.parse(userid);

      AsyncStorage.getItem('token').then(token => {
        var tokenn = JSON.parse(token);

        console.log('Chat Token   ' + tokenn);
        console.log('Chat UserId   ' + useridd);

        axios
          .post(
            Application_Constant.BaseUrl + 'api/appointmentchat',
            {
              appointment_id: appointment_id,
              user_id: useridd,
              hospital_id: userIdd_receiverId,
              message: this.state.textmsgText,
            },
            {
              headers: {
                'Content-Type': 'application/json',
                'user-id': useridd,
                token: tokenn,
              },
            },
          )
          .then(response => response.data)
          .then(data => {
            console.log('Data is  ' + data);
            var res_data = data.error;
            console.log('Data Res is  ' + res_data);

            console.log(res_data);
            if (res_data == 'true') {
              var res_data_msg = data.errorMessage;
              console.log('Response True is  ' + res_data_msg);

              this.setState({
                animating: false,
                lodingDialog: false,
                textmsgText: '',
              });
              //alert(res_data_msg)
            } else if (res_data == 'false') {
              this.setState({
                animating: false,
                lodingDialog: false,
                textmsgText: '',
              });
            }
          });
      });
    });
  }

  render() {
    return (
      <View style={styles.containerWhite}>
        <View
          style={{flex: 1, flexDirection: 'column', backgroundColor: 'white'}}>
          <View
            style={{
              flexDirection: 'row',
              height: 50,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{width: '10%', flexDirection: 'row', marginRight: 0}}
         onPress={() => this.props.navigation.navigate('AfterLoginNew')}
           //   onPress={() => this.handleBackButtonClick()}
           >
              <Image
                source={require('../assets/back.png')}
                style={{width: 25, height: 25, marginRight: 10, marginLeft: 0}}
              />
            </TouchableOpacity>
            <View
              style={{
                width: '80%',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: 'Poppins-Bold',
                }}>
                {' '}
                {Application_Constant.typeText}
              </Text>
            </View>
          </View>

          <View
            style={{
              flex: 1,
              backgroundColor: '#e5e5e5',
            }}>
            <ScrollView>
              <FlatList
                data={this.state.chatuser}
                showsVerticalScrollIndicator={false}
                renderItem={({item}) => {
                  if (item.message_by == 2) {
                    var statuscat = '';
                    statuscat = Application_Constant.enableText;
                  } else {
                    var statuscat = '';
                    statuscat = Application_Constant.disableText;
                    //  console.log("status:::$$$$::::" + statuscat);
                  }
                  return (
                    <View style={{flex: 1, marginTop: 10}}>
                      {item.message_by === 2 && (
                        <View style={{width: '100%', marginLeft: 5}}>
                          <View
                            style={{
                              width: 200,
                              height: 60,
                              backgroundColor: '#F8D0D0',
                              borderRadius: 50,
                              marginTop: 10,
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}>
                            {!!item.image &&
                              item.image !=
                                'http://docturgences.com/public/upload/usersprofile/' && (
                                <Image
                                  style={{
                                    width: 40,
                                    height: 40,
                                    borderRadius: 40 / 2,
                                    marginLeft: 10,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                  }}
                                  source={{
                                    uri: item.image,
                                    priority: Image.priority.high,
                                  }}
                                  onLoadStart={e => {
                                    //console.log('Loading Start')

                                    this.imageLoadingStart(true);
                                  }}
                                  onProgress={e => {
                                    // console.log(
                                    //     'Loading Progress ' +
                                    //     e.nativeEvent.loaded / e.nativeEvent.total
                                    // )
                                  }}
                                  onLoad={e => {
                                    // console.log(
                                    //     'Loading Loaded ' + e.nativeEvent.width,
                                    //     e.nativeEvent.height
                                    // )
                                  }}
                                  onLoadEnd={e => {
                                    //   console.log('Loading Ended')
                                    this.imageLoadingStart(false);
                                  }}>
                                  <ActivityIndicator
                                    animating={this.state.animating}
                                    visible={this.state.animating}
                                    style={[{width: 15, height: 15}]}
                                    color="#C00"
                                    size="small"
                                    hidesWhenStopped={true}
                                  />
                                </Image>
                              )}
                            {/* <Image source={require("../assets/profile_picture.png")}
                                                            style={{ width: 40, height: 40, borderRadius: 40 / 2,
                                                             marginLeft: 10 }} /> */}

                            <View
                              style={{
                                flexDirection: 'column',
                                marginLeft: 5,
                                justifyContent: 'space-evenly',
                              }}>
                              <Text style={{fontSize: 14}}>{item.message}</Text>
                              <Text style={{fontSize: 12}}>{item.time}</Text>
                            </View>
                          </View>
                        </View>
                      )}
                      {item.message_by === 1 && (
                        <View
                          style={{
                            width: '100%',
                            alignItems: 'flex-end',
                            marginRight: 5,
                          }}>
                          <View
                            style={{
                              width: 200,
                              height: 60,
                              backgroundColor: '#DBD3FC',
                              borderRadius: 50,
                              marginTop: 10,
                              flexDirection: 'row-reverse',
                              alignItems: 'center',
                            }}>
                            {!!item.image &&
                              item.image !=
                                'http://docturgences.com/public/upload/usersprofile/' && (
                                <Image
                                  style={{
                                    width: 40,
                                    height: 40,
                                    borderRadius: 40 / 2,
                                    marginRight: 10,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                  }}
                                  source={{
                                    uri: item.image,
                                    priority: Image.priority.high,
                                  }}
                                  onLoadStart={e => {
                                    //  console.log('Loading Start')

                                    this.imageLoadingStart2(true);
                                  }}
                                  onProgress={e => {
                                    // console.log(
                                    //     'Loading Progress ' +
                                    //     e.nativeEvent.loaded / e.nativeEvent.total
                                    // )
                                  }}
                                  onLoad={e => {
                                    // console.log(
                                    //     'Loading Loaded ' + e.nativeEvent.width,
                                    //     e.nativeEvent.height
                                    // )
                                  }}
                                  onLoadEnd={e => {
                                    // console.log('Loading Ended')
                                    this.imageLoadingStart2(false);
                                  }}>
                                  <ActivityIndicator
                                    animating={this.state.animating2}
                                    visible={this.state.animating2}
                                    style={[{width: 15, height: 15}]}
                                    color="#C00"
                                    size="small"
                                    hidesWhenStopped={true}
                                  />
                                </Image>
                              )}
                            {/* <Image source={require("../assets/profile_picture.png")}
                                                            style={{ width: 40, height: 40, borderRadius: 40 / 2, marginRight: 10 }} /> */}
                            <View
                              style={{
                                flexDirection: 'column',
                                marginRight: 10,
                                justifyContent: 'space-evenly',
                              }}>
                              <Text style={{fontSize: 14}}>{item.message}</Text>
                              <Text style={{fontSize: 12}}>{item.time}</Text>
                            </View>
                          </View>
                        </View>
                      )}
                    </View>
                  );
                }}
              />
            </ScrollView>
          </View>
          <View
            style={{
              backgroundColor: '#e5e5e5',
              justifyContent: 'flex-end',
            }}>
            <View style={{backgroundColor: '#e5e5e5', marginBottom: 10}}>
              <View
                style={{
                  backgroundColor: 'white',
                  width: '98%',
                  borderRadius: 50,
                  marginLeft: 5,
                  marginRight: 5,
                  marginTop: 5,
                  flexDirection: 'row',
                }}>
                {/*<View style={{ width: '0%', justifyContent: 'center' }}>
                                    <TouchableOpacity style={{ justifyContent: 'center', marginLeft: 15 }} >

                                        <Image source={require("../assets/camera.png")}
                                            style={styles.imageMenuIcon}
                                        />
                                    </TouchableOpacity>
                                </View> */}

                <View style={{width: '85%', paddingLeft: 5}}>
                  <TextInput
                    value={this.state.textmsgText}
                    onChangeText={textmsgText =>
                      this.setState({textmsgText, textmsgTextErr: ''})
                    }
                    placeholder={Application_Constant.typeText}
                    style={{marginLeft: 10, color: 'black', fontSize: 16}}
                  />
                  {!!this.state.textmsgTextErr && (
                    <Text
                      style={{
                        color: 'red',
                        marginLeft: 30,
                        marginTop: -10,
                        fontSize: 12,
                      }}>
                      {this.state.textmsgTextErr}
                    </Text>
                  )}
                </View>

                <View style={{width: '15%', justifyContent: 'center'}}>
                  <TouchableOpacity
                    onPress={() => this.validation()}
                    style={{justifyContent: 'center', marginLeft: 15}}>
                    <Image
                      source={require('../assets/4.png')}
                      style={styles.imageMenuIcon}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerWhite: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column',
    marginTop: Platform.OS == 'ios' ? 0 : 0,
  },
  imageMenuIcon: {
    width: 25,
    height: 20,
  },
});
