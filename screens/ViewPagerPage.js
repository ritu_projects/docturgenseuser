import { StyleSheet, View, Text, TouchableOpacity, AsyncStorage, PermissionsAndroid, Platform,StatusBar, } from 'react-native';
import React, { Component } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';

//import { IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from '@react-native-community/viewpager';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Styles from '../styles/styles';
import Application_Constant from './localization'
import FastImage from 'react-native-fast-image'
import Geolocation from '@react-native-community/geolocation';
import PagerView from 'react-native-pager-view';
const STATUS_BAR_THEME = Platform.OS === 'ios' ? 'dark-content' : 'light-content';

import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";


// export async function requestLocationPermission() {
//     try {
//         const granted = await PermissionsAndroid.request(
//             PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
//             {
//                 'title': 'Example App',
//                 'message': 'Example App access to your location '
//             }
//         )
//         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
//             console.log("You can use the location")
//             //alert("You can use the location");

//         } else {
//             console.log("location permission denied")
//             // alert("Location permission denied");
//         }
//     } catch (err) {
//         console.warn(err)
//     }
// }
export default class ViewPagerPage extends Component {
    constructor(props) {
        super(props);
        {
            this.state = {
                text: Application_Constant.Medication,
                text2: Application_Constant.Medication2,
                text3: Application_Constant.Medication3,

                positionn: 0,
                currentlatitude: 48.953814,
                currentlongitude: 2.899333,

            }
        }
    }
    getCurrentPosition = () => {
        if(Platform.OS == 'ios')
        {

        
        Geolocation.requestAuthorization();
        }
       
        Geolocation.getCurrentPosition(
            (position) => {
              console.log('Position ',position)
              let region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
      
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              };
              this.setState({
                currentlatitude: position.coords.latitude,
                currentlongitude: position.coords.longitude,
                initialRegion: region,
              });
        

            },
            (error) => alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
         );
 };
    async componentDidMount() {


        if (Platform.OS == 'android') {


             this.requestLocationPermission()

        }

        else if (Platform.OS == 'ios') {
            this.getCurrentPosition2();
        }
    }



    async requestLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              'title': 'Location Permission',
              'message': 'MyMapApp needs access to your location'
            }
            )
    
           if (granted === PermissionsAndroid.RESULTS.GRANTED) {
               console.log("Location permission granted")
               this.getCurrentPosition()
    
           } else {
               console.log("Location permission denied")
           }
        } catch (err) {
           console.warn(err)
        }
}
    getCurrentPosition2 = () => {
        if(Platform.OS == 'ios')
        {
         Geolocation.requestAuthorization();
        }
        Geolocation.getCurrentPosition(
           (info) => {
               console.log(info);
               console.log("latitude:::" + info.coords.latitude);
               console.log("longitude:::" + info.coords.longitude);
               this.setState({
                   currentlatitude: info.coords.latitude,
                   currentlongitude: info.coords.longitude,
               })
           },
           (error) => {
             console.log("map error: ",error);
               console.log(error.code, error.message);
           },
           {enableHighAccuracy: true, timeout: 15000, maximumAge: 5000},
           );
       

    };
    _onPressButton = () => {
        if (this.state.positionn == 0) {

        }
        else {

            let pos = this.state.positionn;

            console.log('position::####::::' + pos);
            console.log('position::::$$$::' + this.state.postistio);


            let new_pos = pos - 1;

            console.log('position number', new_pos);
            this.setState({ positionn: new_pos })
            console.log('position state', this.state.positionn);

            this.viewPager.setPage(new_pos);

        }
    }

    _onPressButton2 = () => {
        console.log('position add::***::::' + this.state.positionn);

        if (this.state.positionn == 2) {

        }
        else {

            this.props.navigation.navigate('MapExample');

        }


    }
    _onPressButton3 = () => {

        console.log('position::!!!::::' + this.state.positionn);


        if (this.state.positionn == 2) {
            this.props.navigation.navigate('MapExample');

        }
    }

        setPageIndex = (positionn) =>
        {

                console.log("Position method ",positionn)
                if(positionn == 2)
                {
                    this.props.navigation.navigate('MapExample');

                }

        }
    render() {
        return (
            <View 
            style={{ flex: 1, marginTop: Platform.OS == 'ios' ? 0 : 0, }}>
              
        {Platform.OS == 'ios' ?
          <StatusBar
            translucent={true}
            backgroundColor={'#324048'}
            barStyle={STATUS_BAR_THEME} />
          :
          null
        }
                <PagerView
                    style={Styles.viewpagerbg_img}
                    ref={viewPager => {
                        this.viewPager = viewPager
                    }}
                    initialPage={0}
                    onPageSelected={e => 
                        {
                            console.log("sseltce:::::::" + e.nativeEvent.position),

                           this.setPageIndex(e.nativeEvent.position)
                        }
                       }

                 

                    //indicator={this._renderDotIndicator()}
                    showPageIndicator = {true}
                    >

                    <View 
                    key="1"
                    style={{flex:1, backgroundColor:'white',alignItems:'center',}}>

                        <FastImage
                            source={require("../assets/Fichier2.png")} 
                            style={Styles.splashbg_img2} 
                            resizeMode={FastImage.resizeMode.contain}
                            >
                                </FastImage>
                            <Text style={Styles.viewpager_txt_new} >  {this.state.text}   </Text>
                            <Text style={Styles.viewpager_secondtxt_new}>{Application_Constant.Firsttab2ndTxt}</Text>

                     

                    </View>



                    <View 
                    key="2">
                        <FastImage 
                        source={require("../assets/pagebg_one.png")} style={Styles.splashbg_img} >
                            <Text style={Styles.viewpager_txt} >  {this.state.text2}   </Text>
                            <Text style={Styles.viewpager_secondtxt}>{Application_Constant.Firsttab2ndTxt2}</Text>

                        </FastImage>




                    </View>
                    <View 
                    key="3">

                        <FastImage source={require("../assets/pagebg_three.png")} style={Styles.splashbg_img} >
                            <Text style={Styles.viewpager_txt} >  {this.state.text3}   </Text>
                            <Text style={Styles.viewpager_secondtxt}>{Application_Constant.Firsttab2ndTxt3}</Text>

                        </FastImage>




                    </View>
                </PagerView>


            </View>
        );
    }

    _renderTitleIndicator() {
        return <PagerTitleIndicator titles={['one', 'two', 'three']} />;
    }

    _renderDotIndicator() {
        return <PagerDotIndicator pageCount={3} />;
    }



}