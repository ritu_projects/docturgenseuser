import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Button,
  Text,
  TouchableOpacity,
  Linking,
  PermissionsAndroid,
  Alert,
  Modal,
  TextInput,
  ActivityIndicator,
  AsyncStorage,
  Platform,
} from 'react-native';
import MapView from 'react-native-maps';
import Styles from '../styles/styles_home';
import {Card, CardItem, Icon, Thumbnail, Row, TabHeading} from 'native-base';
import Colors from '../colors/colors';
import LinearGradient from 'react-native-linear-gradient';
import Sidebar from './Sidebar';
import Geolocation from '@react-native-community/geolocation';
import Application_Constant from '../strings/string';
import Image from 'react-native-fast-image';

import PopupDialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import axios from 'axios';

import RadioGroup from 'react-native-radio-button-group';

var radiogroup_options = [
  {id: 0, label: 'M', value: '1'},
  {id: 1, label: 'F', value: '2'},
];

var radiogroup_options_slection = [
  {id: 0, label: 'Adulte', value: '1'},
  {id: 1, label: 'enfant', value: '2'},
];

const steps = [
  {
    id: '0',
    message: 'Welcome to react chatbot!',
    trigger: '1',
  },
  {
    id: '1',
    message: 'Bye!',
    end: true,
  },
];

export async function requestLocationPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Example App',
        message: 'Example App access to your location ',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the location');
      //alert("You can use the location");
    } else {
      console.log('location permission denied');
      // alert("Location permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
}

export default class AfterLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentlatitude: 48.953814,
      currentlongitude: 2.899333,
      modalVisible: false,
      modalVisible2: false,
      modalVisible3: false,
      modalVisible4: false,

      animating: false,
      lodingDialog: false,
      appointment_status: '',
      request_status: '',
      appointment_id: '',
      admin_telephone: '',
      userId: '',
      token: '',
      subcat_id: '',
      admin_telephone: '',
      markerdialog_title: 'Lariboisiere S.A.U.',
      reachingtime: '20',
      googlemapTime: '',
      professional_address: '',
      msg: '',
      msgErr: '',
      genderErr: '',
      gender: '',
      slectionErr: '',
      slection: '',
      hospital_Id: '',
      number: 5,
      subCatdata: [],
      subCatdata: [],

      phonenumber: '+33 998877667',
      specialist_latitude: 0,
      specialist_longitude: 0,
      initialRegion: '',
      markerss: [
        {
          coordinates: {
            latitude: 48.953814,
            longitude: 2.899333,
          },
        },
      ],

      markers: [],
    };
  }
  showModal() {
    this.setState({modalVisible: true});
  }
  hideModal() {
    this.setState({modalVisible: false});

    //  this.refs.PhoneInput._root.focus()
  }

  reachLocation() {
    this.setState({
      animating: true,
      lodingDialog: true,
    });
    console.log('Request Appointment UserId' + this.state.userId);

    axios
      .post(
        Application_Constant.BaseUrl + 'api/ireachedlocation',
        {
          hospital_id: this.state.hospital_Id,
          user_id: this.state.userId,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'user-id': this.state.userId,
            token: this.state.token,
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('UserRequest is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
          alert(res_data_msg);
        } else if (res_data == 'false') {
          this.setState({
            animating: false,
            lodingDialog: false,
            msg: '',
          });
          this.props.navigation.navigate('AfterLogin');
        }
      });
    try {
      this.hideModal();
    } catch (e) {}
    try {
      this.hideModal3();
    } catch (e) {}

    this.openMap();
  }
  openMap() {
    console.log('Current lat map%%%' + this.state.currentlatitude);
    console.log('Current lng map%%%' + this.state.currentlongitude);

    console.log('Current dest lat map%%%' + this.state.specialist_latitude);
    console.log('Current dest lng map%%%' + this.state.specialist_longitude);

    var s_lat_val = this.state.currentlatitude;
    var s_lng_val = this.state.currentlongitude;

    var d_lat_val = this.state.specialist_latitude;
    var d_lng_val = this.state.specialist_longitude;

    Platform.select({
      ios: () => {
        Linking.openURL(
          'http://maps.apple.com/maps?saddr=' +
            s_lat_val +
            ',' +
            s_lng_val +
            '&daddr=' +
            d_lat_val +
            ',' +
            d_lng_val,
        );
      },
      android: () => {
        Linking.openURL(
          'http://maps.google.com/maps?saddr=' +
            s_lat_val +
            ',' +
            s_lng_val +
            '&daddr=' +
            d_lat_val +
            ',' +
            d_lng_val,
        );
      },
    })();
  }

  showModal3() {
    this.setState({modalVisible3: true});
  }
  hideModal3() {
    this.setState({modalVisible3: false});

    //  this.refs.PhoneInput._root.focus()
  }
  showModal2() {
    this.setState({
      modalVisible: false,
      modalVisible2: true,
    });
  }

  callChatScreen(appointment_id) {
    this.setState({
      modalVisible3: false,
    });
    console.log('Appointment id  ' + this.state.appointment_id);
    console.log('Appointment Reciver id  ' + this.state.hospital_Id);

    this.props.navigation.navigate('Chat', {
      appointment_id: this.state.appointment_id,
      userIdd_receiverId: this.state.hospital_Id,
    });
  }
  sendRequesttoSpecialist() {
    this.setState({
      animating: true,
      lodingDialog: true,
    });
    console.log('Request Appointment UserId' + this.state.userId);

    axios
      .post(
        Application_Constant.BaseUrl + 'api/userRequestappointment',
        {
          hospital_id: this.state.hospital_Id,
          user_id: this.state.userId,
          message: this.state.msg,
          book_status: '1',
          age_group: this.state.slection,
          gender: this.state.gender,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'user-id': this.state.userId,
            token: this.state.token,
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('UserRequest is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
          //  alert(res_data_msg)
        } else if (res_data == 'false') {
          this.setState({
            animating: false,
            lodingDialog: false,
            msg: '',
          });
          this.props.navigation.navigate('AfterLogin');
        }
      });
  }

  hideModal2() {
    this.setState({
      modalVisible2: false,
    });
  }

  componentDidMount() {
    AsyncStorage.getItem('phonenumber').then(phonenumber => {
      console.log('Phonenumber  ' + phonenumber);

      this.setState({
        phonenumber: phonenumber,
      });
    });

    requestLocationPermission();
    this.getallSubCategories();
    this.getCurrentPosition();

    const {navigation} = this.props;
    navigation.addListener('willFocus', () => {
      this.getCurrentPosition();
    });
  }
  componentWillReceiveProps() {
    AsyncStorage.getItem('phonenumber').then(phonenumber => {
      console.log('Phonenumber  ' + phonenumber);

      this.setState({
        phonenumber: phonenumber,
      });
    });

    requestLocationPermission();
    this.getCurrentPosition();
    this.getallSubCategories();
  }
  validation() {
    var isValidate = 0;

    if (this.state.slection != '') {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        slectionErr: Application_Constant.ShouldemptyText,
      });
    }

    if (this.state.gender != '') {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        genderErr: Application_Constant.ShouldemptyText,
      });
    }
    if (this.state.msg != '') {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        msgErr: Application_Constant.ShouldemptyText,
      });
    }
    if (isValidate == 3) {
      isValidate += 1;
      this.setState({
        modalVisible2: false,
      });

      this.sendRequesttoSpecialist();
    }
  }
  getallSubCategories() {
    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);

      var useridd = JSON.parse(userid);
      console.log('User ID^^^' + useridd);

      this.setState({
        userId: useridd,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      console.log('User Token^^^' + tokenn);

      this.setState({
        token: tokenn,
      });

      console.log('User Token%%%' + this.state.token);
      this.setState({
        animating: true,
        lodingDialog: true,
      });

      axios
        .get(
          Application_Constant.BaseUrl + 'api/getAllSubCategory',
          //{
          //     user_id: this.state.userId,
          // },
          //     {
          //         headers: {

          //             'user-id': this.state.userId,
          //             'token': this.state.token,
          //             'Content-Type': 'application/json',

          //             //other header fields
          //         }
          //  }
        )
        .then(response => response.data)
        .then(data => {
          console.log('Data is Category   ' + JSON.stringify(data));
          var res_data = data.error;
          console.log('Data Res is  ' + res_data);

          console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            this.setState({
              animating: false,
              lodingDialog: false,
              subCatdata: [],
            });
            // alert(res_data_msg)
          } else if (res_data == 'false') {
            this.setState({
              subCatdata: [],
            });
            this.setState({
              animating: false,
              lodingDialog: false,
              subCatdata: data.record,
            });
            console.log('SubCat Data ', this.state.subCatdata.length);
          }
          this.updateLatlong_ofUser();

          this.getHospital();
        });
    });
  }
  getSubCat_Id(item_subcatid) {
    console.log('Item SubCat Id   ' + item_subcatid);
    this.setState({
      subcat_id: item_subcatid,
    });
    // this.showModal();
    this.getHospital();
  }

  getIndividualHospital_Details(hospitl_id) {
    this.setState({
      animating: true,
      lodingDialog: true,
    });
    axios
      .post(
        Application_Constant.BaseUrl + 'api/getHospitalDetail',
        {
          hospital_id: hospitl_id,
          user_id: this.state.userId,
        },
        {
          headers: {
            'user-id': this.state.userId,
            token: this.state.token,
            'Content-Type': 'application/json',

            //other header fields
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('Data GetIndividual is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res AllHospital is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True AllHospital is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
            reachingtime: 'Aucune donnée trouvée',
            markerdialog_title: 'Aucune donnée trouvée',
            googlemapTime: 'Aucune donnée trouvée',
            professional_address: 'Aucune donnée trouvée',
          });
          // alert(res_data_msg)
        } else if (res_data == 'false') {
          this.setState({
            reachingtime: data.record.waiting_time,
            markerdialog_title: data.record.fullname,
            googlemapTime: data.record.current_time,
            professional_address: data.record.address,
            hospital_Id: hospitl_id,
            appointment_status: data.book_status,
            request_status: data.request_status,
            specialist_latitude: data.record.latitude,
            specialist_longitude: data.record.longitude,
            animating: false,
            lodingDialog: false,
          });
          console.log(
            'Request appointment Status  ' + this.state.appointment_status,
          );

          if (
            this.state.appointment_status != null &&
            this.state.appointment_status != '' &&
            this.state.appointment_status != 'null' &&
            this.state.appointment_status == 1
          ) {
            console.log('Request Status  ' + this.state.request_status);

            if (this.state.request_status == 0) {
              //  alert("Vous avez déjà envoyé des demandes pour ces hôpitaux")

              this.setState({modalVisible4: true});
            } else if (this.state.request_status == 1) {
              this.setState({
                appointment_id: data.appointment_id,
                admin_telephone: data.admin_telephone,
              });
              this.showModal3();
            }
          } else {
            this.showModal();
          }
        }
      });
  }

  updateLatlong_ofUser() {
    setInterval(() => {
      if (this.state.number == 0) {
        // alert("Time out")
        this.setState({number: 5});
        console.log('Current Latitude   ' + this.state.currentlatitude);
        console.log('Current Longitude' + this.state.currentlongitude);

        axios
          .post(
            Application_Constant.BaseUrl + 'api/update_lat_long',
            {
              user_id: this.state.userId,
              latitude: this.state.currentlatitude,
              longitude: this.state.currentlongitude,
            },
            {
              headers: {
                'user-id': this.state.userId,
                token: this.state.token,
                'Content-Type': 'application/json',

                //other header fields
              },
            },
          )
          .then(response => response.data)
          .then(data => {
            console.log('Data is  ' + JSON.stringify(data));
            var res_data = data.error;
            console.log('Data Res is  ' + res_data);

            console.log(res_data);
            if (res_data == 'true') {
              var res_data_msg = data.errorMessage;
              console.log('Response True is  ' + res_data_msg);

              // this.setState({
              //     animating: false,
              //     lodingDialog: false,

              // });
              //   alert(res_data_msg)
            } else if (res_data == 'false') {
              // this.setState({
              //     animating: false,
              //     lodingDialog: false,
              // });
            }
          });

        Geolocation.getCurrentPosition(info => {
          console.log('latitude:::' + info.coords.longitude);
          console.log('latitude:::' + info.coords.latitude);

          this.setState({
            currentlatitude: info.coords.latitude,
            currentlongitude: info.coords.longitude,
          });
        });

        this.getCurrentPosition();
      }
      this.setState({number: parseInt(this.state.number, 10) - 1});
    }, 2000);
  }
  // async moveToRegion() {
  //     console.log("Icon Click " + "Icon Click ")
  //     await navigator.geolocation.getCurrentPosition(
  //         (geoLocation) => {

  //             let region = {
  //                 latitude: parseFloat(geoLocation.latitude),
  //                 longitude: parseFloat(geoLocation.longitude),
  //                 latitudeDelta: 5,
  //                 longitudeDelta: 5
  //             };
  //             this.setState({
  //                 initialRegion: region

  //             });
  //             this.map.animateToRegion(this.state.initialRegion, 2000);

  //         });

  // }
  getHospital() {
    console.log('Latitude Current  ' + this.state.currentlatitude);
    console.log('Longitude Current  ' + this.state.currentlongitude);
    console.log('SubCat id  ' + this.state.subcat_id);
    this.setState({
      animating: true,
      lodingDialog: true,
    });
    axios
      .post(
        Application_Constant.BaseUrl + 'api/allHospital',
        {
          current_lat: this.state.currentlatitude,
          current_long: this.state.currentlongitude,
          category_id: this.state.subcat_id,
          user_id: this.state.userId,
        },
        {
          headers: {
            'user-id': this.state.userId,
            token: this.state.token,
            'Content-Type': 'application/json',

            //other header fields
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('Data AllHospital is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res AllHospital is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True AllHospital is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
            markers: [],
          });
          //   alert(res_data_msg)
          this.setState({
            animating: false,
            lodingDialog: false,
          });
        } else if (res_data == 'false') {
          var obj = data.record;
          console.log('resoJSon GetVehicle obj===' + obj);

          var count = Object.keys(obj).length;
          console.log('resoJSon GetVehicle count===' + count);

          var hospitalMarker = [];
          this.setState({
            markers: [],
          });
          for (var i = 0; i < count; i++) {
            console.log('Latitude is%%%' + obj[i].status);

            if (obj[i].status != 2) {
              hospitalMarker.push({
                title: obj[i].admin_name,
                status: obj[i].status,

                coordinate: {
                  latitude: JSON.parse(obj[i].latitude),
                  longitude: JSON.parse(obj[i].longitude),
                },
                id: obj[i].id,
                image: require('../assets/markerbg.png'),
              });
            } else if (obj[i].status == 2) {
              hospitalMarker.push({
                title: obj[i].admin_name,
                status: obj[i].status,

                coordinate: {
                  latitude: JSON.parse(obj[i].latitude),
                  longitude: JSON.parse(obj[i].longitude),
                },
                id: obj[i].id,
                image: require('../assets/map2bg.png'),
              });
            }
          }

          // hospitalMarker.push({
          //     coordinate: {
          //       latitude: obj.latitude,
          //       longitude: obj.longitude,
          //     },
          //   });
          var obj2 = JSON.stringify(hospitalMarker);
          console.log('Hospital is   ' + obj2);

          this.setState({
            //     markers.push({
            //     coordinate: {
            //         latitude: JSON.parse(res_data.record.latitude),
            //         longitude: JSON.parse(res_data.record.longitude),
            //         title: JSON.parse(res_data.record.fullname),
            //     },
            // })
            markers: hospitalMarker,
          });

          this.setState({
            animating: false,
            lodingDialog: false,
          });
        }
      });
  }
  iconClick() {
    console.log('Icon Click  ', 'Icon Click ');
    this.getCurrentPosition();
  }
  getCurrentPosition = () => {
    return new Promise((resolve, reject) => {
      Geolocation.getCurrentPosition(info => {
        console.log('latitude:::' + info.coords.latitude);
        console.log('longitude:::' + info.coords.longitude);

        let region = {
          latitude: info.coords.latitude,
          longitude: info.coords.longitude,

          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        };
        this.setState({
          currentlatitude: info.coords.latitude,
          currentlongitude: info.coords.longitude,
          initialRegion: region,
        });

        this.map.animateToRegion(this.state.initialRegion, 2000);
      });
    });
  };
  forLogin = () => {};
  callNumber = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };
  callNumber2 = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.admin_telephone + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.admin_telephone + '}';
    }

    Linking.openURL(phoneNumber);
  };
  openAlert = () => {
    Alert.alert(
      'Alert Title',
      'My Alert Msg',
      [
        {
          text: 'Ask me later',
          onPress: () => console.log('Ask me later pressed'),
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  };

  onSelect(option) {
    var opp = JSON.stringify(option);
    console.log('Value   ' + opp);
    var opp2 = option.value;
    console.log('Value2   ' + opp2);

    this.setState({
      gender: opp2,
      genderErr: '',
    });
  }

  onSelect2(option) {
    var opp = JSON.stringify(option);
    console.log('Value   ' + opp);
    var opp2 = option.value;
    console.log('Value2   ' + opp2);

    this.setState({
      slection: opp2,
      slectionErr: '',
    });
  }
  onMarkerClick = evt => {
    console.log('Marker is$$$' + evt);
    // this.AlertPro.open();
    this.getIndividualHospital_Details(evt);
  };

  render() {
    console.log('SubCat Length Render ', this.state.subCatdata.length);
    return (
      <View style={Styles.containerWhite}>
        <MapView
          style={Styles.map_bg}
          ref={map => {
            this.map = map;
          }}
          initialRegion={this.state.initialRegion}
          mapType={'standard'}
          // customMapStyle={mapStyle}
          minZoomLevel={1}
          maxZoomLevel={20}
          enableZoomControl={true}
          //     showsUserLocation={true}
          //   showsMyLocationButton={true}
          zoomEnabled={true}>
          {this.state.markers.map((marker, i) => (
            <MapView.Marker
              coordinate={marker.coordinate}
              // title={marker.title}
              onPress={() => this.onMarkerClick(marker.id)}>
              <View style={{width: '100%', height: 70, alignItems: 'center'}}>
                <Image
                  style={{
                    width: 40,
                    height: 40,
                    alignItems: 'center',
                    paddingTop: 3,
                  }}
                  // source={require("../assets/markerbg.png")}
                  source={marker.image}>
                  <Image
                    style={{width: 20, height: 20}}
                    source={require('../assets/doctor.png')}
                  />
                </Image>

                <View
                  style={{
                    backgroundColor: 'white',
                    borderRadius: 3,
                    marginTop: 1,
                    flexDirection: 'row',
                    padding: 5,
                  }}>
                  <Text style={Styles.pinText}>{marker.title}</Text>
                </View>
              </View>
            </MapView.Marker>
          ))}
        </MapView>

        <Image
          style={{
            width: '100%',

            position: 'absolute',
            bottom: 10,
            height: 150,
            flexDirection: 'row',
          }}
          resizeMode="stretch"
          source={require('../assets/list_bg.png')}>
          <View style={Styles.cardItemStle}>
            <FlatList
              ref={flatList1 => {
                this.flatList1 = flatList1;
              }}
              style={{
                width: '100%',
                marginTop: 10,
                marginBottom: 0,
                marginLeft: 10,
                marginRight: 10,
              }}
              horizontal={false}
              data={this.state.subCatdata}
              keyExtractor={item => item.id}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={{
                    width: '100%',
                    marginBottom: 10,
                  }}
                  onPress={() => this.getSubCat_Id(item.id)}>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'center',
                      paddingLeft: 5,
                      paddingRight: 5,
                      paddingTop: 5,
                      paddiingBottom: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'Poppins-Medium',
                        textAlign: 'center',
                        color: 'white',
                      }}>
                      {item.sub_cat_name}
                    </Text>
                  </View>

                  {/* <View
                                            style={{
                                                width: "90%",
                                              flexDirection: "row",
                                              height: "1",
                                              marginLeft: 5,
                                              marginRight: 5,
                                              backgroundColor: 'white',


                                            }}>
                                                </View> */}
                </TouchableOpacity>
              )}
            />
          </View>
        </Image>

        <PopupDialog
          onHardwareBackPress={() => {
            this.setState({lodingDialog: false});
          }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({slideFrom: 'bottom'})}>
          <DialogContent>
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[
                  {
                    height: 10,
                    marginBottom: 10,
                    marginTop: 30,
                    marginLeft: 20,
                    marginRight: 20,
                  },
                ]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>
        <Modal
          //   source={require('../assets/box.png')}
          animationType="slide"
          transparent={true}
          onHardwareBackPress={() => {
            this.setState({modalVisible: false});
          }}
          // presentationStyle='fullScreen'
          width={0.9}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#d3d3d3',
          }}
          visible={this.state.modalVisible}
          // dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
        >
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <DialogContent style={{}}>
              <Image
                source={require('../assets/box.png')}
                width={0.9}
                style={{
                  width: '94%',
                  justifyContent: 'center',
                  height: 321,
                }}>
                <View style={{flexDirection: 'row', height: 50}}>
                  <View
                    style={{
                      width: '83%',
                      alignItems: 'center',
                    }}>
                    {/* <Text
                                            style={{
                                                color: "black", fontFamily: "Poppins-Medium", paddingLeft: 55,

                                            }}>{this.state.reachingtime} minutes
                           </Text> */}
                    <Text
                      style={{
                        color: 'black',
                        fontFamily: 'Poppins-Medium',
                        paddingLeft: 55,
                      }}>
                      HHH
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={{
                      width: '10%',
                      marginTop: 30,
                    }}
                    onPress={() => this.hideModal()}>
                    <Image
                      source={require('../assets/cross_icon.png')}
                      style={{
                        width: 30,
                        height: 30,
                        marginRight: 0,
                      }}
                    />
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontFamily: 'Poppins-Bold',
                        color: 'white',
                      }}>
                      {Application_Constant.proffessional_name}:{' '}
                      {this.state.markerdialog_title}
                    </Text>
                  </View>
                  {/*
                                    <Text
                                        style={{
                                            textAlign: "center",
                                            fontFamily: "Poppins-Regular",
                                            paddingTop: 10,
                                            color: "white",


                                        }}>{this.state.googlemapTime}min de trajet(voiture)</Text> */}

                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: 'Poppins-Regular',
                      paddingTop: 10,
                      color: 'white',
                    }}>
                    {Application_Constant.proffessional_address}:{' '}
                    {this.state.professional_address}
                  </Text>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: 'Poppins-Regular',
                      paddingTop: 10,
                      color: 'white',
                    }}>
                    {this.state.reachingtime}min sur place
                  </Text>

                  <TouchableOpacity
                    style={{
                      width: 250,
                      borderRadius: 5,
                      backgroundColor: 'white',
                      height: 50,
                      marginLeft: 10,
                      marginRight: 0,
                      marginTop: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() => this.showModal2()}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#6FB8EF',
                        fontFamily: 'Poppins-Medium',
                        padding: 15,
                      }}>
                      Aller à l'hôpital
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: 250,
                      borderRadius: 5,
                      backgroundColor: 'white',
                      height: 50,
                      marginLeft: 10,
                      marginRight: 0,
                      marginTop: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() => this.reachLocation()}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#6FB8EF',
                        padding: 10,
                        fontFamily: 'Poppins-Medium',
                      }}>
                      Je suis en attente sur place{' '}
                    </Text>
                  </TouchableOpacity>
                </View>
              </Image>
            </DialogContent>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          onHardwareBackPress={() => {
            this.setState({modalVisible3: false});
          }}
          // presentationStyle='fullScreen'
          width={0.9}
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#d3d3d3',
          }}
          visible={this.state.modalVisible3}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
            }}>
            <DialogContent style={{}}>
              <Image
                source={require('../assets/new_box.png')}
                width={0.9}
                style={{
                  width: '100%',
                  height: 286,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      width: '83%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: 'black',
                        marginLeft: 60,
                        fontFamily: 'Poppins-Medium',
                      }}>
                      {this.state.reachingtime} minutes
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={{
                      width: '10%',
                      marginTop: 40,
                    }}
                    onPress={() => this.hideModal3()}>
                    <Image
                      source={require('../assets/cross_icon.png')}
                      style={{
                        width: 30,
                        height: 30,
                        marginRight: 0,
                      }}
                    />
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontFamily: 'Poppins-Bold',
                        color: 'white',
                      }}>
                      {this.state.markerdialog_title}
                    </Text>
                  </View>

                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: 'Poppins-Regular',
                      paddingTop: 10,
                      color: 'white',
                    }}>
                    {this.state.googlemapTime}min de trajet(voiture)
                  </Text>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: 'Poppins-Regular',
                      paddingTop: 10,
                      color: 'white',
                    }}>
                    {this.state.reachingtime}min sur place
                  </Text>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: 'Poppins-Regular',
                      paddingTop: 10,
                      color: '#008838',
                    }}>
                    validé
                  </Text>

                  <View style={{width: '100%', flexDirection: 'row'}}>
                    <TouchableOpacity
                      style={{
                        width: '46%',
                        borderRadius: 5,
                        backgroundColor: 'white',
                        height: 50,
                        marginLeft: 10,
                        marginRight: 0,
                        marginTop: 10,
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                      }}
                      onPress={() =>
                        this.callChatScreen(this.state.hospital_Id)
                      }>
                      <Image
                        source={require('../assets/message_icon.png')}
                        style={{width: 30, height: 30}}
                      />
                      <Text
                        style={{
                          textAlign: 'center',
                          color: '#6FB8EF',
                          fontFamily: 'Poppins-Medium',
                          padding: 15,
                        }}>
                        Chat
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        width: '48%',
                        borderRadius: 5,
                        backgroundColor: 'white',
                        height: 50,
                        marginLeft: 10,
                        marginRight: 0,
                        marginTop: 10,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      onPress={() => this.callNumber2()}>
                      <Image
                        source={require('../assets/mobile_icon.png')}
                        style={{width: 30, height: 30}}
                      />
                      <Text
                        style={{
                          textAlign: 'center',
                          color: '#6FB8EF',
                          padding: 10,
                          fontFamily: 'Poppins-Medium',
                        }}>
                        Appelez
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <TouchableOpacity
                    style={{
                      width: 250,
                      borderRadius: 5,
                      backgroundColor: 'white',
                      height: 50,
                      marginLeft: 10,
                      marginRight: 0,
                      marginTop: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() => this.reachLocation()}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#6FB8EF',
                        padding: 10,
                        fontFamily: 'Poppins-Medium',
                      }}>
                      GO{' '}
                    </Text>
                  </TouchableOpacity>
                </View>
              </Image>
            </DialogContent>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          onHardwareBackPress={() => {
            this.setState({modalVisible2: false});
          }}
          // presentationStyle='fullScreen'
          width={0.9}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent',
          }}
          visible={this.state.modalVisible2}
          // dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
        >
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <DialogContent>
              <Image
                source={require('../assets/box.png')}
                width={0.9}
                style={{
                  width: '94%',
                  justifyContent: 'center',
                  height: 360,
                }}>
                <View style={{flexDirection: 'row-reverse'}}>
                  <TouchableOpacity
                    style={{
                      width: '17%',
                      marginTop: 30,
                    }}
                    onPress={() => this.hideModal2()}>
                    <Image
                      source={require('../assets/cross_icon.png')}
                      style={{
                        width: 30,
                        height: 30,
                        marginRight: 0,
                      }}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'column', marginTop: 10}}>
                  <View style={{marginLeft: 20, marginRight: 20}}>
                    <RadioGroup
                      horizontal
                      style={{flexDirection: 'row', padding: 40, marginTop: 20}}
                      options={radiogroup_options_slection}
                      onChange={option => this.onSelect2(option)}
                      flexDirection="column"
                      circleStyle={{fillColor: '#4085F4', borderColor: 'grey'}}
                    />
                    {!!this.state.slectionErr && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.genderErr}
                      </Text>
                    )}
                  </View>
                  <View style={{marginLeft: 20, marginRight: 20}}>
                    <RadioGroup
                      horizontal
                      style={{flexDirection: 'row', padding: 40, marginTop: 20}}
                      options={radiogroup_options}
                      onChange={option => this.onSelect(option)}
                      flexDirection="row"
                      circleStyle={{fillColor: '#4085F4', borderColor: 'grey'}}
                    />
                    {!!this.state.genderErr && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.genderErr}
                      </Text>
                    )}
                  </View>
                </View>
                <TextInput
                  value={this.state.msg}
                  style={{
                    fontFamily: 'Poppins-Regular',
                    padding: 10,
                    height: 40,
                    marginTop: 0,
                    color: 'white',
                    marginLeft: 20,
                    marginRight: 20,
                  }}
                  editable
                  maxLength={170}
                  underlineColorAndroid="white"
                  placeholder="Entrez vos messages"
                  onChangeText={msg => this.setState({msg, msgErr: ''})}
                />
                {!!this.state.msgErr && (
                  <Text
                    style={{
                      color: 'red',
                      marginLeft: 10,
                      marginTop: -5,
                      fontSize: 12,
                    }}>
                    {this.state.msgErr}
                  </Text>
                )}

                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                  }}>
                  <TouchableOpacity
                    style={{
                      width: 250,
                      borderRadius: 5,
                      backgroundColor: 'white',
                      height: 50,
                      marginLeft: 10,
                      marginRight: 0,
                      marginTop: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}
                    onPress={() => this.validation()}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#6FB8EF',
                        fontFamily: 'Poppins-Medium',
                        padding: 15,
                      }}>
                      Envoyer la demande
                    </Text>

                    <Image
                      source={require('../assets/right-tick_icon.png')}
                      style={{width: 30, height: 30}}
                    />
                  </TouchableOpacity>
                </View>
              </Image>
            </DialogContent>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          onHardwareBackPress={() => {
            this.setState({modalVisible4: false});
          }}
          // presentationStyle='fullScreen'
          width={0.9}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent',
          }}
          visible={this.state.modalVisible4}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <DialogContent>
              <Image
                source={require('../assets/box.png')}
                width={0.9}
                style={{
                  width: '94%',
                  justifyContent: 'center',
                  height: 250,
                }}>
                <Text
                  style={{
                    color: 'black',
                    fontFamily: 'Poppins-Medium',
                    paddingLeft: 20,
                    paddingRight: 20,
                    marginTop: 20,
                  }}>
                  Vous avez déjà envoyé des demandes pour ces hôpitaux
                </Text>

                <Text
                  style={{
                    color: '#ff8a40',
                    fontFamily: 'Poppins-SemiBold',
                    marginTop: 10,
                    paddingLeft: 20,
                    paddingRight: 20,
                  }}>
                  {' '}
                  en attente'
                </Text>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                  }}>
                  <TouchableOpacity
                    style={{
                      width: 250,
                      borderRadius: 5,
                      backgroundColor: 'white',
                      height: 50,
                      marginLeft: 10,
                      marginRight: 0,
                      marginTop: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}
                    onPress={() => {
                      this.setState({modalVisible4: false});
                    }}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#6FB8EF',
                        fontFamily: 'Poppins-Medium',
                        padding: 15,
                      }}>
                      {Application_Constant.ok}
                    </Text>
                  </TouchableOpacity>
                </View>
              </Image>
            </DialogContent>
          </View>
        </Modal>
      </View>
    );
  }
}
