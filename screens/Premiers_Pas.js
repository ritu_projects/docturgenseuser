import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Button,
  Profile,
  FlatList,
  MenuItem,
  ScrollView,
  DrawerLayoutAndroid,
  Dimensions,
  TouchableHighlight,
  Linking,
} from 'react-native';
import {Card} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import {TextInput} from 'react-native-gesture-handler';
import Colors from '../colors/colors';
import Application_Constant from './localization';
import Image from 'react-native-fast-image';

export default class Premiers_Pas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phonenumber: '+33 998877667',
    };
  }
  callNumber = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };

  render() {
    return (
      <ScrollView
        style={{
          flex: 1,
        }}>
        <Image
          source={require('../assets/premium_pas_one.png')}
          style={{
            flex: 1,
            flexDirection: 'column',
            width: '100%',
            height: '100%',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 350,
            }}>
            <Text
              style={{
                textAlign: 'center',
                color: '#7CB5F8',
                fontFamily: 'Poppins-Medium',
                fontSize: 16,
              }}>
              1.Temps d'attente
            </Text>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'Poppins-Regular',
                marginTop: 20,
                marginLeft: 20,
                marginRight: 20,
                padding: 3,
                fontSize: 15,
                color: 'grey',
              }}>
              Comparez les temps d’attente dans les hôpitaux et leurs
              alternatives
            </Text>

            <TouchableOpacity
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 10,
                marginBottom: 20,
              }}
              onPress={() => this.props.navigation.navigate('Premiers_Pas2')}>
              <LinearGradient
                style={{
                  marginLeft: 20,
                  marginRight: 20,
                  backgroundColor: 'white',
                  borderRadius: 15,
                  padding: 15,
                  marginTop: 20,
                  flexDirection: 'row',
                }}
                colors={['#7CB5F8', '#4C94E6']}>
                <Text
                  style={{
                    width: '100%',
                    fontFamily: 'Poppins-Bold',
                    color: 'white',
                    fontSize: 15,
                    textAlign: 'center',
                  }}>
                  {Application_Constant.next_step}
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </Image>
        <TouchableOpacity
          style={{
            bottom: 0,
            height: 70,
          }}
          onPress={this.callNumber}>
          <LinearGradient
            style={{
              flexDirection: 'row',
              backgroundColor: 'white',
              borderTopLeftRadius: 27,
              borderTopRightRadius: 27,
              padding: 20,
              alignItems: 'center',
            }}
            colors={['#7CB5F8', '#4C94E6']}
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}>
            <TouchableOpacity
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginRight: 5,
                marginLeft: 5,
                paddingLeft: 10,
              }}
              onPress={this.callNumber}>
              <Image
                style={{width: 40, height: 40}}
                source={require('../assets/call_icon.png')}
              />
              <Text
                style={{
                  width: '100%',
                  fontFamily: 'Poppins-Regular',
                  paddingLeft: 0,
                  marginRight: 5,
                  color: 'white',
                  flex: 1,
                  fontSize: 13,
                  paddingRight: 5,
                  marginLeft: 15,
                }}>
                Je suis en situation d’urgence{' '}
              </Text>
            </TouchableOpacity>
          </LinearGradient>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}
