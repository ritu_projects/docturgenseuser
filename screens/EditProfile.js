import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Platform,
  Profile,
  FlatList,
  MenuItem,
  ScrollView,
  ActivityIndicator,
  Dimensions,
  TouchableHighlight,
  AsyncStorage,
  Linking,
  ToastAndroid,
  AlertIOS,
  BackHandler,
} from 'react-native';
import styles from '../styles/styles_profile';
//import Application_Constant from '../strings/string'
import Application_Constant from './localization';

import { Card } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { TextInput } from 'react-native-gesture-handler';
import Colors from '../colors/colors';
import ImagePicker from 'react-native-image-picker';
import PopupDialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';

import axios from 'axios';

import CheckBox from 'react-native-check-box';
import Image from 'react-native-fast-image';
import { Dropdown, Dropdown2 } from 'react-native-material-dropdown';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';

let drop_down_bloodGroup = [
  { value: 'A+' },
  { value: 'A-' },
  { value: 'B+' },
  { value: 'B-' },
  { value: 'AB+' },
  { value: 'AB-' },
  { value: 'O+' },
  { value: 'O-' },
];
var radio_props = [
  { label: 'Homme', value: 0 },
  { label: 'Femme', value: 1 },
  { label: 'Autre', value: 2 },
];

var radiogroup_language_options = [
  { key: 0, value: Application_Constant.language_french },
  { key: 1, value: Application_Constant.language_english },
];
// var radiogroup_options = [
//     { id: 0, label: 'Homme', value: 'M', checked: true, },
//     { id: 1, label: 'Femme', value: 'F', checked: false, },
// ];
let selectedItem;
var get_lang;
var langg;
import RNFetchBlob from 'rn-fetch-blob';


export default class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      telephone: '',
      username: '',
      shortname: 'JD',
      image_Url: '',
      profilePic: '',
      fileName: '',
      fsPath: '',
      binaryResponse: '',
      isImageAvailable: false,
      animating: false,
      lodingDialog: false,
      animating2: false,
      fullNameErr: '',
      emailErr: '',
      mobileErr: '',
      phonenumber: '+33 998877667',
      userId: '',
      token: '',
      gender: '',
      genderErr: '',
      height: '',
      heightErr: '',
      weight: '',
      weightErr: '',
      allergy: '',
      allergyErr: '',
      bloodGroup: '',
      bloodGroupErr: '',
      checked: false,
      checkedValue: 0,
      old_image: '',
      doctorinchrge_médecintraitant: '',
      doctorinchrge_médecintraitantErr: '',
      familiy_mesproches: '',
      familiy_mesprochesErr: '',
      dmp: '',
      dmpErr: '',
      medicalhistory_antécédentsmédicaux: '',
      medicalhistory_antécédentsmédicauxErr: '',
      checked: 1,
      radio_position: '',
      value: '',
      language: '',
      languageErr: '',
      presets:'',
      image_data_Url: '',


    };
  }
  notifyMessage(msg) {
    if (Platform.OS === 'android') {
      ToastAndroid.show(msg, ToastAndroid.SHORT);
      // this.props.navigation.navigate('ProfileScreen');
      this.props.navigation.navigate('ProfileScreen')

    } else {
      AlertIOS.alert(msg);
      this.props.navigation.navigate('ProfileScreen')
    }
  }
  callNumber = () => {
    // Linking.openURL(this.state.phonenumber);
    // let phoneNumber = '';

    let phoneNumber = 15;
    if (Platform.OS === 'android') {
      // phoneNumber = 'tel:${' + this.state.phonenumber + '}';
      phoneNumber = 'tel:${' + phoneNumber + '}';
    } else {
      // phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
      phoneNumber = 'telprompt:${' + phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };
  // componentWillMount() {
  //   BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  // }
  // componentWillUnmount() {
  //   BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);

  // }
  // handleBackButtonClick() {
  // //  this.props.navigation.navigate('ProfileScreen');

  // }
  componentDidMount() {
    get_lang = Application_Constant.getLanguage();
    console.log('Get Language EditProfile ', get_lang);

    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);

      // var useridd = JSON.parse(userid);
      // console.log("User ID^^^" + useridd)

      this.setState({
        userId: userid,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      console.log('User Token^^^' + tokenn);

      this.setState({
        token: tokenn,
      });

      console.log('User Token%%%' + this.state.token);
      this.setState({
        animating: true,
        lodingDialog: true,
      });
      axios
        .post(
          Application_Constant.BaseUrl + 'api/getProfile',
          {
            user_id: this.state.userId,
          },
          {
            headers: {
              'user-id': this.state.userId,
              token: this.state.token,
              'Content-Type': 'application/json',

              //other header fields
            },
          },
        )
        .then(response => response.data)
        .then(data => {
          console.log('Data is  ' + JSON.stringify(data));
          var res_data = data.error;
          console.log('Data Res is  ' + res_data);

          console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            this.setState({
              animating: false,
              lodingDialog: false,
            });
            alert(res_data_msg);
          } else if (res_data == 'false') {
            var usersDetailss = JSON.stringify(data.usersDetails);
            var usersfullname = data.usersDetails.fullname;
            var userstelephone = data.usersDetails.telephone;
            var usersemail = data.usersDetails.email;
            var usersGender = data.usersDetails.gender;
            var usersheight = data.usersDetails.height;
            var usersWeight = data.usersDetails.weight;
            var usersallergy = data.usersDetails.allergy;
            var userspublic_profile = data.usersDetails.public_profile;
            var usersblood_group = data.usersDetails.blood_group;
            var in_charge_doctor = data.usersDetails.in_charge_doctor;
            var userold_image = data.usersDetails.old_image;
            // let fullimagee = { uri: data.usersDetails.fullimage };
            var fullimagee = data.usersDetails.fullimage;

            var familiy_mesproches = data.usersDetails.family;
            var dmp = data.usersDetails.dmp;
            var medicalhistory_antécédentsmédicaux =
              data.usersDetails.medical_history;

            console.log('Get Gender value   ' + usersGender);

            if (
              usersGender != '' &&
              usersGender != 'null' &&
              usersGender != null
            ) {
              if (usersGender == 'Male' || usersGender == 'Homme') {
                console.log('Get Gender   ' + usersGender);

                this.setState({
                  animating: false,
                  lodingDialog: false,
                  username: usersfullname,
                  telephone: userstelephone,
                  email: usersemail,
                  gender: 'M',
                  genderErr: '',
                  height: usersheight,
                  heightErr: '',
                  weight: usersWeight,
                  weightErr: '',
                  allergy: usersallergy,
                  allergyErr: '',
                  bloodGroup: usersblood_group,
                  old_image: userold_image,
                  profilePic: fullimagee,
                  doctorinchrge_médecintraitant: in_charge_doctor,
                  familiy_mesproches: familiy_mesproches,
                  dmp: dmp,
                  medicalhistory_antécédentsmédicaux: medicalhistory_antécédentsmédicaux,
                  radio_position: 0,
                });

                console.log('Radio Position@@@  ' + this.state.radio_position);
              } else if (usersGender == 'Female' || usersGender == 'Femme') {
                this.setState({
                  animating: false,
                  lodingDialog: false,
                  username: usersfullname,
                  telephone: userstelephone,
                  email: usersemail,
                  gender: 'F',
                  genderErr: '',
                  height: usersheight,
                  heightErr: '',
                  weight: usersWeight,
                  weightErr: '',
                  allergy: usersallergy,
                  allergyErr: '',
                  old_image: userold_image,
                  bloodGroup: usersblood_group,
                  profilePic: fullimagee,
                  doctorinchrge_médecintraitant: in_charge_doctor,
                  familiy_mesproches: familiy_mesproches,
                  dmp: dmp,
                  medicalhistory_antécédentsmédicaux: medicalhistory_antécédentsmédicaux,
                  radio_position: 1,
                });

                console.log('Radio Position  ' + this.state.radio_position);
              } else if (usersGender == 'Autre' || usersGender == 'Other') {
                this.setState({
                  animating: false,
                  lodingDialog: false,
                  username: usersfullname,
                  telephone: userstelephone,
                  email: usersemail,
                  gender: 'F',
                  genderErr: '',
                  height: usersheight,
                  heightErr: '',
                  weight: usersWeight,
                  weightErr: '',
                  allergy: usersallergy,
                  allergyErr: '',
                  old_image: userold_image,
                  bloodGroup: usersblood_group,
                  profilePic: fullimagee,
                  doctorinchrge_médecintraitant: in_charge_doctor,
                  familiy_mesproches: familiy_mesproches,
                  dmp: dmp,
                  medicalhistory_antécédentsmédicaux: medicalhistory_antécédentsmédicaux,
                  radio_position: 2,
                });

                console.log('Radio Position  ' + this.state.radio_position);
              }
            }
            //       this.componentWillReceiveProps(this.state.radioButtons)
          }

          if (
            userspublic_profile != '' &&
            userspublic_profile != 'null' &&
            userspublic_profile != null
          ) {
            console.log('User Get Public Profile   ' + userspublic_profile);

            if (userspublic_profile == 0) {
              this.setState({
                checked: false,
                checkedValue: 0,
                old_image: userold_image,
                animating: false,
                lodingDialog: false,
                username: usersfullname,
                telephone: userstelephone,
                email: usersemail,

                height: usersheight,
                heightErr: '',
                weight: usersWeight,
                weightErr: '',
                allergy: usersallergy,
                profilePic: fullimagee,
                allergyErr: '',
                animating: false,
                bloodGroup: usersblood_group,
                doctorinchrge_médecintraitant: in_charge_doctor,
                familiy_mesproches: familiy_mesproches,
                dmp: dmp,
                medicalhistory_antécédentsmédicaux: medicalhistory_antécédentsmédicaux,

                lodingDialog: false,
              });
            } else if (userspublic_profile == 1) {
              this.setState({
                checked: true,
                checkedValue: 1,
                old_image: userold_image,
                animating: false,
                lodingDialog: false,

                username: usersfullname,
                telephone: userstelephone,
                email: usersemail,
                height: usersheight,
                heightErr: '',
                weight: usersWeight,
                weightErr: '',
                allergy: usersallergy,
                allergyErr: '',
                profilePic: fullimagee,
                bloodGroup: usersblood_group,
                doctorinchrge_médecintraitant: in_charge_doctor,
                old_image: userold_image,
                familiy_mesproches: familiy_mesproches,
                dmp: dmp,
                medicalhistory_antécédentsmédicaux: medicalhistory_antécédentsmédicaux,
              });
            }
          }

          if (
            usersfullname != '' &&
            usersfullname != 'null' &&
            usersfullname != null
          ) {
            console.log('User Get Gender   ' + usersfullname);

            this.setState({
              animating: false,
              lodingDialog: false,
              username: usersfullname,
              telephone: userstelephone,
              email: usersemail,
              old_image: userold_image,
              profilePic: fullimagee,
            });
          }

          var languagee = data.usersDetails.lang;

          if (languagee == 'fr') {
            this.setState({
              language: Application_Constant.language_french,
              languageErr: '',
            });
          } else if (languagee == 'en') {
            this.setState({
              language: Application_Constant.language_english,
              languageErr: '',
            });
          }
        });
    });
  }

  validation() {
    console.log('Checked Data is%%%   ' + this.state.checked);

    console.log('Checked Vaule is%%%   ' + this.state.checkedValue);

    var isValidate = 0;
    if (this.state.username != '') {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        fullNameErr: Application_Constant.ShouldemptyText,
      });
    }

    if (this.state.email != '') {
      isValidate += 1;
      if (this.validateEmail(this.state.email)) {
        isValidate += 1;
      } else {
        isValidate -= 1;
        this.setState({
          emailErr: Application_Constant.enter_emailid,
        });
      }
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        emailErr: Application_Constant.ShouldemptyText,
      });
    }
    if (this.state.telephone != '') {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        mobileErr: Application_Constant.ShouldemptyText,
      });
    }
    console.log('Gender is%%%   ' + this.state.gender);

    if (this.state.gender != '') {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        genderErr: Application_Constant.selectGender,
      });
    }
    console.log('Height is%%%   ' + this.state.height);

    if (
      this.state.height != '' &&
      this.state.height != null &&
      this.state.height != 'null'
    ) {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        heightErr: Application_Constant.ShouldemptyText,
      });
    }
    if (
      this.state.weight != '' &&
      this.state.weight != null &&
      this.state.weight != 'null'
    ) {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        weightErr: Application_Constant.ShouldemptyText,
      });
    }

    if (
      this.state.allergy != '' &&
      this.state.allergy != null &&
      this.state.allergy != 'null'
    ) {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        allergyErr: Application_Constant.ShouldemptyText,
      });
    }
    if (
      this.state.bloodGroup != '' &&
      this.state.bloodGroup != null &&
      this.state.bloodGroup != 'null'
    ) {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        bloodGroupErr: Application_Constant.ShouldemptyText,
      });
    }
    if (
      this.state.doctorinchrge_médecintraitant != '' &&
      this.state.doctorinchrge_médecintraitant != null &&
      this.state.doctorinchrge_médecintraitant != 'null'
    ) {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        doctorinchrge_médecintraitantErr: Application_Constant.ShouldemptyText,
      });
    }
    if (
      this.state.familiy_mesproches != '' &&
      this.state.familiy_mesproches != null &&
      this.state.familiy_mesproches != 'null'
    ) {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        familiy_mesprochesErr: Application_Constant.ShouldemptyText,
      });
    }
    if (
      this.state.dmp != '' &&
      this.state.dmp != null &&
      this.state.dmp != 'null'
    ) {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        dmpErr: Application_Constant.ShouldemptyText,
      });
    }
    if (
      this.state.medicalhistory_antécédentsmédicaux != '' &&
      this.state.medicalhistory_antécédentsmédicaux != null &&
      this.state.medicalhistory_antécédentsmédicaux != 'null'
    ) {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        medicalhistory_antécédentsmédicauxErr:
          Application_Constant.ShouldemptyText,
      });
    }
    console.log('isValidate ', isValidate);
    if (isValidate == 13) {
      console.log(
        'Use Login::::' + this.state.email + '==' + this.state.password,
      );

      if (Platform.OS == 'android') {


        this.editProfileApi();
      }
      else if (Platform.OS == 'ios') {
        this.editProfileApi2()
      }
      this.setState({
        animating: true,
        lodingDialog: true,
      });
    } else {
      // alert(strings.Pleasefillallthefields)
    }
  }
  editProfileApi() {
    console.log(this.state);

    var genn = '';
    if (
      this.state.gender == 'Homme' ||
      this.state.gender == 'Male' ||
      this.state.gender == 'M'
    ) {
      genn = 'M';
    } else if (
      this.state.gender == 'Femme' ||
      this.state.gender == 'Female' ||
      this.state.gender == 'F'
    ) {
      genn = 'F';
    } else if (
      this.state.gender == 'Autre' ||
      this.state.gender == 'Other' ||
      this.state.gender == 'O'
    ) {
      genn = 'O';
    }

    if (this.state.language == Application_Constant.language_french) {
      langg = 'fr';
    } else if (this.state.language == Application_Constant.language_english) {
      langg = 'en';
    }
    console.log('Checked Value Radio  ' + genn);

    let data = new FormData();
    data.append('user_id', this.state.userId);
    data.append('fullname', this.state.username);
    data.append('email', this.state.email);
    data.append('telephone', this.state.telephone);
    data.append('gender', genn);
    data.append('height', this.state.height);
    data.append('weight', this.state.weight);
    data.append('allergy', this.state.allergy);
    data.append('blood_group', this.state.bloodGroup);
    data.append('in_charge_doctor', this.state.doctorinchrge_médecintraitant);
    data.append('family', this.state.familiy_mesproches);
    data.append('dmp', this.state.dmp);
    data.append(
      'medical_history',
      this.state.medicalhistory_antécédentsmédicaux,
    );
    data.append('public_profile', this.state.checkedValue);
    data.append('lang', langg);

    if (
      this.state.fsPath != '' &&
      this.state.fsPath != 'null' &&
      this.state.fsPath != null
    ) {
      console.log('File Path   ' + this.state.fsPath);





      var fspathh = this.state.image_Url;
      data.append('image', {
        uri: Platform.OS === 'android' ? this.state.image_Url.uri : this.state.image_Url.uri.replace('file:///', ''),

        type: 'image/jpg',
        name: 'image.jpg',
      });

      //   data.append('image',fspathh);
      // data.append('image',
      //         {
      //             uri: fspathh,
      //             name: fileNamee,
      //             type: 'image/jpg'

      //         });
    } else {
      //  data.append('image', "")
      //   var fspathhh = "file://.";
      //   var photo = {
      //     uri: RN.wrap(fspathhh),
      //     type: 'image/jpg',
      //     name: 'image.jpg',
      //     };
      //   data.append('image', photo);
    }

    data.append('old_image', this.state.old_image);
    var axiosConfig = {
      headers: {
        'Access-Control-Allow-Origin': Application_Constant.BaseUrl,

        'Content-Type': 'multipart/form-data',
        'user-id': this.state.userId,
        token: this.state.token,
      },
    };
    let url = Application_Constant.BaseUrl + 'api/edituserprofile';

    axios
      .post(url, data, axiosConfig)
      .then(response => {
        console.log('response' + JSON.stringify(response));
        console.log(response.data);
        this.setState({
          animating: false,
          lodingDialog: false,
        });

        var res_data = response.data.error;
        console.log('res_data   ' + res_data);
        if (res_data == 'true') {
          var res_data_msg = response.data.errorMessage;
          console.log('Response True is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
          alert(res_data_msg);
        } else if (res_data == 'false') {
          console.log("Lang is ",langg);

          Application_Constant.setLanguage(langg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
       

          // this.notifyMessage('Profil mis à jour avec succès');
          this.props.navigation.navigate('ProfileScreen')

        }
        // this.componentDidMount();
      })
      .catch(error => {
        console.log('error');
        console.log(error);
        this.setState({
          animating: false,
          lodingDialog: false,
        });
      });
  }

  editProfileApi2() {

    console.log(this.state);

    var genn = '';
    if (
      this.state.gender == 'Homme' ||
      this.state.gender == 'Male' ||
      this.state.gender == 'M'
    ) {
      genn = 'M';
    } else if (
      this.state.gender == 'Femme' ||
      this.state.gender == 'Female' ||
      this.state.gender == 'F'
    ) {
      genn = 'F';
    } else if (
      this.state.gender == 'Autre' ||
      this.state.gender == 'Other' ||
      this.state.gender == 'O'
    ) {
      genn = 'O';
    }
    if (this.state.language == Application_Constant.language_french) {
      langg = 'fr';
    } else if (this.state.language == Application_Constant.language_english) {
      langg = 'en';
    }

    let headers = {
      'Content-Type': 'multipart/form-data',
      'User-Id': this.state.userId,
      'token': this.state.token,
    };

   RNFetchBlob.fetch('POST', Application_Constant.BaseUrl + 'api/edituserprofile', headers, [
      { name: 'user_id', data: this.state.userId },
      { name: 'email', data: this.state.email},
      { name: 'fullname', data: this.state.username},
      { name: 'telephone', data: this.state.telephone},
      { name: 'gender', data:genn},
      { name: 'height', data: this.state.height},
      { name: 'weight', data: this.state.weight},
      { name: 'allergy', data: this.state.allergy},
      { name: 'dmp', data: this.state.dmp},

      { name: 'blood_group', data: this.state.bloodGroup},
      { name: 'in_charge_doctor', data: this.state.doctorinchrge_médecintraitant},
      { name: 'family', data: this.state.familiy_mesproches},
      { name: 'medical_history', data: this.state.medicalhistory_antécédentsmédicaux},
      { name: 'public_profile', data: this.state.dmp},
      { name: 'lang', data: langg},

      { name: 'old_image', data: null},
      { name: 'image', filename: 'photo.jpg', type: 'image/png', data: this.state.image_data_Url},

    ],

    )
    .uploadProgress((written, total) => {
      let presenteg = Math.floor((written * 100) / total)
      console.log('uploaded', written / total +" presente"+presenteg +"%")
      this.setState({
        presets:presenteg
      });
  })
  // listen to download progress event
  .progress((received, total) => {
      console.log('progress', received 
      / total)
  })

    .then((resp) => {

      console.log("response:::::::" + resp.text());
      var res_txt= resp.text()
   //   alert(res_txt)
   console.log("Lang is ", langg);
 Application_Constant.setLanguage(langg);
    
 this.setState({
        animating: false,
        lodingDialog: false,
      });
   this.props.navigation.navigate('ProfileScreen')


    }).catch((err) => {
      this.setState({
        animating: false,
        lodingDialog: false,
      });
      console.log("response::::err:::" + err);
    });




  }


  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  selectProfilePic = () => {
    const options = {
      title: 'Select Avatar',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        const imageUrl = response.uri;
        var fileName = response.uri.substring(
          response.uri.lastIndexOf('/') + 1,
        );
        // var fsPath = response.fileName;
        var binaryResponse = response.data;
        console.log('Filename is' + fileName);
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        AsyncStorage.setItem('profilePic', JSON.stringify(source));
        console.log('Filename Source is' + source);
        // if (Platform.OS === "android")
        // {
        //   this.setState({
        //     image_Url: response.uri,

        //   })

        // }
        // else if(Platform.OS == 'ios')
        // {
        //   this.setState({
        //     image_Url : response.uri.replace("file://", ""), 

        //   })

        // }

        this.setState({
          image_Url: response,
          image_data_Url: response.data,
          profilePic: response.uri,
          fileName: fileName,
          fsPath: response.data,
          binaryResponse: binaryResponse,
          isImageAvailable: true,
        });
        console.log('image camera====' + this.state.image);
        // console.log("image url::" + JSON.stringify(source))
      }
    });
  };
  onSelect(option) {
    console.log('Option   ' + JSON.stringify(option));

    var opp = JSON.stringify(option);
    console.log('Value   ' + opp);
    var opp2 = JSON.stringify(option.value);
    console.log('Value2   ' + opp2);

    this.setState({
      gender: opp2,
      genderErr: '',
    });
  }
  onChangeCheck() {
    this.setState({ checked: !this.state.checked });
    console.log('Checked Data is   ' + this.state.checked);
    if (this.state.checked == true) {
      console.log('Checked Data is###  ' + this.state.checked);
      this.setState({ checkedValue: 0 });
    } else if (this.state.checked == false) {
      console.log('Checked Data is^^^  ' + this.state.checked);

      this.setState({ checkedValue: 1 });
    }
  }

  imageLoadingStart(val) {
    this.setState({
      animating2: val,
    });
  }
  onChangeTextPress(value) {
    console.log('Blood Group Value is  ' + value);
    this.setState({
      bloodGroup: value,
      bloodGroupErr: '',
    });
  }

  onChangeTextPress2(value) {
    console.log('Language Value is  ' + value);
    this.setState({
      language: value,
      languageErr: '',
    });
  }
  render() {
    console.log('Radio Position Render  ' + this.state.radio_position);

    return (
      <View style={styles.container}>
        <ScrollView>
          <View>
            <Image
              source={require('../assets/box_profile_icon.png')}
              style={{
                width: '100%',
                backgroundColor: 'white',
                height: 300,
                flexDirection: 'row',
              }}
              resizeMode="stretch">
              <TouchableOpacity
                onPress={() => this.selectProfilePic()}
                style={{
                  width: '100%',
                  height: '100%',
                  alignItems: 'center',
                  marginBottom: 40,
                }}>
                {!!this.state.profilePic &&
                  this.state.profilePic !=
                  'https://docturgences.com/public/upload/usersprofile/' &&
                  this.state.profilePic !=
                  'https://docturgences.com/public/upload/usersprofile/null' && (
                    <Image
                      style={{
                        width: 158,
                        height: 158,
                        borderRadius: 60,
                        borderWidth: 0,
                        borderColor: 'white',
                        marginTop: 140,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      source={{
                        uri: this.state.profilePic,
                        priority: Image.priority.high,
                      }}
                      onLoadStart={e => {
                        console.log('Loading Start');

                        this.imageLoadingStart(true);
                      }}
                      onProgress={e =>
                        console.log(
                          'Loading Progress ' +
                          e.nativeEvent.loaded / e.nativeEvent.total,
                        )
                      }
                      onLoad={e =>
                        console.log(
                          'Loading Loaded ' + e.nativeEvent.width,
                          e.nativeEvent.height,
                        )
                      }
                      onLoadEnd={e => {
                        console.log('Loading Ended');
                        this.imageLoadingStart(false);
                      }}>
                      <ActivityIndicator
                        animating={this.state.animating2}
                        visible={this.state.animating2}
                        style={[{ width: 30, height: 30 }]}
                        color="#C00"
                        size="small"
                        hidesWhenStopped={true}
                      />
                    </Image>
                  )}
                {this.state.profilePic ==
                  'http://docturgences.com/public/upload/usersprofile/' && (
                    <Image
                      style={{
                        width: 158,
                        height: 158,
                        borderRadius: 60,
                        borderWidth: 0,
                        borderColor: 'white',
                        marginTop: 140,
                      }}
                      // source={{ uri: 'https://bootdey.com/img/Content/avatar/avatar6.png' }}
                      // source={require('../assets/profile_picture.png')}
                      source={require('../assets/profile_picture.png')}
                    />
                  )}
              </TouchableOpacity>
            </Image>
            <TouchableOpacity
              onPress={() => this.selectProfilePic()}
              style={{
                width: '100%',
                flexDirection: 'row-reverse',
                marginTop: -3,
              }}>
              <Image
                style={{
                  width: 35,
                  height: 35,
                  marginRight: 120,
                }}
                source={require('../assets/camera_icon.png')}
              />
            </TouchableOpacity>

            <View
              style={{
                marginBottom: 20,
                flexDirection: 'column',
                padding: 5,
              }}>
              <Text
                style={{
                  width: '100%',
                  textAlign: 'center',
                  fontSize: 18,
                  color: 'black',
                  paddingTop: 5,
                  fontFamily: 'Poppins-Medium',
                }}>
                {this.state.username}
              </Text>

              <Card
                style={{
                  marginLeft: 20,
                  marginRight: 20,
                  paddingLeft: 20,
                  paddingTop: 30,
                  borderRadius: 20,
                  paddingBottom: 20,
                }}>
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: 'Poppins-Regular',
                    color: 'grey',
                  }}>
                  {Application_Constant.firstname}
                </Text>

                <TextInput
                  style={{
                    fontFamily: 'Poppins-Regular',
                    fontSize: 14,
                    paddingTop: 4,
                  }}
                  editable
                  value={this.state.username}
                  underlineColorAndroid={Colors.COLOR.lightBlue}
                  placeholder="John Doe"
                  onChangeText={username =>
                    this.setState({ username, fullNameErr: '' })
                  }
                />
                {!!this.state.fullNameErr && (
                  <Text
                    style={{
                      color: 'red',
                      marginLeft: 7,
                      marginTop: -5,
                      fontSize: 12,
                    }}>
                    {this.state.fullNameErr}
                  </Text>
                )}
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: 'Poppins-Regular',
                    color: 'grey',
                    marginTop: 20,
                  }}>
                  {Application_Constant.AddresssMail}
                </Text>

                <TextInput
                  style={{
                    fontFamily: 'Poppins-Regular',
                    fontSize: 14,
                    paddingTop: 4,
                  }}
                  value={this.state.email}
                  editable
                  // value={this.state.email}
                  keyboardType="email-address"
                  underlineColorAndroid={Colors.COLOR.lightBlue}
                  placeholder="johndoe@gmail.com"
                  onChangeText={email => this.setState({ email, emailErr: '' })}
                />

                {!!this.state.emailErr && (
                  <Text
                    style={{
                      color: 'red',
                      marginLeft: 7,
                      marginTop: -5,
                      fontSize: 12,
                    }}>
                    {this.state.emailErr}
                  </Text>
                )}
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: 'Poppins-Regular',
                    color: 'grey',
                    marginTop: 20,
                  }}>
                  {Application_Constant.Telephone}
                </Text>
                <TextInput
                  style={{
                    fontFamily: 'Poppins-Regular',
                    fontSize: 14,
                    paddingTop: 4,
                  }}
                  value={this.state.telephone}
                  editable
                  underlineColorAndroid={Colors.COLOR.lightBlue}
                  keyboardType="phone-pad"
                  placeholder="+33 987654321"
                  onChangeText={telephone =>
                    this.setState({ telephone, mobileErr: '' })
                  }>
                  {/* {this.state.telephone} */}
                </TextInput>
                {!!this.state.mobileErr && (
                  <Text
                    style={{
                      color: 'red',
                      marginLeft: 7,
                      marginTop: -5,
                      fontSize: 12,
                    }}>
                    {this.state.mobileErr}
                  </Text>
                )}

                {/* <RadioGroup
                                        horizontal
                                        style={{ flexDirection: "row", padding: 40, marginTop: 20, }}
                                        options={radiogroup_options}
                                        radioButtons={selectedItem}
                                        // onPress={radioButtons => this.setState({ radioButtons })}

                                        onChange={(option) => this.onSelect(option)}
                                        flexDirection='row'
                                        circleStyle={{ fillColor: '#6FB8EF', borderColor: 'grey' }}
                                    /> */}

                {this.state.radio_position ? (
                  <View style={{ marginTop: 20 }}>
                    <Text
                      style={{ fontFamily: 'Poppins-Regular', paddingBottom: 4 }}>
                      {Application_Constant.gender_Selection}
                    </Text>

                    <RadioForm
                      radio_props={radio_props}
                      initial={this.state.radio_position}
                      buttonSize={12}
                      onPress={value => {
                        this.setState({
                          value: value,
                          gender: value,
                          genderErr: '',
                        });
                      }}
                      style={{}}
                    />
                    {!!this.state.genderErr && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.genderErr}
                      </Text>
                    )}
                  </View>
                ) : null}

                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'Poppins-Regular',
                      color: 'grey',
                    }}>
                    {Application_Constant.height}
                  </Text>
                  <TextInput
                    value={this.state.height}
                    style={{
                      fontFamily: 'Poppins-Regular',
                      fontSize: 14,
                      paddingTop: 4,
                    }}
                    editable
                    underlineColorAndroid={Colors.COLOR.lightBlue}
                    keyboardType="phone-pad"
                    placeholder="40"
                    onChangeText={height =>
                      this.setState({ height, heightErr: '' })
                    }
                  />
                  {!!this.state.heightErr && (
                    <Text
                      style={{
                        color: 'red',
                        marginLeft: 10,
                        marginTop: -5,
                        fontSize: 12,
                      }}>
                      {this.state.heightErr}
                    </Text>
                  )}
                </View>
                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'Poppins-Regular',
                      color: 'grey',
                    }}>
                    {Application_Constant.weight}
                  </Text>
                  <TextInput
                    style={{
                      fontFamily: 'Poppins-Regular',
                      fontSize: 14,
                      paddingTop: 4,
                    }}
                    value={this.state.weight}
                    editable
                    underlineColorAndroid={Colors.COLOR.lightBlue}
                    keyboardType="phone-pad"
                    placeholder="40"
                    onChangeText={weight =>
                      this.setState({ weight, weightErr: '' })
                    }
                  />
                  {!!this.state.weightErr && (
                    <Text
                      style={{
                        color: 'red',
                        marginLeft: 10,
                        marginTop: -5,
                        fontSize: 12,
                      }}>
                      {this.state.weightErr}
                    </Text>
                  )}
                </View>

                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'Poppins-Regular',
                      color: 'grey',
                    }}>
                    {Application_Constant.allergie}
                  </Text>
                  <TextInput
                    style={{
                      fontFamily: 'Poppins-Regular',
                      fontSize: 14,
                      paddingTop: 4,
                    }}
                    value={this.state.allergy}
                    editable
                    underlineColorAndroid={Colors.COLOR.lightBlue}
                    placeholder="Food Allergy"
                    onChangeText={allergy =>
                      this.setState({ allergy, allergyErr: '' })
                    }>
                    {/* {this.state.telephone} */}
                  </TextInput>
                  {!!this.state.allergyErr && (
                    <Text
                      style={{
                        color: 'red',
                        marginLeft: 10,
                        marginTop: -5,
                        fontSize: 12,
                      }}>
                      {this.state.allergyErr}
                    </Text>
                  )}
                </View>
                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'Poppins-Regular',
                      color: 'grey',
                    }}>
                    {Application_Constant.blood_group}
                  </Text>
                  {/* <TextInput style={{
                                        fontFamily: "Poppins-Regular",
                                        fontSize: 14,
                                        paddingTop: 4,
                                    }}
                                        value={this.state.bloodGroup}
                                        editable
                                        underlineColorAndroid={Colors.COLOR.lightBlue}
                                        placeholder="B+"
                                        onChangeText={(bloodGroup) => this.setState({ bloodGroup, bloodGroupErr: '' })}
                                    >
                                    </TextInput>
                                    {!!this.state.bloodGroupErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, marginTop: -5, fontSize: 12, }}>{this.state.bloodGroupErr}</Text>
                                    )} */}

                  <Dropdown
                    style={{ flex: 1 }}
                    label={Application_Constant.blood_group}
                    data={drop_down_bloodGroup}
                    value={this.state.bloodGroup}
                    rippleCentered={true}
                    valueExtractor={({ value }) => value}
                    onChangeText={value => {
                      this.onChangeTextPress(value);
                    }}
                  />
                  {!!this.state.bloodGroupErr && (
                    <Text
                      style={{
                        color: 'red',
                        marginLeft: 10,
                        marginTop: -5,
                        fontSize: 12,
                      }}>
                      {this.state.bloodGroupErr}
                    </Text>
                  )}
                </View>
                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'Poppins-Regular',
                      color: 'grey',
                    }}>
                    {Application_Constant.doctorinchrge_médecintraitant}
                  </Text>
                  <TextInput
                    style={{
                      fontFamily: 'Poppins-Regular',
                      fontSize: 14,
                      paddingTop: 4,
                    }}
                    value={this.state.doctorinchrge_médecintraitant}
                    editable
                    underlineColorAndroid={Colors.COLOR.lightBlue}
                    placeholder="Dr. Abc"
                    onChangeText={doctorinchrge_médecintraitant =>
                      this.setState({
                        doctorinchrge_médecintraitant,
                        doctorinchrge_médecintraitantErr: '',
                      })
                    }
                  />
                  {!!this.state.doctorinchrge_médecintraitantErr && (
                    <Text
                      style={{
                        color: 'red',
                        marginLeft: 10,
                        marginTop: -5,
                        fontSize: 12,
                      }}>
                      {this.state.doctorinchrge_médecintraitantErr}
                    </Text>
                  )}
                </View>
                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'Poppins-Regular',
                      color: 'grey',
                    }}>
                    {Application_Constant.familiy_mesproches}
                  </Text>
                  <TextInput
                    style={{
                      fontFamily: 'Poppins-Regular',
                      fontSize: 14,
                      paddingTop: 4,
                    }}
                    value={this.state.familiy_mesproches}
                    editable
                    underlineColorAndroid={Colors.COLOR.lightBlue}
                    placeholder="Qwerty"
                    onChangeText={familiy_mesproches =>
                      this.setState({
                        familiy_mesproches,
                        familiy_mesprochesErr: '',
                      })
                    }
                  />
                  {!!this.state.familiy_mesprochesErr && (
                    <Text
                      style={{
                        color: 'red',
                        marginLeft: 10,
                        marginTop: -5,
                        fontSize: 12,
                      }}>
                      {this.state.familiy_mesprochesErr}
                    </Text>
                  )}
                </View>
                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'Poppins-Regular',
                      color: 'grey',
                    }}>
                    {Application_Constant.dmp}
                  </Text>
                  <TextInput
                    style={{
                      fontFamily: 'Poppins-Regular',
                      fontSize: 14,
                      paddingTop: 4,
                    }}
                    value={this.state.dmp}
                    editable
                    underlineColorAndroid={Colors.COLOR.lightBlue}
                    placeholder="Qwerty"
                    onChangeText={dmp => this.setState({ dmp, dmpErr: '' })}
                  />
                  {!!this.state.dmpErr && (
                    <Text
                      style={{
                        color: 'red',
                        marginLeft: 10,
                        marginTop: -5,
                        fontSize: 12,
                      }}>
                      {this.state.dmpErr}
                    </Text>
                  )}
                </View>
                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'Poppins-Regular',
                      color: 'grey',
                    }}>
                    {Application_Constant.medicalhistory_antécédentsmédicaux}
                  </Text>
                  <TextInput
                    style={{
                      fontFamily: 'Poppins-Regular',
                      fontSize: 14,
                      paddingTop: 4,
                    }}
                    value={this.state.medicalhistory_antécédentsmédicaux}
                    editable
                    underlineColorAndroid={Colors.COLOR.lightBlue}
                    placeholder="Qwerty"
                    onChangeText={medicalhistory_antécédentsmédicaux =>
                      this.setState({
                        medicalhistory_antécédentsmédicaux,
                        medicalhistory_antécédentsmédicauxErr: '',
                      })
                    }
                  />
                  {!!this.state.medicalhistory_antécédentsmédicauxErr && (
                    <Text
                      style={{
                        color: 'red',
                        marginLeft: 10,
                        marginTop: -5,
                        fontSize: 12,
                      }}>
                      {this.state.medicalhistory_antécédentsmédicauxErr}
                    </Text>
                  )}
                </View>

                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{ fontFamily: 'Poppins-Regular', paddingBottom: 4 }}>
                    {Application_Constant.slct_language}
                  </Text>
                  <Dropdown
                    style={{ flex: 1 }}
                    label={Application_Constant.slct_language}
                    data={radiogroup_language_options}
                    value={this.state.language}
                    rippleCentered={true}
                    valueExtractor={({ value }) => value}
                    onChangeText={value => {
                      this.onChangeTextPress2(value);
                    }}
                  />
                  {!!this.state.languageErr && (
                    <Text
                      style={{
                        color: 'red',
                        marginLeft: 10,
                        marginTop: -5,
                        fontSize: 12,
                      }}>
                      {this.state.languageErr}
                    </Text>
                  )}
                </View>
                <View style={{ marginTop: 20, flexDirection: 'row' }}>
                  <CheckBox
                    style={{ paddingRight: 4 }}
                    // value={this.state.checked}
                    checkBoxColor="#6FB8EF"
                    onClick={() => this.onChangeCheck()}
                    isChecked={this.state.checked}
                    leftText={''}
                  />

                  <Text
                    style={{
                      fontFamily: 'Poppins-Regular',
                      fontSize: 12,
                      paddingRight: 4,
                    }}>
                    {Application_Constant.public_view}{' '}
                  </Text>
                </View>
              </Card>

              <Card
                style={{
                  marginLeft: 20,
                  marginRight: 20,
                  paddingLeft: 20,
                  paddingTop: 20,
                  borderRadius: 20,
                  paddingBottom: 20,
                  marginBottom: 10,
                  marginTop: 20,
                  flexDirection: 'column',
                }}>
                <Text
                  style={{
                    fontSize: 13,
                    fontFamily: 'Poppins-Medium',
                  }}>
                  {Application_Constant.mon_medecin_traitant}
                </Text>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    marginTop: 10,
                  }}>
                  <TouchableHighlight
                    style={{
                      borderRadius:
                        Math.round(
                          Dimensions.get('window').width +
                          Dimensions.get('window').height,
                        ) / 2,
                      width: 30,
                      height: 30,
                      backgroundColor: '#d3d3d3',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    underlayColor="#ccc"
                    onPress={() => alert(this.state.shortname)}>
                    <Text
                      style={{
                        fontSize: 12,
                        fontFamily: 'Poppins-Regular',
                        color: 'gray',
                      }}>
                      {this.state.shortname}
                    </Text>
                  </TouchableHighlight>

                  <Text
                    style={{
                      fontFamily: 'Poppins-Regular',
                      fontSize: 13,
                      paddingLeft: 11,
                      textAlign: 'center',
                      paddingTop: 5,
                    }}>
                    {this.state.username}
                  </Text>
                </View>
                <TouchableOpacity
                  style={{
                    borderRadius: 15,
                    marginLeft: 10,
                    marginTop: 20,
                    marginRight: 10,
                  }}
                //   onPress={() =>
                //   this.props.navigation.navigate('Doctor_Information')
                  
                // }
                  >
                  <LinearGradient
                    style={{
                      borderRadius: 15,
                      padding: 15,
                      flexDirection: 'row',
                    }}
                    colors={['#7CB5F8', '#4C94E6']}>
                    <Image
                      style={{ width: 20, height: 20 }}
                      source={require('../assets/plus_icon.png')}
                    />

                    <Text
                      style={{
                        width: '100%',
                        fontFamily: 'Poppins-Bold',
                        paddingLeft: 12,
                        color: 'white',
                        fontSize: 11,
                        paddingRight: 5,
                      }}>
                      {Application_Constant.modifie_man_medicine}
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </Card>
              <TouchableOpacity
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 0,
                  marginBottom: 20,
                }}
                onPress={() => this.validation()}>
                <LinearGradient
                  style={{
                    backgroundColor: 'white',
                    width: 150,
                    borderRadius: 15,
                    padding: 15,
                    marginTop: 20,
                  }}
                  colors={['#7CB5F8', '#4C94E6']}>
                  <Text
                    style={{
                      width: '100%',
                      fontFamily: 'Poppins-Bold',
                      color: 'white',
                      fontSize: 16,
                      textAlign: 'center',
                    }}>
                    {Application_Constant.update}
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

        {/* <TouchableOpacity
                    style={{
                        bottom: 0,
                        height: 70,
                    }}
                    onPress={this.callNumber}
                >
                    <LinearGradient
                        style={{
                            flexDirection: 'row',
                            backgroundColor: 'white',
                            borderTopLeftRadius: 27,
                            borderTopRightRadius: 27,
                            padding: 20,
                            alignItems: "center",

                        }}
                        colors={['#7CB5F8', '#4C94E6']}
                        start={{ x: 0, y: 1 }}
                        end={{ x: 1, y: 1 }}>
                        <TouchableOpacity style={{ width: '100%', flexDirection: 'row', alignItems: "center", marginRight: 5, marginLeft: 5, paddingLeft: 10, }}
                            onPress={this.callNumber} >

                            <Image style={{ width: 40, height: 40, }}
                                source={require('../assets/call_icon.png')} ></Image>
                            <Text style={{ width: "100%", fontFamily: "Poppins-Regular", paddingLeft: 0, marginRight: 5, color: "white", flex: 1, fontSize: 13, paddingRight: 5, marginLeft: 15 }}>Composer le 15 si vous ete en situation d`urgence </Text>
                        </TouchableOpacity>
                    </LinearGradient>

                </TouchableOpacity> */}
             <PopupDialog
              onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
              width={0.3}
              visible={this.state.lodingDialog}
              dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
              <DialogContent>
                <View style={{ alignItems: 'center', }}>
                  <ActivityIndicator
                    animating={this.state.animating}
                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                    color="#C00"
                    size="large"
                    hidesWhenStopped={true}
                  />
                  <Text>{this.state.presets} %</Text>
                </View>
              </DialogContent>
            </PopupDialog>


      </View>
    );
  }
}
