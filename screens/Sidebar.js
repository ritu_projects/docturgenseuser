import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  Profile,
  ActivityIndicator,
  Alert,
  MenuItem,
  ScrollView,
  DrawerLayoutAndroid,
  Linking,
  AsyncStorage,
  Dimensions
} from 'react-native';
import MapView from 'react-native-maps';
import styles from '../styles/styles_navigation';
import { Card, CardItem, Icon, Thumbnail, Row } from 'native-base';
import Colors from '../colors/colors';
import PropTypes from 'prop-types';
import { NavigationActions } from 'react-navigation';
import Application_Constant from './localization';
import Image from 'react-native-fast-image';
import PopupDialog, {
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import axios from 'axios';
const { width, height } = Dimensions.get("window");

import { SafeAreaView } from 'react-native-safe-area-context';

const STATUSBAR_HEIGHT = StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

class Sidebar extends Component {
  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    this.props.navigation.dispatch(navigateAction);
  };

  constructor(props) {
    super(props);
    this.state = {
      AddresssMail: Application_Constant.AddresssMail,
      phonenumber: '+33 998877667',
      userId: '',
      session: '',
      username: '',
      profile_pic: '',
      token: '',
      animating: false,
      lodingDialog: false,
      animating2: false,
      about_percent: ',',
    };
  }
  componentDidMount() {
    AsyncStorage.getItem('phonenumber').then(phonenumber => {
      console.log('Phonenumber  ' + phonenumber);

      this.setState({
        phonenumber: phonenumber,
      });
    });
    this.checkCondition();
    // const {navigation} = this.props;
    // navigation.addListener ('willFocus', () =>{
    //   console.log("willFocus ","willFocus ")
    // // do whatever you want to do when focused
    // this.checkCondition();

    //});
    // this.checkCondition();
  }
  componentWillReceiveProps() {
    this.checkCondition();
  }
  checkCondition() {
    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);
      var useridd = JSON.parse(userid);
      console.log('User ID@@@' + useridd);

      if (userid == '' || userid == null || userid == 'null') {
        this.setState({
          userId: '',
          session: '',
          username: 'Connexion',
          profilePic: 'http://docturgences.com/public/upload/usersprofile/',
        });
      } else {
        this.setState({
          userId: userid,
          session: '',
        });
        this.getUserDetails();
      }
    });
  }
  getUserDetails() {
    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);

      var useridd = JSON.parse(userid);
      console.log('User ID^^^' + useridd);

      this.setState({
        userId: useridd,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      console.log('User Token^^^' + tokenn);

      this.setState({
        token: tokenn,
      });

      console.log('User Token%%%' + this.state.token);
      // this.setState({
      //   animating: true,
      //   lodingDialog: true,

      // });
      axios
        .post(
          Application_Constant.BaseUrl + 'api/getProfile',
          {
            user_id: this.state.userId,
          },
          {
            headers: {
              'user-id': this.state.userId,
              token: this.state.token,
              'Content-Type': 'application/json',

              //other header fields
            },
          },
        )
        .then(response => response.data)
        .then(data => {
          console.log('Data GetProfile Sidebar is  ' + JSON.stringify(data));
          var res_data = data.error;
          console.log('Data Res is  ' + res_data);

          console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            this.setState({
              animating: false,
              lodingDialog: false,
            });
            alert(res_data_msg);
          } else if (res_data == 'false') {
            var usersfullname = data.usersDetails.fullname;
            var usersfullimg = data.usersDetails.fullimage;

            console.log('Full Image   ' + usersfullimg);

            this.setState({
              animating: false,
              lodingDialog: false,
              username: usersfullname,
              profilePic: usersfullimg,
            });
          }
        });
    });

    this.getAbout();
  }

  getAbout() {
    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);

      var useridd = JSON.parse(userid);
      console.log('User ID^^^' + useridd);

      this.setState({
        userId: useridd,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      console.log('User Token^^^' + tokenn);

      this.setState({
        token: tokenn,
      });

      console.log('User Token%%%' + this.state.token);
      // this.setState({
      //   animating: true,
      //   lodingDialog: true,

      // });
      axios
        .get(Application_Constant.BaseUrl + 'api/about', {
          headers: {
            'user-id': this.state.userId,
            token: this.state.token,
            'Content-Type': 'application/json',

            //other header fields
          },
        })
        .then(response => response.data)
        .then(data => {
          console.log('Data GetAbout  Specialst is  ' + JSON.stringify(data));
          var res_data = data.error;
          console.log('Data Res is  ' + res_data);

          console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            this.setState({
              animating: false,
              lodingDialog: false,
            });
            alert(res_data_msg);
          } else if (res_data == 'false') {
            var about_percent = data.percentage;

            console.log('About Percent   ' + about_percent);

            this.setState({
              animating: false,
              lodingDialog: false,
              about_percent: about_percent,
            });
          }
        });
    });
  }

  footerClick = () => {
    //this.props.navigation.navigate('AfterLogin');

    this.props.navigation.navigate('AfterLoginNew');
  };

  profileClick = () => {
    if (this.state.userId == '' || this.state.userId == null) {
      this.props.navigation.navigate('Connexion_Login');
    } else {
      this.props.navigation.navigate('ProfileScreen');
    }
  };
  Premiers_Pas = () => {
    this.props.navigation.navigate('Premiers_Pas');
  };

  HomeClick = () => {
    if (this.state.userId == '' || this.state.userId == null) {
      this.props.navigation.navigate('MapExample');
    } else {
      //  this.props.navigation.navigate('AfterLogin');
      this.props.navigation.navigate('AfterLoginNew');
    }
  };

  logoutAlert() {
    Alert.alert(
      Application_Constant.alert_title,
      Application_Constant.alert_info,

      [
        {
          text: Application_Constant.logout,
          onPress: () => {
            this.logout();
          },
          style: 'cancel',
        },
        {
          text: Application_Constant.cancel,
          onPress: () => console.log('cancel Pressed'),
          style: 'cancel',
        },
      ],
      { cancelable: true },
    );
  }
  logout = () => {
    AsyncStorage.setItem('userid', '');
    AsyncStorage.setItem('usersDetails', '');
    AsyncStorage.setItem('token', '');
    Application_Constant.setLanguage('fr');

    this.props.navigation.navigate('MapExample');
  };
  openURL = () => {
    Linking.openURL('https://www.dmp.fr/?xtor=SEC-61-GOO');
  };

  openURL3 = () => {
    //  Linking.openURL('https://www.docturgences.com/tuto')
    this.props.navigation.navigate('Webview');

  };

  openURL4 = () => {
    //  Linking.openURL('https://www.doctipharma.fr/');
    Linking.openURL('https://www.medadom.com/login')
  };
  callNumber = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };
  openURL2 = () => {
    Linking.openURL('https://www.docturgences.com/about-us');
  };
  openURL5 = () => {
    this.props.navigation.navigate('WebviewMention_legas');
  };
  imageLoadingStart(val) {
    this.setState({
      animating2: val,
    });
  }
  imageLoadingEnd(val) {
    this.setState({
      animating2: false,
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View
        style={styles.container}>
        {/* <StatusBar
          translucent={true}
          backgroundColor={'#324048'}
          barStyle={'dark-content'} /> */}
        <View style={{
          width: '100%',
          height: '100%',
        }}
        >

          <View
            // source={require('../assets/box_profile_icon.png')}
            style={{
              backgroundColor: '#01a0f3',
              height: "30%",

              justifyContent: 'center',
              alignItems: 'center',
              zIndex: 1,
            }}>

            <Image
              style={{
                width: 155,
                height: 71,
              }}
              source={require('../assets/newlogoo.png')}
            />



          </View>


          <TouchableOpacity
            style={{
              flexDirection: 'row',
              width: '100%',
              height: '8.75%',
              marginTop: 10,
            }}
            onPress={this.HomeClick}>
            <Image
              source={require('../assets/location_icon.png')}
              style={{ width: 20, height: 20, marginLeft: 20 }}
            />

            <Text
              style={{
                marginLeft: 15,
                textAlign: 'center',

                fontFamily: 'Poppins-Medium',
              }}>
              {Application_Constant.sidebar_home}
                </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              width: '100%',
              height: '8.75%',
            }}
            onPress={this.profileClick}>
            <Image
              source={require('../assets/inscription_login_icon.png')}
              style={{ width: 20, height: 20, marginLeft: 20 }}
            />

            <Text
              style={{
                marginLeft: 15,
                textAlign: 'center',

                fontFamily: 'Poppins-Medium',
              }}>
              {Application_Constant.sidebar_profile}
                </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              flexDirection: 'row',
              width: '100%',
              height: '8.75%',
            }}
            onPress={this.openURL}
          // onPress={()=> this.openURL}
          >
            <Image
              source={require('../assets/medicalhistory_antecedentsmedicus_icon.png')}
              style={{ width: 20, height: 20, marginLeft: 20 }}
            />

            <Text
              style={{
                marginLeft: 15,
                textAlign: 'center',
                fontFamily: 'Poppins-Medium',
              }}>
              {Application_Constant.sidebar_dmp}
                </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              width: '100%',
              height: '8.75%',

            }}
            onPress={this.openURL4}>
            <Image
              source={require('../assets/about_us_icon.png')}
              style={{ width: 20, height: 20, marginLeft: 20 }}
            />

            <Text
              style={{
                marginLeft: 15,
                textAlign: 'center',
                fontFamily: 'Poppins-Medium',
              }}>
              {Application_Constant.sidebar_teleconsult}
                </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              width: '100%',
              height: '8.75%',
              //  marginTop: 5,
            }}
            onPress={this.openURL3}>
            <Image
              source={require('../assets/about_us_icon.png')}
              style={{ width: 20, height: 20, marginLeft: 20 }}
            />

            <Text
              style={{
                marginLeft: 15,
                textAlign: 'center',
                fontFamily: 'Poppins-Medium',
              }}>
              {Application_Constant.sidebar_howwork_comment}
                </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              width: '100%',
              height: '8.75%',

            }}
            onPress={this.openURL5}>
            <Image
              source={require('../assets/about_us_icon.png')}
              style={{ width: 20, height: 20, marginLeft: 20 }}
            />
            <Text
              style={{
                marginLeft: 15,
                textAlign: 'center',
                fontFamily: 'Poppins-Medium',
              }}>
              {Application_Constant.sidebar_mentonlegal}
              </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              width: '100%',
              height: '8.75%',
              //  marginTop: 5,
            }}
            onPress={() => this.props.navigation.navigate('FAQ_LesQuestions')}>
            <Image
              source={require('../assets/faq_icon.png')}
              style={{ width: 20, height: 20, marginLeft: 20 }}
            />

            <Text
              style={{
                marginLeft: 15,
                textAlign: 'center',

                fontFamily: 'Poppins-Medium',
              }}>
              {Application_Constant.sidebar_FAQ}
            </Text>
          </TouchableOpacity>


          <View style={{
            bottom: 30,
            position: 'absolute',
            width: '100%',


          }}>
            {!!this.state.userId && (
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  //   height: 25,
                  //   bottom: 10,
                  width: '100%',


                }}
                onPress={() => this.logoutAlert()}>
                <Image
                  source={require('../assets/logout_icon.png')}
                  style={{ width: 20, height: 20, marginLeft: 20 }}
                />
                <Text
                  style={{
                    marginLeft: 15,
                    textAlign: 'center',
                    fontFamily: 'Poppins-Medium',
                  }}>
                  {Application_Constant.sidebar_logout}
            </Text>
              </TouchableOpacity>
            )}
          </View>

        </View>

        <PopupDialog
          onHardwareBackPress={() => {
            this.setState({ lodingDialog: false });
          }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
          <DialogContent>
            <View style={{ alignItems: 'center' }}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[
                  {
                    height: 10,
                    marginBottom: 10,
                    marginTop: 30,
                    marginLeft: 20,
                    marginRight: 20,
                  },
                ]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>
      </View>
    );
  }
}

Sidebar.propTypes = {
  navigation: PropTypes.object,
};

export default Sidebar;
