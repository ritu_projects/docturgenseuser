import React, {Component} from 'react';
var ReactNative = require('react-native');
import {View, Text, StyleSheet, Dimensions} from 'react-native';
const win = Dimensions.get('window');
import Colors from '.../colors/colors';

export default {
  viewpagerbg_img: {
    height: '100%',
    backgroundColor: Colors.COLOR.darkBlue,
  },
  viewpager_txt: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center', // <-- the magic
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 200,
    color: Colors.COLOR.lightBlue,
  },
};
