import React, {Component} from 'react';
var ReactNative = require('react-native');
import {View, Text, StyleSheet, Dimensions} from 'react-native';
const win = Dimensions.get('window');

export default {
  splashbg_img: {
    flex: 1,
    width: win.width,
    height: win.height,
  },
};
