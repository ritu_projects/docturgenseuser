import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  ScrollView,
  AsyncStorage,
  TouchableOpacity,
  ActivityIndicator,
  Linking,
  Platform,
  StatusBar,

} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import Styles from '../styles/styles_inscription_login';
import {Card, CardItem, Icon, Thumbnail, Row} from 'native-base';
import Colors from '../colors/colors';
import LinearGradient from 'react-native-linear-gradient';
import Application_Constant from './localization';

import Image from 'react-native-fast-image';
import PopupDialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import axios from 'axios';
const STATUS_BAR_THEME = Platform.OS === 'ios' ? 'dark-content' : 'light-content';

export default class ForgotPassword_Screen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AddresssMail: '',
      errEmail: '',
      enter_emailid: Application_Constant.enter_emailid,
      currentlatitude: '',
      currentlongitude: '',
      phonenumber: '+33 998877667',
    };
  }
  moveToLogin = () => {
    this.props.navigation.navigate('Connexion_Login');
  };
  componentDidMount() {
    AsyncStorage.getItem('phonenumber').then(phonenumber => {
      console.log('Phonenumber  ' + phonenumber);

      this.setState({
        phonenumber: phonenumber,
      });
    });
  }
  callNumber = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';
    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };

  validation() {
    var isValidate = 0;
    if (this.state.AddresssMail != '') {
      isValidate += 1;
      if (this.validateEmail(this.state.AddresssMail)) {
        isValidate += 1;
      } else {
        isValidate -= 1;
        this.setState({
          errEmail: Application_Constant.enter_emailid,

        });
      }
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        errEmail: Application_Constant.ShouldemptyText,
      });
    }
    if (isValidate == 2) {
      console.log(
        'Use Login::::' +
          this.state.AddresssMail +
          '==' +
          this.state.ModdePassword,
      );
      // this.userLoginApi();
      this.setState({
        animating: true,
        lodingDialog: true,
      });
      this.user_ForgotPasswordApi();
    } else {
      // alert(strings.Pleasefillallthefields)
    }
  }

  user_ForgotPasswordApi() {
    console.log('Email Address Forgot   ' + this.state.AddresssMail);
    axios
      .post(
        Application_Constant.BaseUrl + 'api/userForgotPassword',
        {
          email: this.state.AddresssMail,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            //other header fields
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('ForgotPass Data is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
          alert(res_data_msg);
        } else if (res_data == 'false') {
          this.setState({
            animating: false,
            lodingDialog: false,
          });

          this.props.navigation.navigate('Connexion_Login');
        }
      });
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  render() {
    return (
      <SafeAreaView 

       style={{flex: 1, marginTop: Platform.OS == 'ios' ? 0 : 0}}>
            {Platform.OS == 'ios' ?
         
         <StatusBar
            translucent={true}
            backgroundColor={'#324048'}
            barStyle={STATUS_BAR_THEME} />
          :
          null
        }
        <ScrollView style={{flex: 1}}>
          <View>
            <Image
              source={require('../assets/login_bg.png')}
              style={Styles.backgroundImage}>
              <View style={{width: '100%', marginTop: 50}}>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    paddingLeft: 13,
                    paddingRight: 13,
                    paddingTop: 10,
                    paddingBottom: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Poppins-Bold',
                      textAlign: 'center',
                      color: Colors.COLOR.whiteclr,
                      fontSize: 18,
                    }}>
                    Mot de passe oublié ?
                  </Text>

                  <Text
                    style={{
                      fontFamily: 'Poppins-Medium',
                      textAlign: 'center',
                      color: Colors.COLOR.whiteclr,
                      fontSize: 16,
                      marginTop: 10,
                    }}
                  />
                  <Card
                    style={{
                      width: '100%',
                      marginTop: 40,
                      borderRadius: 40,
                      padding: 25,
                    }}>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontFamily: 'Poppins-Bold',
                      }}>
                      {' '}
                      {this.state.enter_emailid}
                    </Text>

                    <TextInput
                      style={{
                        fontFamily: 'Poppins-Regular',
                        padding: 10,
                        height: 40,
                        marginTop: 20,
                      }}
                      editable
                      underlineColorAndroid={Colors.COLOR.lightBlue}
                      placeholder={Application_Constant.AddresssMail}
                      keyboardType="email-address"
                      onChangeText={AddresssMail =>
                        this.setState({AddresssMail, errEmail: ''})
                      }>
                      {this.state.AddresssMail}
                    </TextInput>

                    {!!this.state.errEmail && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.errEmail}
                      </Text>
                    )}
                  </Card>
                  <LinearGradient
                    style={{
                      flexDirection: 'row',
                      height: 50,
                      marginRight: 10,
                      marginLeft: 10,
                      borderRadius: 13,
                      marginTop: 70,
                      marginBottom: 5,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    colors={['#4C94E6', '#7CB5F8']}>
                    <TouchableOpacity
                      onPress={() => this.validation()}
                      style={Styles.bottom_btn_view}>
                      <Text
                        style={{
                          fontSize: 19,
                          color: Colors.COLOR.whiteclr,
                          fontFamily: 'Poppins-Medium',
                          textAlign: 'center',
                          fontSize: 18,
                        }}>
                        Continuer
                      </Text>

                      {/* <Image style={{
                                                width: 20,
                                                height: 20,
                                                marginLeft: 15,


                                            }}
                                                source={require('../assets/arrow_left_icon.png')}></Image> */}
                    </TouchableOpacity>
                  </LinearGradient>
                </View>
              </View>
            </Image>
            <TouchableOpacity
              style={Styles.img_cross_vew}
              onPress={() => this.moveToLogin()}>
              <Image
                style={Styles.img_cross_vew}
                source={require('../assets/cross_icon.png')}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>

        {/* <TouchableOpacity
          style={{
            bottom: 0,
            height: 70,
          }}
          onPress={this.callNumber}>
          <LinearGradient
            style={{
              flexDirection: 'row',
              backgroundColor: 'white',
              borderTopLeftRadius: 27,
              borderTopRightRadius: 27,
              padding: 20,
              alignItems: 'center',
            }}
            colors={['#7CB5F8', '#4C94E6']}
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}>
            <TouchableOpacity
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginRight: 5,
                marginLeft: 5,
                paddingLeft: 10,
              }}
              onPress={this.callNumber}>
              <Image
                style={{width: 40, height: 40}}
                source={require('../assets/call_icon.png')}
              />
              <Text
                style={{
                  width: '100%',
                  fontFamily: 'Poppins-Regular',
                  paddingLeft: 0,
                  marginRight: 5,
                  color: 'white',
                  flex: 1,
                  fontSize: 13,
                  paddingRight: 5,
                  marginLeft: 15,
                }}>
                Je suis en situation d’urgence{' '}
              </Text>
            </TouchableOpacity>
          </LinearGradient> 
        </TouchableOpacity> */}
        <PopupDialog
          onHardwareBackPress={() => {
            this.setState({lodingDialog: false});
          }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({slideFrom: 'bottom'})}>
          <DialogContent>
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[
                  {
                    height: 10,
                    marginBottom: 10,
                    marginTop: 30,
                    marginLeft: 20,
                    marginRight: 20,
                  },
                ]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>
      </SafeAreaView>
    );
  }
}
