import React, { Component, useState } from 'react';
import {
  StyleSheet,
  StatusBar,
  View,
  Text,
  TextInput,
  AsyncStorage,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Linking,
  ToastAndroid,
  AlertIOS,
  Platform,
} from 'react-native';
import Styles from '../styles/styles_inscription_login';
import { Card, CardItem, Icon, Thumbnail, Row } from 'native-base';
import Application_Constant from './localization';
import { SafeAreaView } from 'react-native-safe-area-context';

import Image from 'react-native-fast-image';
import Colors from '../colors/colors';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';

//import Application_Constant from '../strings/string'
import PopupDialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';

import axios from 'axios';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';


var langg = '';
var radio_props = [
  { label: 'Homme', value: 0 },
  { label: 'Femme', value: 1 },
  { label: 'Autre', value: 2 },
];

const radioButtonsData = [
  { id: 0, label: 'Homme', value: 'M' },
  { id: 1, label: 'Femme', value: 'F' },
  { id: 2, label: 'Autre', value: 'O' },
]


var radiogroup_language_options = [
  { key: 0, value: Application_Constant.language_french, index_vlue: 'fr' },
  { key: 1, value: Application_Constant.language_english, index_vlue: 'en' },
];
var genn;
const STATUS_BAR_THEME = Platform.OS === 'ios' ? 'dark-content' : 'light-content';


export default class Inscription_Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: '',
      fullNameErr: '',
      AddresssMail: '',
      errEmail: '',
      Telephone: '',
      teleErr: '',
      ModdePassword: '',
      passErr: '',
      confirm_password: '',
      cpassErr: '',
      animating: false,
      lodingDialog: false,
      phonenumber: '+33 998877667',
      gender: 0,
      genderErr: '',
      language: '',
      languageErr: '',
      selectedValue1: false,
      value: '',

    };
  }
  callNumber = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };

  moveToLogin = () => {
    this.props.navigation.navigate('Connexion_Login');
  };
  componentDidMount() {
    AsyncStorage.getItem('phonenumber').then(phonenumber => {
      console.log('Phonenumber  ' + phonenumber);

      this.setState({
        phonenumber: phonenumber,
      });
    });
  }
  moveToMapScreen = () => {
    this.props.navigation.navigate('MapExample');
  };
  validation() {
    var isValidate = 0;
    if (this.state.fullName != '') {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        fullNameErr: Application_Constant.ShouldemptyText,
      });
    }

    if (this.state.AddresssMail != '') {
      isValidate += 1;
      if (this.validateEmail(this.state.AddresssMail)) {
        isValidate += 1;
      } else {
        isValidate -= 1;
        this.setState({
          errEmail: Application_Constant.enter_emailid,

        });
      }
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        errEmail: Application_Constant.ShouldemptyText,
      });
    }
    if (this.state.Telephone != '') {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        teleErr: Application_Constant.ShouldemptyText,
      });
    }
    if (this.state.ModdePassword != '') {
      isValidate += 1;
      if (this.validatePassword(this.state.ModdePassword)) {
        isValidate += 1;
      } else {
        isValidate -= 1;
        this.setState({
          passErr: Application_Constant.passwrd_length_err,
        });
      }
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        passErr: Application_Constant.ShouldemptyText,
      });
    }

    if (this.state.confirm_password != '') {
      isValidate += 1;
      if (this.validatePassword(this.state.confirm_password)) {
        isValidate += 1;
      } else {
        isValidate -= 1;
        this.setState({
          cpassErr: Application_Constant.passwrd_length_err,
        });
      }
      if (this.state.confirm_password == this.state.ModdePassword) {
        isValidate += 1;
      } else {
        isValidate -= 1;
        this.setState({
          cpassErr: Application_Constant.confirm_password_same,
        });
      }
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        cpassErr: Application_Constant.ShouldemptyText,
      });
    }
    console.log('Gender ', this.state.gender);
    // if (this.state.gender != '') {
    //   isValidate += 1;
    // } else {
    //   console.log('cat empty::::');
    //   isValidate -= 1;
    //   this.setState({
    //     genderErr: Application_Constant.selectGender,
    //   });
    // }

    if (this.state.language != '') {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        languageErr: Application_Constant.slct_language,
      });
    }
    if (isValidate == 10) {
      console.log(
        'Use Login::::' + this.state.AddresssMail + '==' + this.state.password,
      );
      this.setState({
        animating: true,
        lodingDialog: true,
      });

      this.userRegisterApi();
    } else {
      // alert(strings.Pleasefillallthefields)
    }
  }

  userRegisterApi() {
     genn = '';
    if (
      this.state.gender == 'Homme' ||
      this.state.gender == 'Male' ||
      this.state.gender == 'M' ||
      this.state.gender == '0'
    ) {
      genn = 'M';
    } else if (
      this.state.gender == 'Femme' ||
      this.state.gender == 'Female' ||
      this.state.gender == 'F' ||
      this.state.gender == '1'

    ) {
      genn = 'F';
    } else if (
      this.state.gender == 'Autre' ||
      this.state.gender == 'Other' ||
      this.state.gender == 'O' ||
      this.state.gender == '2'

    ) {
      genn = 'O';
    }
    console.log('Gender API ::::', this.state.gender);
    if (this.state.language == 'Français') {
      langg = 'fr';
    } else if (this.state.language == 'Anglais') {
      langg = 'en';
    }

    axios
      .post(
        Application_Constant.BaseUrl + 'api/registration',
        {
          fullname: this.state.fullName,
          email: this.state.AddresssMail,
          telephone: this.state.Telephone,
          password: this.state.ModdePassword,
          gender: genn,
          lang: langg,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            //other header fields
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('Data is  ' + data);
        var res_data = data.error;
        console.log('Data Res is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
          alert(res_data_msg);
        } else if (res_data == 'false') {
          var user_value = JSON.stringify(data.errorMessage);
          console.log('Response User$$$' + user_value);
          // this.storeItem("uservalue", user_value)
          this.setState({
            animating: false,
            lodingDialog: false,
          });
          Application_Constant.setLanguage(langg);

          this.notifyMessage("S'inscrire avec succès");

          // this.props.navigation.navigate('QRCodeScanner')
        }
      });
  }
  notifyMessage(msg) {
    if (Platform.OS === 'android') {
      ToastAndroid.show(msg, ToastAndroid.SHORT);
      this.moveToLogin();
    } else {
   //   AlertIOS.alert(msg);
      this.moveToLogin();
    }
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  validatePassword(password) {
    if (password.length < 6) {
      return false;
    } else if (password.length > 16) {
      return false;
    } else {
      return true;
    }
  }

  onSelect(option) {
    var opp = JSON.stringify(option);
    console.log('Value   ' + opp);
    var opp2 = option.value;
    console.log('Value2   ' + opp2);

    this.setState({
      gender: opp2,
      genderErr: '',
    });
  }

  onChangeTextPress(value, index_vlue) {
    console.log('Language Value is  ' + value);
    console.log('Language index_vlue is  ' + index_vlue);

    this.setState({
      language: value,
      languageErr: '',
    });
  }




  render() {

    return (
      <SafeAreaView 
      style={{ flex: 1, marginTop: Platform.OS == 'ios' ? 0 : 0 }}>
       {Platform.OS == 'ios' ?
         
         <StatusBar
            translucent={true}
            backgroundColor={'#324048'}
            barStyle={STATUS_BAR_THEME} />
          :
          null
        }
        <ScrollView style={{ flex: 1 }} nestedScrollEnabled={true}>
          <View>
            <Image
              source={require('../assets/login_bg.png')}
              style={Styles.backgroundImage}>
              <View style={{ width: '100%', marginTop: 50 }}>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    paddingLeft: 13,
                    paddingRight: 13,
                    paddingTop: 10,
                    paddingBottom: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Poppins-Bold',
                      textAlign: 'center',
                      color: Colors.COLOR.whiteclr,
                      fontSize: 18,
                    }}>{Application_Constant.signup_title}</Text>

                  <Text
                    style={{
                      fontFamily: 'Poppins-Medium',
                      textAlign: 'center',
                      color: Colors.COLOR.whiteclr,
                      fontSize: 16,
                      marginTop: 10,
                      // padding:10,
                    }}>{Application_Constant.signup_2nd_title}</Text>

                  <Card
                    style={{
                      width: '100%',
                      marginTop: 40,
                      borderRadius: 40,
                      padding: 25,
                    }}>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontFamily: 'Poppins-Medium',
                        fontSize: 20,
                      }}>{Application_Constant.signup}</Text>

                    <TextInput
                      style={{
                        fontFamily: 'Poppins-Regular',
                        padding: 10,
                        height: 40,
                        marginTop: 20,
                        marginBottom: 5,
                      }}
                      editable
                      underlineColorAndroid={Colors.COLOR.lightBlue}
                      placeholder={Application_Constant.fullName}
                      onChangeText={fullName =>
                        this.setState({ fullName, fullNameErr: '' })
                      }
                    />
                    {!!this.state.fullNameErr && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.fullNameErr}
                      </Text>
                    )}
                    <TextInput
                      style={{
                        fontFamily: 'Poppins-Regular',
                        padding: 10,
                        height: 40,
                        marginTop: 20,
                        marginBottom: 5,
                      }}
                      autoCapitalize={false}

                      editable
                      underlineColorAndroid={Colors.COLOR.lightBlue}
                      placeholder={Application_Constant.AddresssMail}
                      keyboardType="email-address"
                      onChangeText={AddresssMail =>
                        this.setState({ AddresssMail, errEmail: '' })
                      }
                    />
                    {!!this.state.errEmail && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.errEmail}
                      </Text>
                    )}
                    <TextInput
                      style={{
                        fontFamily: 'Poppins-Regular',
                        padding: 10,
                        height: 40,
                        marginTop: 30,
                        marginBottom: 5,
                      }}
                      underlineColorAndroid={Colors.COLOR.lightBlue}
                      placeholder={Application_Constant.Telephone}
                      keyboardType="phone-pad"
                      onChangeText={Telephone =>
                        this.setState({ Telephone, teleErr: '' })
                      }
                    />
                    {!!this.state.teleErr && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.teleErr}
                      </Text>
                    )}
                    <TextInput
                      style={{
                        fontFamily: 'Poppins-Regular',
                        padding: 10,
                        height: 40,
                        marginTop: 30,
                        marginBottom: 5,
                      }}
                      underlineColorAndroid={Colors.COLOR.lightBlue}
                      placeholder={Application_Constant.password}
                      secureTextEntry={true}
                      onChangeText={ModdePassword =>
                        this.setState({ ModdePassword, passErr: '' })
                      }
                    />
                    {!!this.state.passErr && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.passErr}
                      </Text>
                    )}
                    <TextInput
                      style={{
                        fontFamily: 'Poppins-Regular',
                        padding: 10,
                        height: 40,
                        marginTop: 30,
                        marginBottom: 5,
                      }}
                      underlineColorAndroid={Colors.COLOR.lightBlue}
                      placeholder={Application_Constant.confirmPassword}
                      secureTextEntry={true}
                      onChangeText={confirm_password =>
                        this.setState({ confirm_password, cpassErr: '' })
                      }
                    />
                    {!!this.state.cpassErr && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.cpassErr}
                      </Text>
                    )}
                    <View style={{ marginTop: 30, padding: 10 }}>
                      <Text
                        style={{
                          fontFamily: 'Poppins-Regular',
                          marginBottom: 10,
                        }}>{Application_Constant.gender_Selection}</Text>


                     

                      <RadioForm
                        radio_props={radio_props}
                        buttonSize={12}
                        // initial={-1}
                        initial={0}

                        onPress={value => {
                          this.setState({
                            
                            value: value,
                            gender: value,

                            genderErr: '',
                          });
                        }}
                        style={{flexDirection: "row", marginTop: 20, padding:10,}}
                      />
                      {!!this.state.genderErr && (
                        <Text
                          style={{
                            color: 'red',
                            marginLeft: 10,
                            marginTop: -5,
                            fontSize: 12,
                          }}>
                          {this.state.genderErr}
                        </Text>
                      )}
                    </View>
                    <View style={{ marginTop: 30, padding: 10 }}>
                      <Text
                        style={{
                          fontFamily: 'Poppins-Regular',
                          marginBottom: 10,
                        }}>
                        {Application_Constant.slct_language}
                      </Text>
                      <Dropdown
                        style={{ flex: 1 }}
                        label={Application_Constant.slct_language}
                        data={radiogroup_language_options}
                        rippleCentered={true}
                        valueExtractor={({ value }) => value}
                        onChangeText={(value, index_vlue) => {
                          this.onChangeTextPress(value, index_vlue);
                        }}
                      />
                      {!!this.state.languageErr && (
                        <Text
                          style={{
                            color: 'red',
                            marginLeft: 10,
                            marginTop: -5,
                            fontSize: 12,
                          }}>
                          {this.state.languageErr}
                        </Text>
                      )}
                    </View>
                  </Card>
                  <TouchableOpacity
                    style={{ width: '100%', marginTop: 30, marginBottom: 20 }}
                    onPress={() => this.validation()}>
                    <LinearGradient
                      style={{
                        flexDirection: 'row',
                        height: 50,
                        marginRight: 10,
                        marginLeft: 10,
                        borderRadius: 13,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      colors={['#7CB5F8', '#4C94E6']}
                      start={{ x: 0, y: 1 }}
                      end={{ x: 1, y: 1 }}>
                      <Text
                        style={{
                          fontSize: 19,
                          color: Colors.COLOR.whiteclr,
                          fontFamily: 'Poppins-Medium',
                          textAlign: 'center',
                          fontSize: 18,
                        }}>{Application_Constant.continue_register}</Text>
                      {/* <Image style={{
                                                width: 20,
                                                height: 20,
                                                marginLeft: 15,

                                            }}
                                                source={require('../assets/arrow_left_icon.png')}></Image> */}
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              </View>
            </Image>
            <TouchableOpacity
              style={Styles.img_cross_vew}
              onPress={() => this.moveToMapScreen()}>
              <Image
                style={Styles.img_cross_vew}
                source={require('../assets/cross_icon.png')}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
        {/* <TouchableOpacity
                    style={{
                        bottom: 0,
                        height: 70,
                    }}
                    onPress={this.callNumber}
                >
                    <LinearGradient
                        style={{
                            flexDirection: 'row',
                            backgroundColor: 'white',
                            borderTopLeftRadius: 27,
                            borderTopRightRadius: 27,
                            padding: 20,
                            alignItems: "center",

                        }}
                        colors={['#7CB5F8', '#4C94E6']}
                        start={{ x: 0, y: 1 }}
                        end={{ x: 1, y: 1 }}>
                        <TouchableOpacity style={{ width: '100%', flexDirection: 'row', alignItems: "center", marginRight: 5, marginLeft: 5, paddingLeft: 10, }}
                            onPress={this.callNumber}>

                            <Image style={{ width: 40, height: 40, }}
                                source={require('../assets/call_icon.png')} ></Image>
                            <Text style={{ width: "100%", fontFamily: "Poppins-Regular", paddingLeft: 0, marginRight: 5, color: "white", flex: 1, fontSize: 13, paddingRight: 5, marginLeft: 15 }}>Composer le 15 si vous ete en situation d`urgence </Text>
                        </TouchableOpacity>
                    </LinearGradient>

                </TouchableOpacity> */}
        <PopupDialog
          onHardwareBackPress={() => {
            this.setState({ lodingDialog: false });
          }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
          <DialogContent>
            <View style={{ alignItems: 'center' }}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[
                  {
                    height: 10,
                    marginBottom: 10,
                    marginTop: 30,
                    marginLeft: 20,
                    marginRight: 20,
                  },
                ]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>
      </SafeAreaView>
    );
  }
}
