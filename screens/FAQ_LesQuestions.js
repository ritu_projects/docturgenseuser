import React, {Component} from 'react';
//import react in our code.
import {
  StyleSheet,
  AsyncStorage,
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  FlatList,
  Dimensions,
  Platform,
} from 'react-native';
import {Card} from 'native-base';
//import all the components we are going to use.
import Application_Constant from './localization';

import Image from 'react-native-fast-image';
import PopupDialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import axios from 'axios';

export default class FAQ_LesQuestions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      leave_number_numrrodecongé: '4',
      animating: false,
      lodingDialog: false,
    };
    0;
  }

  componentDidMount() {
    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);

      var useridd = JSON.parse(userid);
      console.log('User ID^^^' + useridd);

      this.setState({
        userId: useridd,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      console.log('User Token^^^' + tokenn);

      this.setState({
        token: tokenn,
      });

      console.log('User Token%%%' + this.state.token);
      this.setState({
        animating: true,
        lodingDialog: true,
      });
      axios
        .get(Application_Constant.BaseUrl + 'api/getUserfaqs', {})
        .then(response => response.data)
        .then(data => {
          console.log('Recent Response is  ' + JSON.stringify(data));
          var res_data = data.error;
          console.log('Data Res is  ' + res_data);

          console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            this.setState({
              animating: false,
              lodingDialog: false,
              data: [],
            });
            alert(res_data_msg);
          } else if (res_data == 'false') {
            this.setState({
              data: [],
            });
            this.setState({
              animating: false,
              lodingDialog: false,
              data: data.record,
            });
          }
        });
    });
  }
  render() {
    return (
      <View
        //  source={require('../assets/contactus_bg.png')}

        style={{
          flex: 1,
          backgroundColor: 'white',
          marginTop: Platform.OS == 'ios' ? 0 : 0,
        }}>
        <Text
          style={{
            width: '50%',
            fontFamily: 'Poppins-Bold',
            textAlign: 'center',
            paddingLeft: 20,
            marginTop: 20,
          }}>
          Foire aux questions{' '}
        </Text>

        <FlatList
          ref={flatList1 => {
            this.flatList1 = flatList1;
          }}
          style={{width: '100%', marginBottom: 10, backgroundColor: 'white'}}
          horizontal={false}
          data={this.state.data}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <TouchableOpacity
              style={{
                paddingLeft: 10,
                paddingRight: 10,
              }}>
              <Card
                style={{
                  width: '100%',
                  borderRadius: 15,
                  paddingLeft: 10,
                  paddingTop: 20,
                  paddingBottom: 20,

                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: '98%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      paddingTop: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'Poppins-Medium',
                        paddingLeft: 10,
                      }}>
                      {item.question}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      paddingTop: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'Poppins-Regular',
                        paddingLeft: 10,
                      }}>
                      {item.answer}
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={{
                      flexDirection: 'row',
                      paddingTop: 15,
                      alignItems: 'center',
                    }}
                  />
                </View>
              </Card>
            </TouchableOpacity>
          )}
        />
        <PopupDialog
          onHardwareBackPress={() => {
            this.setState({lodingDialog: false});
          }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({slideFrom: 'bottom'})}>
          <DialogContent>
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[
                  {
                    height: 10,
                    marginBottom: 10,
                    marginTop: 30,
                    marginLeft: 20,
                    marginRight: 20,
                  },
                ]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>
      </View>
    );
  }
}
