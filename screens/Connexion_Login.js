import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  StatusBar,
  Linking,
  AsyncStorage,
  Platform,
} from 'react-native';
import Styles from '../styles/styles_inscription_login';
import {Card, CardItem, Icon, Thumbnail, Row} from 'native-base';
import Colors from '../colors/colors';
import LinearGradient from 'react-native-linear-gradient';
// import Application_Constant from '../strings/string'
import Application_Constant from './localization';
import { SafeAreaView } from 'react-native-safe-area-context';
import PopupDialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import axios from 'axios';
import firebase from 'react-native-firebase';
import Image from 'react-native-fast-image';
const STATUS_BAR_THEME = Platform.OS === 'ios' ? 'dark-content' : 'light-content';


export default class Connexion_Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AddresssMail: '',
      errEmail: '',
      ModdePassword: '',
      passwordErr: '',
      dummyEmail: 'john@gmail.com',
      dummyPwd: '123456',
      forgotPassword: Application_Constant.forgotPassword,
      animating: false,
      lodingDialog: false,
      phonenumber: '+33 998877667',
      device_token: '',
      fcmToken: '',
    };
  }

  // async moveToTimer() {
  //     const data = await this.performTimeConsumingTask();
  //     if (data !== null) {
  //         this.moveToRegister()

  //     }
  // }
  // performTimeConsumingTask = async () => {
  //     return new Promise((resolve) =>
  //         setTimeout(
  //             () => { resolve('result') },
  //             3000
  //         )
  //     )
  // }

  async componentDidMount() {
  //  this.checkPermission();

    AsyncStorage.getItem('phonenumber').then(phonenumber => {
      console.log('Phonenumber  ' + phonenumber);

      //   var phonenumberr = JSON.parse(phonenumber);
      //   console.log("Phonenumber  "+phonenumber)

      this.setState({
        phonenumber: phonenumber,
      });
    });
    var get_lang = Application_Constant.getLanguage();

    console.log('Get Language ', get_lang);
    //   this.checkPermission();
    AsyncStorage.getItem('fcmToken').then(fcmToken => {
      this.setState({fcmToken: fcmToken});
      console.log('state fcmToken Login ============' + fcmToken);
    });
  }
//1
async checkPermission() {
  const enabled = await firebase.messaging().hasPermission();
  if (enabled) {
    this.getToken();

  } else {
   this.requestPermission();
  }
}

//2
async requestPermission() {
  try {
    await firebase.messaging().requestPermission();
    // User has authorised
    this.getToken();
  } catch (error) {
    // User has rejected permissions
    console.log('permission rejected',error);
    alert('Activer l\'autorisation de notification')


  }
}

//3
async getToken() {
  let fcmToken = await AsyncStorage.getItem('fcmToken');
  console.log('FCM token$$$  ' + fcmToken);
  if (!fcmToken) {
    fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      // user has a device token
      await AsyncStorage.setItem('fcmToken', fcmToken);
      this.setState({
        fcmToken : fcmToken
      })
      this.validation()
      
    }
  }
}
  callNumber = () => {
    console.log('Mobilenum at Login   ' + this.state.phonenumber);

    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };

  forgotPasswordScreen = () => {
    this.props.navigation.navigate('ForgotPassword_Screen');
  };
  moveToMapScreen = () => {
    console.log("Login cancel Click ","Login cancel Click ")

    this.props.navigation.navigate('MapExample');
  };
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  validatePassword(password) {
    if (password.length < 6) {
      return false;
    } else if (password.length > 16) {
      return false;
    } else {
      return true;
    }
  }
  checkFCMtoken = async () => {
    if(this.state.fcmToken != null && this.state.fcmToken != '' && this.state.fcmToken != "") {

    this.validation()

    }
    else
    {
      //  this.checkPermission()
      this.validation()

    }

  }
  validation() {
    AsyncStorage.getItem('fcmToken').then(fcmToken => {
      this.setState({fcmToken: fcmToken});
      console.log('state userId click ============' + this.state.fcmToken);
    });
    var isValidate = 0;
    if (this.state.AddresssMail != '') {
      isValidate += 1;
      if (this.validateEmail(this.state.AddresssMail)) {
        isValidate += 1;
      } else {
        isValidate -= 1;
        this.setState({
          errEmail: Application_Constant.enter_emailid,
           
        });
      }
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        errEmail: Application_Constant.ShouldemptyText,
      });
    }

    if (this.state.ModdePassword != '') {
      isValidate += 1;
      if (this.validatePassword(this.state.ModdePassword)) {
        isValidate += 1;
      } else {
        isValidate -= 1;
        this.setState({
          passwordErr: 'La longueur devrait être min 6',
        });
      }
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        passwordErr: Application_Constant.ShouldemptyText,
      });
    }

    if (isValidate == 4) {
      console.log(
        'Use Login::::' +
          this.state.AddresssMail +
          '==' +
          this.state.ModdePassword,
      );
      // this.userLoginApi();
      this.setState({
        animating: true,
        lodingDialog: true,
      });
      this.userLoginApi();
    } else {
      // alert(strings.Pleasefillallthefields)
    }
  }

  userLoginApi() {
    AsyncStorage.getItem('fcmToken').then(fcmToken => {
      this.setState({fcmToken: fcmToken});
  
    console.log(
      'state userId api callclick ============' + this.state.fcmToken,
    );

    axios
      .post(
        Application_Constant.BaseUrl + 'api/login',
        {
          email: this.state.AddresssMail,
          password: this.state.ModdePassword,
          deviceToken: this.state.fcmToken,
        // deviceToken: "YYYY",

        },
        {
          headers: {
            'Content-Type': 'application/json',
            //other header fields
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('LoginResponse is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
          if (
            res_data_msg ==
              "You didn't verify your mobile number yet,please verify it." ||
            res_data_msg ==
              "Vous n'avez pas encore vérifié votre numéro de portable, merci de le vérifier."
          ) {
            var res_data_OTP = JSON.stringify(data.otp);
            var res_data_userid = JSON.stringify(data.user_id);

            this.setState({
              animating: false,
              lodingDialog: false,
            });
            console.log('OTP Login   ' + res_data_OTP);
            console.log('OTP Login UserId  ' + res_data_userid);
              
            AsyncStorage.setItem('opt', res_data_OTP);
            AsyncStorage.setItem('useridd', res_data_userid);

            this.props.navigation.navigate('OtpVerfication');
          } else {
            alert(res_data_msg);
          }
        } else if (res_data == 'false') {
          var usersDetailss = JSON.stringify(data.usersDetails);
          var userid = JSON.stringify(data.usersDetails.id);
          var userfullname = data.usersDetails.fullname;
          var userprofilePic = data.usersDetails.image;

          console.log('Response User Id $$$' + userid);
          var token = JSON.stringify(data.token);
          console.log('Response User token $$$' + token);

          var usermobile_verification = JSON.stringify(
            data.usersDetails.mobile_verification,
          );
          if (usermobile_verification == 0) {
            this.setState({
              animating: false,
              lodingDialog: false,
            });

            AsyncStorage.setItem('opt', res_data_OTP);

            this.props.navigation.navigate('OtpVerfication');
          } else if (usermobile_verification == 1) {
            var langg = data.usersDetails.lang
              Application_Constant.setLanguage(langg)
            AsyncStorage.setItem('userid', userid);
            AsyncStorage.setItem('usersDetails', usersDetailss);
            AsyncStorage.setItem('token', token);
            // AsyncStorage.setItem("username", userfullname);

            // if(userprofilePic!=null && userprofilePic!='null' && userprofilePic!='' && userprofilePic!='http://docturgences.com/public/upload/usersprofile/')
            // {
            //    var userprofilePicc="http://docturgences.com/public/upload/usersprofile/"+userprofilePic
            //     AsyncStorage.setItem("userimage", userprofilePicc);

            // }
            // else
            // {
            //     AsyncStorage.setItem("userimage", 'http://docturgences.com/public/upload/usersprofile/');

            // }
            // this.storeItem("uservalue", user_value)
            this.setState({
              animating: false,
              lodingDialog: false,
              AddresssMail: '',
              ModdePassword: '',
            });

            // this.props.navigation.navigate('AfterLogin');
            this.props.navigation.navigate('AfterLoginNew');
          }
        }
      });
    });
  }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: 'white',
          marginTop: Platform.OS == 'ios' ? 0 : 0,
        }}>
    {Platform.OS == 'ios' ?
         
         <StatusBar
            translucent={true}
            backgroundColor={'#324048'}
            barStyle={STATUS_BAR_THEME} />
          :
          null
        }
        <ScrollView style={{flex: 1}}>
          <View>
            <Image
              source={require('../assets/login_bg.png')}
              style={Styles.backgroundImage}>
              <View style={{width: '100%', marginTop: 50}}>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    paddingLeft: 13,
                    paddingRight: 13,
                    paddingTop: 10,
                    paddingBottom: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Poppins-Bold',
                      textAlign: 'center',
                      color: Colors.COLOR.whiteclr,
                      fontSize: 18,
                    }}>{Application_Constant.login}</Text>

                  <Text
                    style={{
                      fontFamily: 'Poppins-Medium',
                      textAlign: 'center',
                      color: Colors.COLOR.whiteclr,
                      fontSize: 16,
                      marginTop: 10,
                    }}
                  />
                  <Card
                    style={{
                      width: '100%',
                      marginTop: 40,
                      borderRadius: 40,
                      padding: 25,
                    }}>
                    {/* <Text style={{

                                        textAlign: "center",
                                        fontFamily: "Poppins-Bold",
                                    }}
                                    >
                                        Connexion</Text> */}
                    <TextInput
                      value={this.state.AddresssMail}
                      autoCapitalize={false}
                      style={{
                        fontFamily: 'Poppins-Regular',
                        padding: 10,
                        height: 40,
                        marginTop: 20,
                      }}
                      editable
                      underlineColorAndroid={Colors.COLOR.lightBlue}
                      placeholder={Application_Constant.AddresssMail}
                      keyboardType="email-address"
                      onChangeText={AddresssMail =>
                        this.setState({AddresssMail, errEmail: ''})
                      }
                    />
                    {!!this.state.errEmail && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.errEmail}
                      </Text>
                    )}

                    <TextInput
                      value={this.state.ModdePassword}
                      style={{
                        fontFamily: 'Poppins-Regular',
                        padding: 10,
                        height: 40,
                        marginTop: 30,
                      }}
                      editable
                      underlineColorAndroid={Colors.COLOR.lightBlue}
                      placeholder={Application_Constant.password}
                      secureTextEntry={true}
                      onChangeText={ModdePassword =>
                        this.setState({ModdePassword, passwordErr: ''})
                      }
                    />
                    {!!this.state.passwordErr && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: 10,
                          marginTop: -5,
                          fontSize: 12,
                        }}>
                        {this.state.passwordErr}
                      </Text>
                    )}
                  </Card>

                  <TouchableOpacity
                    onPress={() => this.forgotPasswordScreen()}
                    style={{
                      flexDirection: 'row',
                      marginTop: 20,
                      justifyContent: 'center',
                    }}>
                    <View>
                      <Text
                        style={{
                          color: Colors.COLOR.blackClr,
                          fontSize: 14,
                          textAlign: 'center',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {' '}
                        {this.state.forgotPassword}{' '}
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{width: '100%', marginTop: 30, marginBottom: 20}}
                    onPress={() => this.checkFCMtoken()}>
                    <LinearGradient
                      style={{
                        flexDirection: 'row',
                        height: 50,
                        marginRight: 10,
                        marginLeft: 10,
                        borderRadius: 13,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      colors={['#7CB5F8', '#4C94E6']}
                      start={{x: 0, y: 1}}
                      end={{x: 1, y: 1}}>
                      <Text
                        style={{
                          fontSize: 19,
                          color: Colors.COLOR.whiteclr,
                          fontFamily: 'Poppins-Medium',
                          textAlign: 'center',
                          fontSize: 18,
                        }}>{Application_Constant.login}
                      </Text>
                      {/* <Image style={{
                                                width: 20,
                                                height: 20,
                                                marginLeft: 15,

                                            }}
                                                source={require('../assets/arrow_left_icon.png')}></Image> */}
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              </View>
            </Image>
            <TouchableOpacity
              style={Styles.img_cross_vew}
              onPress={() => this.moveToMapScreen()}>
              <Image
                style={Styles.img_cross_vew2}
                source={require('../assets/cross_icon.png')}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
        {/* <TouchableOpacity
                    style={{
                        bottom: 0,
                        height: 70,
                    }}
                    onPress={this.callNumber}
                >
                    <LinearGradient
                        style={{
                            flexDirection: 'row',
                            backgroundColor: 'white',
                            borderTopLeftRadius: 27,
                            borderTopRightRadius: 27,
                            padding: 20,
                            alignItems: "center",

                        }}
                        colors={['#7CB5F8', '#4C94E6']}
                        start={{ x: 0, y: 1 }}
                        end={{ x: 1, y: 1 }}>
                        <TouchableOpacity style={{ width: '100%', flexDirection: 'row', alignItems: "center", marginRight: 5, marginLeft: 5, paddingLeft: 10, }}
                            onPress={this.callNumber}
                        >

                            <Image style={{ width: 40, height: 40, }}
                                source={require('../assets/call_icon.png')} ></Image>
                            <Text style={{ width: "100%", fontFamily: "Poppins-Regular", paddingLeft: 0, marginRight: 5, color: "white", flex: 1, fontSize: 13, paddingRight: 5, marginLeft: 15 }}>Je suis en situation d’urgence </Text>
                        </TouchableOpacity>
                    </LinearGradient>

                </TouchableOpacity> */}

        <PopupDialog
          onHardwareBackPress={() => {
            this.setState({lodingDialog: false});
          }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({slideFrom: 'bottom'})}>
          <DialogContent>
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[
                  {
                    height: 10,
                    marginBottom: 10,
                    marginTop: 30,
                    marginLeft: 20,
                    marginRight: 20,
                  },
                ]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>
      </SafeAreaView>
    );
  }
}
