import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, AsyncStorage, PermissionsAndroid, Linking, Platform,StatusBar } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE }  from 'react-native-maps';
import Styles from '../styles/styles_home';
import { SafeAreaView } from 'react-native-safe-area-context';

import { Card, CardItem, Icon, Thumbnail, Row } from 'native-base';
import Colors from '../colors/colors'
import LinearGradient from 'react-native-linear-gradient';
import Geolocation from '@react-native-community/geolocation';
import Image from 'react-native-fast-image'
import normalize from 'react-native-normalize';
const STATUS_BAR_THEME = Platform.OS === 'ios' ? 'dark-content' : 'light-content';

import Application_Constant from './localization';

export async function requestLocationPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Example App',
        'message': 'Example App access to your location '
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the location")
      //alert("You can use the location");

    } else {
      console.log("location permission denied")
      // alert("Location permission denied");
    }
  } catch (err) {
    console.warn(err)
  }
}



export default class MapExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentlatitude: 48.953814,
      currentlongitude: 2.899333,
      phonenumber: '+33 998877667',
      initialRegion: "",
    }
    if(Platform.OS === 'android')
    {

    }
    else if(Platform.OS == 'ios')
    {}
  }

  callNumber = () => {

    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';


    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    }
    else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  }

  getCurrentPosition = () => {

    if(Platform.OS == 'ios')
    {

    
    Geolocation.requestAuthorization();
    }

      Geolocation.getCurrentPosition(info => {
        console.log("latitude:::" + info);
       // console.log("longitude:::" + info.coords.longitude);



        let region = {
          latitude: info.coords.latitude,
          longitude: info.coords.longitude,

          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }
        this.setState({
          currentlatitude: info.coords.latitude,
          currentlongitude: info.coords.longitude,
          initialRegion: region,
        })

      },
      (error) => 
      {
        let region = {
          latitude: 48.8416,
          longitude: 2.2941,

          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        };
        this.setState({
          currentlatitude: 48.8416,
          currentlongitude: 2.2941,
          initialRegion: region,
        });
        console.log(error.message)

      },


      );
      Geolocation.watchPosition(success => {
        console.log("success watchman:::" + JSON.stringify(success));
  
        let region = {
            latitude: success.coords.latitude,
            longitude: success.coords.longitude,
            // latitude: 22.7369395,
            // longitude: 75.8716091,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }
        this.setState({
            initialRegion: region,
  
            currentlatitude: success.coords.latitude,
            currentlongitude: success.coords.longitude,
        })
        
    })
   
  };
  toggleDrawer = () => {
    this.props.navigation.openDrawer()
  };
  getCurrentPosition_animated = () => {
    console.log("Current Position ","Current Position")

    this.map.animateToRegion(this.state.initialRegion);

   

    


      //   let region = {
      //     latitude: info.coords.latitude,
      //     longitude: info.coords.longitude,

      //     latitudeDelta: 0.0922,
      //     longitudeDelta: 0.0421,
      //   }
      //   this.setState({
      //     currentlatitude: info.coords.latitude,
      //     currentlongitude: info.coords.longitude,
      //     initialRegion: region,
      //   })
      //   this.map.animateToRegion(this.state.initialRegion);

      // },
      // (error) => 
      // {
      //   let region = {
      //     latitude: 48.8416,
      //     longitude: 2.2941,

      //     latitudeDelta: 0.0922,
      //     longitudeDelta: 0.0421,
      //   };
      //   this.setState({
      //     currentlatitude: 48.8416,
      //     currentlongitude: 2.2941,
      //     initialRegion: region,
      //   });
      //   console.log(error.message)

      // },


      // );

 
   
  };
  componentDidMount() {

    AsyncStorage.getItem("phonenumber")
      .then(phonenumber => {
        console.log("Phonenumber  " + phonenumber)

        this.setState({
          phonenumber: phonenumber,
        });
      })
    if (Platform.OS == 'android') {
      requestLocationPermission()
    }

    this.getCurrentPosition();


    console.log("Mobilenum at Login   " + this.state.phonenumber)
    const { navigation } = this.props;
    navigation.addListener('willFocus', () => {
      console.log("Receive Map Nav ", "Receive Map Nav ")

      this.getCurrentPosition();

    });
  }


  // componentWillReceiveProps() {
  //   alert("Test")
  //   if (Platform.OS == 'android') {
  //     requestLocationPermission()
  //   }
  //   this.getCurrentPosition();
  // }
  forRegister = () => {

    this.props.navigation.navigate('Inscription_Register');
  }

  componentWillUnmount = () => {
    Geolocation.clearWatch(this.watchID);
 }


  forLogin = () => {
    console.log("Login Click ","Login Click ")

    this.props.navigation.navigate('Connexion_Login');
  }
  render() {
    return (
      <View style={Styles.containerWhite}>
         
        {Platform.OS == 'ios' ?
          <StatusBar
            translucent={true}
            backgroundColor={'#324048'}
            barStyle={STATUS_BAR_THEME} />
          :
          null
        }
        <View style={{ width: '100%', height: '100%' }}>
        <LinearGradient
        // source={require('./assets/download.png')}

        style={{
          flexDirection: 'row',
          height: normalize(30),
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: Platform.OS == 'ios' ? normalize(25) : normalize(0),
          marginBottom: normalize(-5),
        }}
        // colors={['white', 'rgba(0,128,0,0.2)']}
        colors={['#e7eef1', 'rgba(203,229,216,255)']}>
        <TouchableOpacity
          style={{
            width: '15%',
            flexDirection: 'row',
            marginLeft: 10,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={this.toggleDrawer.bind(this)}>
          {/*Donute Button Image */}
          <Image
            source={require('../assets/drawer.png')}
            style={{width: 25, height: 25}}
          />
        </TouchableOpacity>
        <View
          style={{
            width: '70%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontFamily: 'Montserrat-Bold',
              color: '#2da9ff',
              fontSize: 22,
            }}>
            {Application_Constant.homeTitle}
          </Text>
        </View>
        <TouchableOpacity
          style={{
            width: '15%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 10,
          }}
          onPress={() => this.getCurrentPosition_animated()}>
          <Image
            source={require('../assets/pin_icon.png')}
            style={{width: 25, height: 25}}
          />
        </TouchableOpacity>
      </LinearGradient>
          {!!this.state.initialRegion ?
            <MapView style={Styles.map_bg}
              ref={(map) => { this.map = map; }}

              initialRegion={this.state.initialRegion}

              mapType={'standard'}
              minZoomLevel={1}
              maxZoomLevel={20}
              provider={PROVIDER_GOOGLE}

              enableZoomControl={true}
              // showsUserLocation={true}
              // showsMyLocationButton={true}
              zoomEnabled={true}>
            </MapView>
            :
            null
          }


          <View style={Styles.crd_view}>

            <Card style={Styles.cardStle}>

              <TouchableOpacity 
              style={{ width: '49%', flexDirection: 'row', justifyContent: 'center', }}
               onPress={this.forRegister}>
                <Image 
                style={Styles.cardThumbhnail_Stle}

                  source={require('../assets/inscription_login_icon.png')} ></Image>

                <Text style={{
                  marginTop: 10,
                  marginLeft: 10,
                  color: 'black',
                  fontFamily: "Poppins-Medium"
                  // fontFamily:"Roboto",
                }}>{Application_Constant.signup}</Text>
              </TouchableOpacity>
              <View style={{ width: '1%' }}>
                <View style={{
                  width: 1,
                  height: '100%',

                  backgroundColor: Colors.COLOR.darkBlue,
                }}></View>
              </View>

              <TouchableOpacity style={{ width: '50%', flexDirection: 'row', justifyContent: 'center', }
            }
            onPress={() => this.forLogin()}>
                <Image 
                style={Styles.cardThumbhnail_Stle}
                  source={require('../assets/logout_icon.png')} ></Image>

                <Text style={{
                  marginTop: 10,
                  marginLeft: 10,
                  color: 'black',
                  fontFamily: "Poppins-Medium",

                }}
                >{Application_Constant.login}</Text>

              </TouchableOpacity>

              {/* <TouchableOpacity style={{
                flex: 1,
                marginTop: 60,
                height: 65,
                right: 10,
                left: 10,
                position: 'absolute',
              }}
              onPress={this.callNumber}>
                <LinearGradient
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    backgroundColor: 'white',
                    borderRadius: 13, }}
                  colors={['#7CB5F8', '#4C94E6']}
                  start={{ x: 0, y: 1 }}
                  end={{ x: 1, y: 1 }}>
                  <TouchableOpacity style={{ width: '100%', flexDirection: 'row', alignItems: "center", marginRight: 5, marginLeft: 15, paddingLeft: 10, }}
                  onPress={this.callNumber}>
                    <Image style={{ width: 40, height: 40, }}
                      source={require('../assets/call_icon.png')}></Image>
                    <Text style={{ width: "100%", fontFamily: "Poppins-Regular", paddingLeft: 6, color: "white", flex: 1, fontSize: 13, paddingRight: 5, }}>Je suis en situation d’urgence </Text>
                  </TouchableOpacity>
                </LinearGradient>
              </TouchableOpacity> */}

            </Card>

          </View>

        </View>




      </View>

    );
  }
}

