import {Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');
const [shortDimension, longDimension] =
  width < height ? [width, height] : [height, width];

// Default guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const roundValue = value => Number(Math.round(value * 100) / 100);
const scale = size => (shortDimension / guidelineBaseWidth) * size;
const verticalScale = size => (longDimension / guidelineBaseHeight) * size;
const moderateScale = (size, factor = 0.5) =>
  roundValue(size + (scale(size) - size) * factor);
const deviceWidth = width;
const deviceHeight = height;

export const size = {
  scale,
  moderateScale,
  verticalScale,
  deviceWidth,
  deviceHeight,
};
