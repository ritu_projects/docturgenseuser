import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  ScrollView,
  Button,
  Text,
  TouchableOpacity,
  ImageBackground,
  Linking,
  PermissionsAndroid,
  Alert,
  Modal,
  TextInput,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  Platform,
  StatusBar,
} from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Styles from '../styles/styles_home';
import { Card, CardItem, Icon, Thumbnail, Row, TabHeading } from 'native-base';
import Colors from '../colors/colors';
import Sidebar from './Sidebar';
import Geolocation from '@react-native-community/geolocation';
import Application_Constant from './localization';
import speechbubble from '../assets/speech-bubble.png';
import rszspeechbubble from '../assets/rsz_speech-bubble.png';
import size from './size';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import { SafeAreaView } from 'react-native-safe-area-context';

import PopupDialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import axios from 'axios';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import normalize from 'react-native-normalize';

var radiogroup_options = [
  { label: 'M', value: 0 },
  { label: 'F', value: 1 },

];
import LinearGradient from 'react-native-linear-gradient';


var radiogroup_options_slection = [


  { label: 'Adulte', value: 0 },
  { label: 'enfant', value: 1 },
];

const steps = [
  {
    id: '0',
    message: 'Welcome to react chatbot!',
    trigger: '1',
  },
  {
    id: '1',
    message: 'Bye!',
    end: true,
  },
];
import moment from 'moment';
let today = '';

let cat_selection = ''
let gender_selection = ''

import Image from 'react-native-fast-image';

//import { Picker } from '@react-native-picker/picker';
import Picker from '@gregfrench/react-native-wheel-picker';
import { showLocation, Popup } from 'react-native-map-link'
import Clipboard from '@react-native-community/clipboard';


//var PickerItemm = Pickerr.Item;


var PickerItem = Picker.Item;

let itemListt = [];

export async function requestLocationPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Example App',
        message: 'Example App access to your location ',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the location');
      //alert("You can use the location");
    } else {
      console.log('location permission denied');
      // alert("Location permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
}
const STATUS_BAR_THEME = Platform.OS === 'ios' ? 'dark-content' : 'light-content';
import Commonclass from './Commonclass'

const MAP_KEY = Platform.OS === 'ios' ? 'AIzaSyCnbHX-Ru48dDVJ54Idksgi-XIoQRnu9cc' : 'AIzaSyBvSOmwTVyq-rba3RJ0VABVIKHU18v49aI';

export default class AfterLoginNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentlatitude: 48.953814,
      currentlongitude: 2.899333,
      modalVisible: false,
      modalVisible2: false,
      modalVisible3: false,
      modalVisible4: false,
      modalVisible5: false,
      isUpdate: false,
      animating: false,
      lodingDialog: false,
      appointment_status: '',
      request_status: '',
      appointment_id: '',
      admin_telephone: '',
      userId: '',
      token: '',
      subcat_id: '',
      admin_telephone: '',
      markerdialog_title: 'Lariboisiere S.A.U.',
      reachingtime: '20',
      googlemapTime: '',
      professional_address: '',
      msg: '',
      msgErr: '',
      genderErr: '',
      gender: '',
      slectionErr: '',
      slection: '',
      hospital_Id: '',
      number: 5,
      subCatdata: [],

      appMessage: [],

      phonenumber: '+33 998877667',
      specialist_latitude: 0,
      specialist_longitude: 0,
      initialRegion: '',
      markerss: [
        {
          coordinates: {
            latitude: 48.953814,
            longitude: 2.899333,
          },
        },
      ],

      markers: [],
      stage_one: 'Hello, ',
      individual_cat_id: '',
      rdvData: [],
      rdvTime: '',
      rdvTimeErr: '',
      selected_index: '',
      selected_data: [],
      selectedItem: 1,
      itemList: [],
      selected1: false,
      seleted2: false,
      value: -1,
      value2: -1,
      opn_map_dlg1: false,
      opn_map_dlg2: false,
    };
    today = moment().format('DD/MM/YYYY');
    console.log('Day ', today);
  }
  showModal() {
    this.setState({ modalVisible: true });
  }
  hideModal() {
    this.setState({ modalVisible: false });

    //  this.refs.PhoneInput._root.focus()
  }

  hideModal5() {
    this.setState({
      modalVisible5: false,
      selected_index: '',
    });

    //  this.refs.PhoneInput._root.focus()
  }
  reachLocation() {
    this.setState({
      animating: true,
      lodingDialog: true,
    });
    console.log('Request Appointment UserId' + this.state.userId);

    axios
      .post(
        Application_Constant.BaseUrl + 'api/ireachedlocation',
        {
          hospital_id: this.state.hospital_Id,
          user_id: this.state.userId,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'user-id': this.state.userId,
            token: this.state.token,
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('UserRequest is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
          alert(res_data_msg);
        } else if (res_data == 'false') {
          this.setState({
            animating: false,
            lodingDialog: false,
            msg: '',
          });
          this.props.navigation.navigate('AfterLoginNew');
        }
      });
    try {
      this.hideModal();
    } catch (e) { }
    try {
      this.hideModal3();
    } catch (e) { }

    this.openMap();
  }

  openMap() {
    console.log('Current lat map%%%' + this.state.currentlatitude);
    console.log('Current lng map%%%' + this.state.currentlongitude);

    console.log('Current dest lat map%%%' + this.state.specialist_latitude);
    console.log('Current dest lng map%%%' + this.state.specialist_longitude);

    var s_lat_val = this.state.currentlatitude;
    var s_lng_val = this.state.currentlongitude;

    var d_lat_val = this.state.specialist_latitude;
    var d_lng_val = this.state.specialist_longitude;
    this.setState({
      opn_map_dlg1: true,

    })


    // showLocation({
    //   latitude: s_lat_val,
    //   longitude: s_lng_val,
    //   sourceLatitude: d_lat_val,  // optionally specify starting location for directions
    //   sourceLongitude: d_lng_val,  // not optional if sourceLatitude is specified
    //   googleForceLatLon: false,  // optionally force GoogleMaps to use the latlon for the query instead of the title
    //   googlePlaceId: MAP_KEY,  // optionally specify the google-place-id
    //   dialogTitle: 'This is the dialog Title', // optional (default: 'Open in Maps')
    //   dialogMessage: 'This is the amazing dialog Message', // optional (default: 'What app would you like to use?')
    //   cancelText: 'This is the cancel button text', // optional (default: 'Cancel')
    // })


    // Platform.select({
    //   ios: () => {
    //     Linking.openURL(
    //       'http://maps.apple.com/maps?saddr=' +
    //       s_lat_val +
    //       ',' +
    //       s_lng_val +
    //       '&daddr=' +
    //       d_lat_val +
    //       ',' +
    //       d_lng_val,
    //     );
    //   },
    //   android: () => {
    //     Linking.openURL(
    //       'http://maps.google.com/maps?saddr=' +
    //       s_lat_val +
    //       ',' +
    //       s_lng_val +
    //       '&daddr=' +
    //       d_lat_val +
    //       ',' +
    //       d_lng_val,
    //     );
    //   },
    // })();
  }
  select_open_option = () => {

  }

  showModal3() {
    this.setState({ modalVisible3: true });
  }
  hideModal3() {
    this.setState({ modalVisible3: false });

    //  this.refs.PhoneInput._root.focus()
  }
  showModal2() {
    this.setState({
      modalVisible: false,
      modalVisible2: true,
    });
  }

  callChatScreen(appointment_id) {
    this.setState({
      modalVisible3: false,
    });
    console.log('Appointment id  ' + appointment_id);
    console.log('Appointment Reciver id  ' + this.state.hospital_Id);

    this.props.navigation.navigate('Chat', {
      appointment_id: this.state.appointment_id,
      userIdd_receiverId: this.state.hospital_Id,
    });
  }
  sendRequesttoSpecialist() {
    this.setState({
      animating: true,
      lodingDialog: true,
    });
    console.log('Request Appointment UserId' + this.state.userId);
    console.log('Request Appointment hospital_Id' + this.state.hospital_Id);
    console.log(
      'Request Appointment category_id' + this.state.individual_cat_id,
    );

    console.log('Request Appointment request_date' + today);

    console.log('Request Appointment request_time' + this.state.rdvTime);
    console.log('Request Appointment message' + this.state.msg);
    console.log('Request Appointment age_group' + this.state.slection);
    console.log('Request Appointment gender' + this.state.gender);
    if (this.state.value == 1) {
      cat_selection = "Adulte"
    }
    else if (this.state.value == 2) {
      cat_selection = "enfant"

    }

    if (this.state.value2 == 1) {
      gender_selection = "M"
    }
    else if (this.state.value2 == 2) {
      gender_selection = "F"

    }
    axios
      .post(
        Application_Constant.BaseUrl + 'api/userRequestappointment',
        {
          hospital_id: this.state.hospital_Id,
          category_id: this.state.individual_cat_id,
          request_date: today,
          request_time: this.state.rdvTime,
          user_id: this.state.userId,
          message: this.state.msg,
          book_status: '1',
          age_group: cat_selection,
          gender: gender_selection,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'user-id': this.state.userId,
            token: this.state.token,
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('UserRequest is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
          //  alert(res_data_msg)
        } else if (res_data == 'false') {
          this.setState({
            animating: false,
            lodingDialog: false,
            msg: '',
          });
          this.props.navigation.navigate('AfterLoginNew');
        }
      });
  }

  hideModal2() {
    this.setState({
      modalVisible2: false,
    });
  }

  componentDidMount() {
    AsyncStorage.getItem('phonenumber').then(phonenumber => {
      console.log('Phonenumber  ' + phonenumber);

      this.setState({
        phonenumber: phonenumber,
      });
    });

    if (Platform.OS == 'android') {
      requestLocationPermission();
    }
    this.getCurrentPosition();

    this.getAppMessage();

    this.getallSubCategories();

    const { navigation } = this.props;
    navigation.addListener('willFocus', () => {
      console.log('wiilFocus ',)
      AsyncStorage.getItem('phonenumber').then(phonenumber => {
        console.log('Phonenumber  ' + phonenumber);

        this.setState({
          phonenumber: phonenumber,
        });
      });

      if (Platform.OS == 'android') {
        requestLocationPermission();
      }
      this.getCurrentPosition();

      this.getAppMessage();

      this.getallSubCategories();

    });

    this.checkSession()
  }

  checkSession() {
    this.refs.commonclass.callAPI()
  }

  validation() {
    var isValidate = 0;
    console.log('slection validation::::', this.state.value);
    if (this.state.value != -1) {
      isValidate += 1;

    }
    else {
      console.log('cat empty value::::', this.state.value);
      isValidate -= 1;
      this.setState({
        slectionErr: Application_Constant.ShouldemptyText,
      });
    }
    // if(this.state.slection == 0 || this.state.slection == 1)
    // {
    //   isValidate += 1;

    // }
    // else {
    //   console.log('cat empty::::',this.state.slection);
    //   isValidate -= 1;
    //   this.setState({
    //     slectionErr: Application_Constant.ShouldemptyText,
    //   });
    // }
    // if (this.state.slection != '' && this.state.slection !="" && this.state.slection !=null && this.state.slection != -1) {
    //   isValidate += 1;
    // } else {
    //   console.log('cat empty::::',this.state.slection);
    //   isValidate -= 1;
    //   this.setState({
    //     slectionErr: Application_Constant.ShouldemptyText,
    //   });
    // }
    if (this.state.value2 != -1) {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        genderErr: Application_Constant.ShouldemptyText,
      });
    }
    if (this.state.msg != '') {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        msgErr: Application_Constant.ShouldemptyText,
      });
    }
    if (isValidate == 3) {
      isValidate += 1;
      this.setState({
        modalVisible2: false,
      });

      this.sendRequesttoSpecialist();
    }
  }

  getAppMessage() {
    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);

      var useridd = JSON.parse(userid);
      console.log('User ID^^^' + useridd);

      this.setState({
        userId: useridd,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      console.log('User Token^^^' + tokenn);

      this.setState({
        token: tokenn,
      });

      console.log('User Token%%%' + this.state.token);
      this.setState({
        animating: true,
        lodingDialog: true,
      });

      axios
        .get(Application_Constant.BaseUrl + 'api/getAppMessage')
        .then(response => response.data)
        .then(data => {
          console.log('Data is getAppMessage   ' + JSON.stringify(data));
          var res_data = data.error;
          console.log('Data Res is  ' + res_data);

          console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            this.setState({
              animating: false,
              lodingDialog: false,
              appMessage: [],
            });
          } else if (res_data == 'false') {
            this.setState({
              appMessage: [],
            });
            this.setState({
              animating: false,
              lodingDialog: false,
              appMessage: data.record,
            });
            console.log('getAppMessage Data ', this.state.appMessage.length);
          }
        });
    });
  }
  getallSubCategories() {
    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);

      var useridd = JSON.parse(userid);
      console.log('User ID^^^' + useridd);

      this.setState({
        userId: useridd,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      console.log('User Token^^^' + tokenn);

      this.setState({
        token: tokenn,
      });

      console.log('User Token%%%' + this.state.token);
      this.setState({
        animating: true,
        lodingDialog: true,
      });

      axios
        .get(
          Application_Constant.BaseUrl + 'api/getAllSubCategory',
          //{
          //     user_id: this.state.userId,
          // },
          //     {
          //         headers: {

          //             'user-id': this.state.userId,
          //             'token': this.state.token,
          //             'Content-Type': 'application/json',

          //             //other header fields
          //         }
          //  }
        )
        .then(response => response.data)
        .then(data => {
          console.log('Data is Category   ' + JSON.stringify(data));
          var res_data = data.error;
          console.log('Data Res is  ' + res_data);

          console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            this.setState({
              animating: false,
              lodingDialog: false,
              subCatdata: [],
              itemList: [],
            });
            // alert(res_data_msg)
          } else if (res_data == 'false') {
            this.setState({
              subCatdata: [],
              itemList: [],
            });

            var obj = data.record;
            console.log('resoJSon getAllSubCategory obj===' + obj);

            var count = Object.keys(obj).length;
            console.log('resoJSon getAllSubCategory count===' + count);

            itemListt = [];
            for (var i = 0; i < count; i++) {
              itemListt.push(obj[i].sub_cat_name);
            }
            this.setState({
              animating: false,
              lodingDialog: false,
              subCatdata: data.record,
              itemList: itemListt,
            });
            console.log('SubCat Data ', this.state.subCatdata.length);
          }
          this.updateLatlong_ofUser();

          this.getHospital(this.state.subcat_id);
        });
    });
  }
  getSubCat_Id(item_subcatid) {
    console.log('Item SubCat Id   ' + item_subcatid);
    this.setState({
      subcat_id: item_subcatid,
    });
    // this.showModal();
    this.getHospital(item_subcatid);
  }

  getIndividualHospital_Details(hospitl_id) {
    this.setState({
      animating: true,
      lodingDialog: true,
    });
    axios
      .post(
        Application_Constant.BaseUrl + 'api/getHospitalDetail',
        {
          hospital_id: hospitl_id,
          user_id: this.state.userId,
        },
        {
          headers: {
            'user-id': this.state.userId,
            token: this.state.token,
            'Content-Type': 'application/json',

            //other header fields
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('Data GetIndividual is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res AllHospital is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True AllHospital is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
            reachingtime: 'Aucune donnée trouvée',
            markerdialog_title: 'Aucune donnée trouvée',
            googlemapTime: 'Aucune donnée trouvée',
            professional_address: 'Aucune donnée trouvée',
          });
          // alert(res_data_msg)
        } else if (res_data == 'false') {
          this.setState({
            reachingtime: data.record.waiting_time,
            markerdialog_title: data.record.fullname,
            googlemapTime: data.record.current_time,
            professional_address: data.record.address,
            hospital_Id: hospitl_id,
            appointment_status: data.book_status,
            request_status: data.request_status,
            specialist_latitude: data.record.latitude,
            specialist_longitude: data.record.longitude,
            individual_cat_id: data.record.category,
            animating: false,
            lodingDialog: false,
            admin_telephone: data.admin_telephone,
          });
          console.log(
            'Request appointment Status  ' + this.state.appointment_status,
          );

          if (
            this.state.appointment_status != null &&
            this.state.appointment_status != '' &&
            this.state.appointment_status != 'null' &&
            this.state.appointment_status == 1
          ) {
            console.log('Request Status  ' + this.state.request_status);

            if (this.state.request_status == 0) {
              //  alert("Vous avez déjà envoyé des demandes pour ces hôpitaux")

              this.setState({ modalVisible4: true });
            } else if (this.state.request_status == 1) {
              this.setState({
                appointment_id: data.appointment_id,
                admin_telephone: data.admin_telephone,
              });
              this.showModal3();
            }
          } else {
            this.showModal();
          }
        }
      });
  }

  updateLatlong_ofUser() {
    setInterval(() => {
      if (this.state.number == 0) {
        this.setState({ number: 5 });
        console.log('Current Latitude   ' + this.state.currentlatitude);
        console.log('Current Longitude' + this.state.currentlongitude);

        axios
          .post(
            Application_Constant.BaseUrl + 'api/update_lat_long',
            {
              user_id: this.state.userId,
              latitude: this.state.currentlatitude,
              longitude: this.state.currentlongitude,
            },
            {
              headers: {
                'user-id': this.state.userId,
                token: this.state.token,
                'Content-Type': 'application/json',

                //other header fields
              },
            },
          )
          .then(response => response.data)
          .then(data => {
            console.log('Data is  ' + JSON.stringify(data));
            var res_data = data.error;
            console.log('Data Res is  ' + res_data);

            console.log(res_data);
            if (res_data == 'true') {
              var res_data_msg = data.errorMessage;
              console.log('Response True is  ' + res_data_msg);


            } else if (res_data == 'false') {

            }
          });
        this.getCurrentPosition()


      }
      this.setState({ number: parseInt(this.state.number, 10) - 1 });
    }, 2000);
  }
  // async moveToRegion() {
  //     console.log("Icon Click " + "Icon Click ")
  //     await navigator.geolocation.getCurrentPosition(
  //         (geoLocation) => {

  //             let region = {
  //                 latitude: parseFloat(geoLocation.latitude),
  //                 longitude: parseFloat(geoLocation.longitude),
  //                 latitudeDelta: 5,
  //                 longitudeDelta: 5
  //             };
  //             this.setState({
  //                 initialRegion: region

  //             });
  //             this.map.animateToRegion(this.state.initialRegion, 2000);

  //         });

  // }
  getHospital = subcaty_mthod => {
    console.log('Latitude Current  ' + this.state.currentlatitude);
    console.log('Longitude Current  ' + this.state.currentlongitude);
    console.log('SubCat id  ' + subcaty_mthod);
    this.setState({
      animating: true,
      lodingDialog: true,
    });
    axios
      .post(
        Application_Constant.BaseUrl + 'api/allHospital',
        {
          current_lat: this.state.currentlatitude,
          current_long: this.state.currentlongitude,
          category_id: subcaty_mthod,
          user_id: this.state.userId,
        },
        {
          headers: {
            'user-id': this.state.userId,
            token: this.state.token,
            'Content-Type': 'application/json',

            //other header fields
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('Data AllHospital is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res AllHospital is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True AllHospital is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
            markers: [],
          });
          //   alert(res_data_msg)
          this.setState({
            animating: false,
            lodingDialog: false,
          });
        } else if (res_data == 'false') {
          var obj = data.record;
          console.log('resoJSon GetVehicle obj===' + obj);

          var count = Object.keys(obj).length;
          console.log('resoJSon GetVehicle count===' + count);

          var hospitalMarker = [];
          this.setState({
            markers: [],
          });
          for (var i = 0; i < count; i++) {
            console.log('Latitude is%%%' + obj[i].category);

            //     if (obj[i].status != 2) {
            //         hospitalMarker.push({
            //             title: obj[i].admin_name,
            //             status: obj[i].status,

            //             coordinate: {
            //                 latitude: JSON.parse(obj[i].latitude),
            //                 longitude: JSON.parse(obj[i].longitude),
            //             },
            //             id: obj[i].id,
            //             image: require("../assets/markerbg.png")

            //         });
            //     }
            //     else if (obj[i].status == 2) {
            //         hospitalMarker.push({
            //             title: obj[i].admin_name,
            //             status: obj[i].status,

            //             coordinate: {
            //                 latitude: JSON.parse(obj[i].latitude),
            //                 longitude: JSON.parse(obj[i].longitude),
            //             },
            //             id: obj[i].id,
            //             image: require("../assets/map2bg.png")

            //         });
            //     }

            if (obj[i].category == 1) {
              hospitalMarker.push({
                title: obj[i].admin_name,
                status: obj[i].status,

                coordinate: {
                  latitude: JSON.parse(obj[i].latitude),
                  longitude: JSON.parse(obj[i].longitude),
                },
                id: obj[i].id,
                image: require('../assets/Hopitaux(emergency)1.png'),
              });
            } else if (obj[i].category == 2) {
              hospitalMarker.push({
                title: obj[i].admin_name,
                status: obj[i].status,

                coordinate: {
                  latitude: JSON.parse(obj[i].latitude),
                  longitude: JSON.parse(obj[i].longitude),
                },
                id: obj[i].id,
                image: require('../assets/Cabinetcpts2.png'),
              });
            } else if (obj[i].category == 3) {
              hospitalMarker.push({
                title: obj[i].admin_name,
                status: obj[i].status,

                coordinate: {
                  latitude: JSON.parse(obj[i].latitude),
                  longitude: JSON.parse(obj[i].longitude),
                },
                id: obj[i].id,
                image: require('../assets/Cabinetelec3.png'),
              });
            } else if (obj[i].category == 4) {
              console.log('Category 4 ', obj[i].category);
              hospitalMarker.push({
                title: obj[i].admin_name,
                status: obj[i].status,

                coordinate: {
                  latitude: JSON.parse(obj[i].latitude),
                  longitude: JSON.parse(obj[i].longitude),
                },
                id: obj[i].id,
                image: require('../assets/Medecin4.png'),
              });
            }
          }

          var obj2 = JSON.stringify(hospitalMarker);
          console.log('Hospital is   ' + obj2);

          this.setState({
            //     markers.push({
            //     coordinate: {
            //         latitude: JSON.parse(res_data.record.latitude),
            //         longitude: JSON.parse(res_data.record.longitude),
            //         title: JSON.parse(res_data.record.fullname),
            //     },
            // })
            markers: hospitalMarker,
          });

          this.setState({
            animating: false,
            lodingDialog: false,
          });
        }
      });
  };

  callRDV = () => {
    console.log('Today  callRDV  ' + today);
    console.log('hospital_Id id  ' + this.state.hospital_Id);
    this.setState({
      animating: true,
      lodingDialog: true,
      modalVisible: false,
      isUpdate: false,
    });
    axios
      .post(
        Application_Constant.BaseUrl + 'api/getHospitalFinalTimes',
        {
          booking_date: today,
          hospital_id: this.state.hospital_Id,
        },
        {
          headers: {
            'user-id': this.state.userId,
            token: this.state.token,
            'Content-Type': 'application/json',
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('Data getHospitalTimes is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res getHospitalTimes is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True getHospitalTimes is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
          alert(res_data_msg);
        } else if (res_data == 'false') {
          console.log(
            'resoJSon drop_down_subcategorydata_daily false===' +
            JSON.stringify(data.records),
          );

          this.setState({
            rdvData: [],
          });

          this.setState({
            animating: false,
            lodingDialog: false,
            rdvData: data.records,

            modalVisible5: true,
          });
        }
      });
  };

  callRDV_forAppointmentUpdate = () => {
    console.log('Today  callRDV Uodate  ' + today);
    console.log('hospital_Id id Uodate  ' + this.state.hospital_Id);
    this.setState({
      animating: true,
      lodingDialog: true,
      modalVisible3: false,
      isUpdate: true,
    });
    axios
      .post(
        Application_Constant.BaseUrl + 'api/getHospitalFinalTimes',
        {
          booking_date: today,
          hospital_id: this.state.hospital_Id,
        },
        {
          headers: {
            'user-id': this.state.userId,
            token: this.state.token,
            'Content-Type': 'application/json',
          },
        },
      )
      .then(response => response.data)
      .then(data => {
        console.log('Data getHospitalTimes is  ' + JSON.stringify(data));
        var res_data = data.error;
        console.log('Data Res getHospitalTimes is  ' + res_data);

        console.log(res_data);
        if (res_data == 'true') {
          var res_data_msg = data.errorMessage;
          console.log('Response True getHospitalTimes is  ' + res_data_msg);

          this.setState({
            animating: false,
            lodingDialog: false,
          });
          alert(res_data_msg);
        } else if (res_data == 'false') {
          this.setState({
            rdvData: [],
          });

          this.setState({
            animating: false,
            lodingDialog: false,
            rdvData: data.records,

            modalVisible5: true,
          });
        }
      });
  };

  getCurrentPosition = () => {
    if (Platform.OS == 'ios') {


      Geolocation.requestAuthorization();
    }

    Geolocation.getCurrentPosition(info => {
      console.log("latitude:::" + info);
      // console.log("longitude:::" + info.coords.longitude);



      let region = {
        latitude: info.coords.latitude,
        longitude: info.coords.longitude,

        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }
      this.setState({
        currentlatitude: info.coords.latitude,
        currentlongitude: info.coords.longitude,
        initialRegion: region,
      })

    },
      (error) => {
        let region = {
          latitude: 48.8416,
          longitude: 2.2941,

          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        };
        this.setState({
          currentlatitude: 48.8416,
          currentlongitude: 2.2941,
          initialRegion: region,
        });

        console.log(error.message)

      },
    { enableHighAccuracy: true, timeout: 15000, maximumAge: 1000 }


    );

    Geolocation.watchPosition(success => {
      console.log("success watchman:::" + JSON.stringify(success));

      let region = {
        latitude: success.coords.latitude,
        longitude: success.coords.longitude,
        // latitude: 22.7369395,
        // longitude: 75.8716091,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }
      this.setState({
        initialRegion: region,

        currentlatitude: success.coords.latitude,
        currentlongitude: success.coords.longitude,
      })

    })

  };

  getCurrentPosition_animated = () => {
    console.log('Current Location ', 'Current Location')
    this.map.animateToRegion(this.state.initialRegion);



    // if (Platform.OS == 'ios') {


    //   Geolocation.requestAuthorization();
    // }

    // Geolocation.getCurrentPosition(info => {
    //   console.log("latitude:::" + info);
    //   // console.log("longitude:::" + info.coords.longitude);



    //   let region = {
    //     latitude: info.coords.latitude,
    //     longitude: info.coords.longitude,

    //     latitudeDelta: 0.0922,
    //     longitudeDelta: 0.0421,
    //   }
    //   this.setState({
    //     currentlatitude: info.coords.latitude,
    //     currentlongitude: info.coords.longitude,
    //     initialRegion: region,
    //   })
    //   this.map.animateToRegion(this.state.initialRegion);

    // },
    //   (error) => {
    //     let region = {
    //       latitude: 48.8416,
    //       longitude: 2.2941,

    //       latitudeDelta: 0.0922,
    //       longitudeDelta: 0.0421,
    //     };
    //     this.setState({
    //       currentlatitude: 48.8416,
    //       currentlongitude: 2.2941,
    //       initialRegion: region,
    //     });
    //     this.map.animateToRegion(this.state.initialRegion);

    //     console.log(error.message)

    //   },


    // );


  }

  componentWillUnmount = () => {
    Geolocation.clearWatch(this.watchID);
  }


  forLogin = () => { };
  callNumber = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };
  callNumber2 = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.admin_telephone + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.admin_telephone + '}';
    }

    Linking.openURL(phoneNumber);
  };
  updateAppointment = () => {
    this.setState({
      isUpdate: true,
    });
  };
  timeValidation = () => {
    var isValidate = 0;

    if (this.state.rdvTime != '') {
      isValidate += 1;
    } else {
      console.log('cat empty::::');
      isValidate -= 1;
      this.setState({
        rdvTimeErr: Application_Constant.ShouldemptyText,
      });
    }

    if (isValidate == 1) {
      this.setState({
        modalVisible5: false,
        selected_index: '',
      });

      if (this.state.isUpdate == false) {
        this.showModal2();
      } else if (this.state.isUpdate == true) {
        this.upteTimeSlot();
      }
    }
  };

  upteTimeSlot = () => {
    console.log('Appointment Id Update^^^' + this.state.appointment_id);
    console.log('Appointment Id rdvTime^^^' + this.state.rdvTime);

    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);

      var useridd = JSON.parse(userid);
      console.log('User ID^^^' + useridd);

      this.setState({
        userId: useridd,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      console.log('User Token^^^' + tokenn);

      this.setState({
        token: tokenn,
      });

      console.log('User Token%%%' + this.state.token);
      this.setState({
        animating: true,
        lodingDialog: true,
      });
      axios
        .post(
          Application_Constant.BaseUrl + 'api/resheduleAppointment',
          {
            appointment_id: this.state.appointment_id,
            request_time: this.state.rdvTime,
          },
          {
            headers: {
              'user-id': this.state.userId,
              token: this.state.token,
              'Content-Type': 'application/json',

              //other header fields
            },
          },
        )
        .then(response => response.data)
        .then(data => {
          console.log('Data is  ' + data);
          var res_data = data.error;
          console.log('Data Res is  ' + res_data);

          console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);
            this.setState({
              animating: false,
              lodingDialog: false,
            });
            alert(res_data_msg);
          } else if (res_data == 'false') {
            this.setState({
              animating: false,
              lodingDialog: false,
            });

            this.componentDidMount();
          }
        });
    });
  };
  cancelAppointment = () => {
    console.log('Appointment Id Cancel^^^' + this.state.appointment_id);

    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);

      var useridd = JSON.parse(userid);
      console.log('User ID^^^' + useridd);

      this.setState({
        userId: useridd,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      console.log('User Token^^^' + tokenn);

      this.setState({
        token: tokenn,
      });

      console.log('User Token%%%' + this.state.token);
      this.setState({
        modalVisible3: false,
        animating: true,
        lodingDialog: true,
      });
      axios
        .post(
          Application_Constant.BaseUrl + 'api/userCancelAppointment',
          {
            appointment_id: this.state.appointment_id,
          },
          {
            headers: {
              'user-id': this.state.userId,
              token: this.state.token,
              'Content-Type': 'application/json',

              //other header fields
            },
          },
        )
        .then(response => response.data)
        .then(data => {
          console.log('Data is  ' + data);
          var res_data = data.error;
          console.log('Data Res is  ' + res_data);

          console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            this.setState({
              animating: false,
              lodingDialog: false,
            });

            alert(res_data_msg);
          } else if (res_data == 'false') {
            this.setState({
              animating: false,
              lodingDialog: false,
            });

            this.componentDidMount();
          }
        });
    });
  };


  // onSelect(option) {
  //   var opp = JSON.stringify(option);
  //   console.log('Value   ' + opp);
  //   var opp2 = option.value;
  //   console.log('Value2   ' + opp2);

  //   this.setState({
  //     gender: opp2,
  //     genderErr: '',
  //   });
  // }

  // onSelect2(option) {
  //   var opp = JSON.stringify(option);
  //   console.log('Value   ' + opp);
  //   var opp2 = option.value;
  //   console.log('Value2   ' + opp2);

  //   this.setState({
  //     slection: opp2,
  //     slectionErr: '',
  //   });
  // }
  onMarkerClick = evt => {
    console.log('Marker is$$$' + evt);
    // this.AlertPro.open();
    this.getIndividualHospital_Details(evt);
  };

  checkValueforSelected = (timesloats, availibility, indexx) => {
    console.log('checkValueforSelected timesloats ', timesloats);
    console.log('checkValueforSelected availibility ', availibility);
    console.log('checkValueforSelected indexx ', indexx);


    if (availibility == 1) {
      this.setState({
        rdvTime: timesloats,
        selected_index: indexx,
      });
    } else {
      alert('Impossible à sélectionner');
    }
  };

  handleScroll = () => {
    //console.log("FlatList Scroll ",this.flatList1.getNativeScrollRef())
    //this.flatList1.scrollToOffset({ animated: true, offset: 0 });
  };

  onPickerSelect(index) {
    this.setState({
      animating: true,
      lodingDialog: true,
    });
    this.setState({
      selectedItem: index,
    });
    console.log('Index is ', index);

    var item_sub_catidd = this.state.subCatdata[index].id;
    console.log('Item SubCat Id   ' + item_sub_catidd);
    this.setState({
      subcat_id: item_sub_catidd,
    });
    this.setState({
      animating: false,
      lodingDialog: false,
    });
    // this.showModal();
    this.getHospital(item_sub_catidd);
  }
  toggleDrawer = () => {
    this.props.navigation.openDrawer()
  };

  render() {
    console.log('SubCat Length Render ', this.state.subCatdata.length);
    return (
      <View
        style={Styles.containerWhite}>
        {Platform.OS == 'ios' ?

          <StatusBar
            translucent={true}
            backgroundColor={'#324048'}
            barStyle={STATUS_BAR_THEME} />
          :
          null
        }
        <Commonclass ref="commonclass" />
        <LinearGradient
          // source={require('./assets/download.png')}

          style={{
            flexDirection: 'row',
            height: normalize(30),
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: Platform.OS == 'ios' ? normalize(25) : normalize(0),
            marginBottom: normalize(-5),
          }}
          // colors={['white', 'rgba(0,128,0,0.2)']}
          colors={['#e7eef1', 'rgba(203,229,216,255)']}>
          <TouchableOpacity
            style={{
              width: '15%',
              flexDirection: 'row',
              marginLeft: 10,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={this.toggleDrawer.bind(this)}>
            {/*Donute Button Image */}
            <Image
              source={require('../assets/drawer.png')}
              style={{ width: 25, height: 25 }}
            />
          </TouchableOpacity>
          <View
            style={{
              width: '70%',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'Montserrat-Bold',
                color: '#2da9ff',
                fontSize: 22,
              }}>{Application_Constant.homeTitle}</Text>
          </View>
          <TouchableOpacity
            style={{
              width: '15%',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 10,
            }}
            onPress={() => this.getCurrentPosition_animated()}>
            <Image
              source={require('../assets/pin_icon.png')}
              style={{ width: 25, height: 25 }}
            />
          </TouchableOpacity>
        </LinearGradient>
        {!!this.state.initialRegion ?
          <MapView
            style={Styles.map_bg}
            ref={map => {
              this.map = map;
            }}
            initialRegion={this.state.initialRegion}
            mapType={'standard'}
            provider={PROVIDER_GOOGLE}
            // customMapStyle={mapStyle}
            minZoomLevel={1}
            maxZoomLevel={20}
            enableZoomControl={true}
            //     showsUserLocation={true}
            //   showsMyLocationButton={true}
            zoomEnabled={true}>
            {this.state.markers.map((marker, i) => (
              <MapView.Marker
                coordinate={marker.coordinate}
                // title={marker.title}
                onPress={() => this.onMarkerClick(marker.id)}>
                <View style={{ width: '100%', height: 70, alignItems: 'center' }}>
                  {marker.status == 1 ||
                    marker.status == '1' ||
                    marker.status == '1' ? (
                    <Image
                      style={{
                        width: 40,
                        height: 40,
                        alignItems: 'center',
                        paddingTop: 3,
                      }}
                      source={require('../assets/map2bg.png')}
                    // source={marker.image}
                    >
                      <Image
                        style={{ width: 35, height: 35 }}
                        source={marker.image}
                      />
                    </Image>
                  ) : (
                    <Image
                      style={{ width: 40, height: 40, paddingTop: 3 }}
                      source={marker.image}
                    />
                  )}

                  <View
                    style={{
                      backgroundColor: 'white',
                      borderRadius: 3,
                      marginTop: 1,
                      flexDirection: 'row',
                      padding: 5,
                    }}>
                    <Text style={Styles.pinText}>{marker.title}</Text>
                  </View>
                </View>
              </MapView.Marker>
            ))}
          </MapView>
          : null}
        <View
          style={{
            position: 'absolute',

            bottom:
              Platform.OS == 'ios'
                ? normalize(45, 'height')
                : normalize(35, 'height'),
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',

          }}>
          {this.state.appMessage != null &&
            this.state.appMessage != [] &&
            this.state.appMessage.length > 0 &&
            this.state.modalVisible == false &&
            this.state.modalVisible2 == false &&
            this.state.modalVisible3 == false &&
            this.state.modalVisible4 == false &&
            this.state.modalVisible5 == false ? (
            <View
              style={{
                width: '95%',
                alignSelf: 'center',
              }}>
              <View
                style={{
                  width: '100%',
                }}>
                <View
                  style={{
                    borderWidth: 0,
                    borderRadius: 15,
                    height: 70,
                    alignSelf: 'flex-start',
                    backgroundColor: '#3fa9f5',
                    width: '69%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      textAlign: 'center',
                      color: 'white',
                      fontFamily: 'Montserrat-Regular',
                    }}>
                    {this.state.appMessage[0].messages}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  width: '1%',
                  position: 'absolute',
                }}
              />

              <View
                style={{
                  width: '30%',
                  alignSelf: 'flex-end',
                  position: 'absolute',
                  bottom: 15,
                  backgroundColor: 'transparent',
                }}>
                <Image
                  source={speechbubble}
                  //source={rszspeechbubble}
                  resizeMode="cover"
                  style={{ width: 123, height: 158 }}
                />
              </View>
            </View>
          ) : null}

          {Platform.OS == 'android' ?
            <View style={{

              width: '95%',

              height: 123,
              marginTop: 7,
              borderWidth: 1,
              borderRadius: 15,
              borderColor: 'rgba(16,157,255,255)',
              backgroundColor: 'rgba(16,157,255,255)',

            }}>

              {!!this.state.itemList && this.state.itemList.length > 0 ? (

                <Picker
                  style={{
                    marginLeft: 20,
                    marginRight: 20,


                    alignSelf: 'center',
                    width: '99%',
                    height: '100%',

                  }}
                  selectedValue={this.state.selectedItem}
                  itemStyle={{
                    fontFamily: 'Montserrat-Regular',
                    textAlign: 'center',
                    color: 'white',
                    fontSize: 22,
                    width: '100%',
                    height: 123,
                    backgroundColor: '#2da9ff',


                  }}

                  mode={'dropdown'}
                  onValueChange={index => this.onPickerSelect(index)}>
                  {this.state.itemList.map((value, i) => (
                    <PickerItem label={value} value={i} key={'money' + value} />
                  ))}
                </Picker>

              ) : null}
            </View>

            :
            <View style={{

              width: '95%',

              height: 150,
              marginTop: 7,
              borderWidth: 1,
              borderRadius: 15,
              borderColor: 'rgba(16,157,255,255)',
              backgroundColor: 'rgba(16,157,255,255)',


            }}>

              {!!this.state.itemList && this.state.itemList.length > 0 ? (

                <Picker
                  style={{

                    alignSelf: 'center',
                    width: '95%',
                    height: '95%',
                    backgroundColor: '#2da9ff',
                  }}
                  selectedValue={this.state.selectedItem}
                  itemStyle={{
                    fontFamily: 'Montserrat-Regular',
                    textAlign: 'center',
                    color: 'white',
                    fontSize: 22,
                    width: '100%',
                    height: 150,
                    backgroundColor: 'rgba(16,157,255,255)',

                  }}

                  onValueChange={index => this.onPickerSelect(index)}>
                  {this.state.itemList.map((value, i) => (
                    <PickerItem label={value} value={i} key={'money' + value} />
                  ))}
                </Picker>

              ) : null}
            </View>
          }





        </View>
        <PopupDialog
          onHardwareBackPress={() => {
            this.setState({ lodingDialog: false });
          }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
          <DialogContent>
            <View style={{ alignItems: 'center' }}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[
                  {
                    height: 10,
                    marginBottom: 10,
                    marginTop: 30,
                    marginLeft: 20,
                    marginRight: 20,
                  },
                ]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>

        <Modal
          animationType="slide"
          transparent={true}
          onHardwareBackPress={() => {
            this.setState({ modalVisible: false });
          }}
          // presentationStyle='fullScreen'
          width={0.9}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent',
            height: '100%',
          }}
          visible={this.state.modalVisible}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: normalize(10, 'height'),
              marginRight: normalize(10, 'height'),
            }}>

            {this.state.modalVisible == true ? (
              <View
                style={{
                  width: '95%',
                }}>
                <View
                  style={{
                    width: '100%',
                  }}>
                  <View
                    style={{
                      borderWidth: 0,
                      borderRadius: normalize(15),
                      height: normalize(70),
                      alignSelf: 'flex-start',
                      backgroundColor: '#3fa9f5',
                      width: '63%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {this.state.individual_cat_id == 1 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[0].messages}
                      </Text>
                    ) : null}
                    {this.state.individual_cat_id == 2 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[1].messages}
                      </Text>
                    ) : null}
                    {this.state.individual_cat_id == 3 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[2].messages}
                      </Text>
                    ) : null}
                    {this.state.individual_cat_id == 4 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[3].messages}
                      </Text>
                    ) : null}
                  </View>
                </View>

                <View
                  style={{
                    width: '1%',
                    position: 'absolute',
                  }}
                />

                <View
                  style={{
                    width: '36%',
                    alignSelf: 'flex-end',
                    position: 'absolute',
                    bottom: normalize(0),
                    backgroundColor: 'transparent',
                  }}>
                  <Image
                    source={speechbubble}
                    resizeMode="cover"
                    style={{ width: normalize(123), height: normalize(158) }}
                  />
                </View>
              </View>
            ) : null}

            <View
              // source={require('../assets/list_bg_time.png')}
              //  width={0.8}

              style={{
                width: '95%',
                justifyContent: 'center',
                padding: 5,
                marginTop: 5,
                alignSelf: 'center',
                backgroundColor: '#01a0f3',
                borderRadius: 15,

                alignItems: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  top: 10,
                  alignSelf: 'center',

                  width: '100%',
                }}>
                <Text
                  style={{
                    fontFamily: 'Poppins-Medium',
                    color: 'white',

                    fontSize: 33,
                    textTransform: 'uppercase',
                  }}>
                  {this.state.markerdialog_title}
                </Text>

                <TouchableOpacity
                  style={{
                    width: '10%',
                    position: 'absolute',
                    top: 5,
                    right: 5,
                  }}
                  onPress={() => this.hideModal()}>
                  <Image
                    source={require('../assets/cross_icon.png')}
                    style={{
                      width: 25,
                      height: 25,
                      marginRight: 0,
                    }}
                  />
                </TouchableOpacity>
              </View>

              <View
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: normalize(3, 'height'),
                  marginBottom: normalize(25, 'height'),
                  alignSelf: 'center',
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontFamily: 'Poppins-Regular',
                    paddingTop: normalize(3, 'height'),
                    color: 'white',
                    width: 210,
                    fontStyle: 'italic'

                  }}>
                  {this.state.professional_address}
                </Text>

                <View
                  style={{
                    width: '100%',
                    height: normalize(95, 'height'),
                    marginBottom: normalize(15, 'height'),

                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      width: normalize(75, 'height'),
                      height: normalize(75, 'height'),

                      top: normalize(10, 'height'),
                      flexDirection: 'row',
                    }}>
                    <TouchableOpacity
                      style={{
                        borderRadius: normalize(40, 'height'),
                        width: normalize(75, 'height'),
                        height: normalize(75, 'height'),

                        backgroundColor: 'white',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                      onPress={() => this.callNumber2()}>
                      <Image
                        source={require('../assets/call_new_icon.png')}
                        style={{
                          width: normalize(65, 'height'),
                          height: normalize(65, 'height'),
                        }}
                      />
                      {/* <Text style={{ color: 'white', textAlign: 'center', fontWeight: 'bold', }}>CALL</Text> */}
                    </TouchableOpacity>
                  </View>
                  {this.state.individual_cat_id == 3 ||
                    this.state.individual_cat_id == 4 ? (
                    <View
                      style={{
                        width: normalize(75, 'height'),
                        height: normalize(75, 'height'),
                        left: normalize(5, 'height'),
                        right: normalize(5, 'height'),
                        top: normalize(35, 'height'),
                        flexDirection: 'row',
                      }}>
                      <View
                        style={{
                          borderRadius: normalize(40, 'height'),
                          width: normalize(75, 'height'),
                          height: normalize(75, 'height'),
                          // backgroundColor: 'white',

                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                      // onPress={() => this.callRDV()}
                      >
                        <Image
                          //  source={require('../assets/rdv_icon.png')}
                          style={{
                            width: normalize(55, 'height'),
                            height: normalize(55, 'height'),
                          }}
                        />
                      </View>
                    </View>
                  ) : (
                    <View
                      style={{
                        width: normalize(75, 'height'),
                        height: normalize(75, 'height'),
                        left: normalize(5, 'height'),
                        right: normalize(5, 'height'),
                        top: normalize(35, 'height'),
                        flexDirection: 'row',
                      }}>
                      <View
                        style={{
                          borderRadius: normalize(40, 'height'),
                          width: normalize(75, 'height'),
                          height: normalize(75, 'height'),

                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                      //  onPress={() => this.callRDV()}
                      >
                        <Image
                          //source={require('../assets/rdv_icon.png')}
                          style={{
                            width: normalize(55, 'height'),
                            height: normalize(55, 'height'),
                          }}
                        />
                        {/* <Text style={{ color: 'white', textAlign: 'center', fontWeight: 'bold', }}>RDV</Text> */}
                      </View>
                    </View>
                  )}

                  {this.state.individual_cat_id == 1 || this.state.individual_cat_id == 2 || this.state.individual_cat_id == 3 ? (
                    <View
                      style={{
                        width: normalize(75, 'height'),
                        height: normalize(75, 'height'),
                        left: normalize(10, 'height'),
                        right: normalize(10, 'height'),
                        top: normalize(10, 'height'),
                        flexDirection: 'row',
                      }}>
                      <TouchableOpacity
                        style={{
                          borderRadius: normalize(40, 'height'),
                          width: normalize(75, 'height'),
                          height: normalize(75, 'height'),

                          backgroundColor: 'white',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                        onPress={() => {
                          this.setState({
                            modalVisible: false,
                          })
                          this.sendRequesttoSpecialist()
                          this.openMap()
                        }
                        }>
                        <Image
                          source={require('../assets/go_icon.png')}
                          style={{
                            width: normalize(55, 'height'),
                            height: normalize(55, 'height'),
                          }}
                        />
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <View
                      style={{
                        width: normalize(75, 'height'),
                        height: normalize(75, 'height'),
                        left: normalize(10, 'height'),
                        right: normalize(10, 'height'),
                        top: normalize(10, 'height'),
                        flexDirection: 'row',
                      }}>
                      <TouchableOpacity
                        style={{
                          borderRadius: normalize(40, 'height'),
                          width: normalize(75, 'height'),
                          height: normalize(75, 'height'),

                          backgroundColor: 'white',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                        onPress={() => this.showModal2()}>
                        <Image
                          source={require('../assets/go_icon.png')}
                          style={{
                            width: normalize(55, 'height'),
                            height: normalize(55, 'height'),
                          }}
                        />
                      </TouchableOpacity>
                    </View>
                  )}
                </View>

                {/* <TouchableOpacity
                              style={{
                                  width: 250, borderRadius: 5, backgroundColor: "white",
                                  height: 50, marginLeft: 10, marginRight: 0, marginTop: 0,
                                  justifyContent: "center", alignItems: "center",
                              }}
                              onPress={() => this.reachLocation()}>
                              <Text
                                  style={{
                                      textAlign: "center",
                                      color: "#6FB8EF",
                                      padding: 10,
                                      fontFamily: "Poppins-Medium",

                                  }}>Je suis en attente sur place </Text>
                          </TouchableOpacity> */}
              </View>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          onHardwareBackPress={() => {
            this.setState({ modalVisible3: false });
          }}
          // presentationStyle='fullScreen'
          width={0.9}
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#d3d3d3',
          }}
          visible={this.state.modalVisible3}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: normalize(10, 'height'),
              marginRight: normalize(10, 'height'),
            }}>

            {this.state.modalVisible3 == true ? (
              <View
                style={{
                  width: '95%',
                }}>
                <View
                  style={{
                    width: '100%',
                  }}>
                  <View
                    style={{
                      borderWidth: 0,
                      borderRadius: normalize(15),
                      height: normalize(70),
                      alignSelf: 'flex-start',
                      backgroundColor: '#3fa9f5',
                      width: '63%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {this.state.individual_cat_id == 1 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[0].messages}
                      </Text>
                    ) : null}
                    {this.state.individual_cat_id == 2 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[1].messages}
                      </Text>
                    ) : null}
                    {this.state.individual_cat_id == 3 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[2].messages}
                      </Text>
                    ) : null}
                    {this.state.individual_cat_id == 4 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[3].messages}
                      </Text>
                    ) : null}
                  </View>
                </View>

                <View
                  style={{
                    width: '1%',
                    position: 'absolute',
                  }}
                />

                <View
                  style={{
                    width: '36%',
                    alignSelf: 'flex-end',
                    position: 'absolute',
                    bottom: normalize(0),
                    backgroundColor: 'transparent',
                  }}>
                  <Image
                    source={speechbubble}
                    resizeMode="cover"
                    style={{ width: normalize(123), height: normalize(158) }}
                  />
                </View>
              </View>
            ) : null}



            <View
              width={0.9}
              style={{
                width: '95%',
                justifyContent: 'center',
                padding: 5,
                marginTop: 5,
                alignSelf: 'center',
                backgroundColor: '#01a0f3',
                borderRadius: 15,

                alignItems: 'center',
              }}>



              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  top: 10,
                  alignSelf: 'center',

                  width: '100%',
                }}>
                <Text
                  style={{
                    fontFamily: 'Poppins-Medium',
                    color: 'white',

                    fontSize: 33,
                    textTransform: 'uppercase',
                  }}>
                  {this.state.markerdialog_title}
                </Text>

                <TouchableOpacity
                  style={{
                    width: '10%',
                    position: 'absolute',
                    top: 5,
                    right: 5,
                  }}
                  onPress={() => this.hideModal3()}>
                  <Image
                    source={require('../assets/cross_icon.png')}
                    style={{
                      width: 25,
                      height: 25,
                      marginRight: 0,
                    }}
                  />
                </TouchableOpacity>
              </View>



              <View
                style={{
                  width: '95%',
                  justifyContent: 'center',
                  padding: 5,
                  marginTop: 5,
                  alignSelf: 'center',
                  backgroundColor: '#01a0f3',
                  borderRadius: 15,

                  alignItems: 'center',
                }}>

                <Text
                  style={{
                    textAlign: 'center',
                    fontFamily: 'Poppins-Regular',
                    fontStyle: 'italic',

                    paddingTop: normalize(3, 'height'),
                    color: 'white',
                  }}> {this.state.professional_address}</Text>


                <View
                  style={{
                    width: '100%',
                    height: normalize(95, 'height'),
                    marginBottom: normalize(15, 'height'),

                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      borderRadius: normalize(40, 'height'),
                      width: normalize(75, 'height'),
                      height: normalize(75, 'height'),

                      backgroundColor: 'white',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={() => this.callNumber2()}>
                    <Image
                      source={require('../assets/call_new_icon.png')}
                      style={{
                        width: normalize(65, 'height'),
                        height: normalize(65, 'height'),
                      }}
                    />
                    {/* <Text style={{ color: 'white', textAlign: 'center', fontWeight: 'bold', }}>CALL</Text> */}
                  </TouchableOpacity>

                  {this.state.individual_cat_id == 3
                    //  || this.state.individual_cat_id == 4 || 
                    //   this.state.individual_cat_id == 1 || 
                    //   this.state.individual_cat_id == 2 
                    ? (
                      <TouchableOpacity
                        style={{
                          borderRadius: normalize(40, 'height'),
                          width: normalize(75, 'height'),
                          height: normalize(75, 'height'),
                          //   backgroundColor: 'white',
                          top: normalize(35, 'height'),
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                      // onPress={() =>
                      //   this.callChatScreen(this.state.appointment_id)
                      // }
                      >
                        <Image
                          //  source={require('../assets/chat_new_icon.png')}
                          style={{
                            width: normalize(65, 'height'),
                            height: normalize(65, 'height')
                          }}
                        />
                        {/* <Text style={{ color: 'white', textAlign: 'center', fontWeight: 'bold', }}>CHAT</Text> */}
                      </TouchableOpacity>
                    ) : (
                      <TouchableOpacity
                        style={{
                          borderRadius: normalize(40, 'height'),
                          width: normalize(75, 'height'),
                          height: normalize(75, 'height'),

                          //  backgroundColor: 'white',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                      //   onPress={() => this.callChatScreen(this.state.hospital_Id)}
                      >
                        <Image
                          //    source={require('../assets/chat_new_icon.png')}
                          style={{
                            width: normalize(65, 'height'),
                            height: normalize(65, 'height')
                          }}
                        />
                        {/* <Text style={{ color: 'white', textAlign: 'center', fontWeight: 'bold', }}>CHAT</Text> */}
                      </TouchableOpacity>
                    )}

                  <TouchableOpacity
                    style={{
                      borderRadius: normalize(40, 'height'),
                      width: normalize(75, 'height'),
                      height: normalize(75, 'height'),

                      backgroundColor: 'white',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={() => this.reachLocation()}>
                    <Image
                      source={require('../assets/go_icon.png')}
                      style={{
                        width: normalize(55, 'height'),
                        height: normalize(55, 'height')
                      }}
                    />
                    {/* <Text style={{ color: 'white', textAlign: 'center', fontWeight: 'bold', }}>GO</Text> */}
                  </TouchableOpacity>
                </View>


              </View>
            </View>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          onHardwareBackPress={() => {
            this.setState({ modalVisible2: false });
          }}
          width={0.9}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent',
          }}
          visible={this.state.modalVisible2}
        >
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: normalize(10, 'height'),
              marginRight: normalize(10, 'height'),
            }}>

            {this.state.modalVisible2 == true ? (
              <View
                style={{
                  width: '95%',
                }}>
                <View
                  style={{
                    width: '100%',
                  }}>
                  <View
                    style={{
                      borderWidth: 0,
                      borderRadius: normalize(15),
                      height: normalize(70),
                      alignSelf: 'flex-start',
                      backgroundColor: '#3fa9f5',
                      width: '63%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {this.state.individual_cat_id == 1 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[0].messages}
                      </Text>
                    ) : null}
                    {this.state.individual_cat_id == 2 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[1].messages}
                      </Text>
                    ) : null}
                    {this.state.individual_cat_id == 3 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[2].messages}
                      </Text>
                    ) : null}
                    {this.state.individual_cat_id == 4 ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: 'white',
                          fontFamily: 'Montserrat-Regular',
                        }}>
                        {this.state.appMessage[3].messages}
                      </Text>
                    ) : null}
                  </View>
                </View>

                <View
                  style={{
                    width: '1%',
                    position: 'absolute',
                  }}
                />

                <View
                  style={{
                    width: '36%',
                    alignSelf: 'flex-end',
                    position: 'absolute',
                    bottom: normalize(0),
                    backgroundColor: 'transparent',
                  }}>
                  <Image
                    source={speechbubble}
                    resizeMode="cover"
                    style={{ width: normalize(123), height: normalize(158) }}
                  />
                </View>
              </View>
            ) : null}



            <View
              width={0.9}
              style={{
                width: '95%',
                justifyContent: 'center',
                padding: 5,
                marginTop: 2,
                alignSelf: 'center',
                backgroundColor: '#01a0f3',
                borderRadius: 15,

                alignItems: 'center',

              }}>


              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  top: 5,
                  alignSelf: 'center',

                  width: '100%',
                }}>
                <Text
                  style={{
                    fontFamily: 'Poppins-Medium',
                    color: 'white',

                    fontSize: 33,
                    textTransform: 'uppercase',
                  }}>
                  {this.state.markerdialog_title}
                </Text>

                <TouchableOpacity
                  style={{
                    width: '10%',
                    position: 'absolute',
                    top: 5,
                    right: 5,
                  }}
                  onPress={() => this.hideModal2()}>
                  <Image
                    source={require('../assets/cross_icon.png')}
                    style={{
                      width: 25,
                      height: 25,
                      marginRight: 0,
                    }}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: '95%',
                  justifyContent: 'center',
                  padding: 5,
                  marginTop: 5,
                  alignSelf: 'center',
                  backgroundColor: '#01a0f3',
                  borderRadius: 15,

                }}>
                <RadioForm
                  radio_props={radiogroup_options_slection}
                  buttonSize={12}
                  formHorizontal={true}
                  initial={-1}
                  onPress={value => {
                    console.log("Value Selectio is ", value)
                    this.setState({
                      value: value,


                      slection: value,
                      slectionErr: '',
                    });
                  }}

                  buttonStyle={{ padding: 7, }}
                  labelStyle={{ paddingLeft: 7, paddingRight: 7, }}
                  buttonColor={'black'}
                  selectedButtonColor={'black'}

                />
                {!!this.state.slectionErr && (
                  <Text
                    style={{
                      color: 'red',
                      marginLeft: 10,
                      marginTop: -5,
                      fontSize: 12,
                    }}>
                    {this.state.slectionErr}
                  </Text>
                )}

                <RadioForm
                  radio_props={radiogroup_options}
                  buttonSize={12}
                  formHorizontal={true}
                  initial={-1}

                  onPress={value => {
                    this.setState({
                      value2: value,
                      gender: value,
                      genderErr: '',
                    });
                  }}
                  style={{ marginTop: 5, }}
                  buttonStyle={{ padding: 7, }}
                  labelStyle={{ paddingLeft: 7, paddingRight: 7, }}
                  buttonColor={'black'}
                  selectedButtonColor={'black'}
                />

                {!!this.state.genderErr && (
                  <Text
                    style={{
                      color: 'red',
                      marginLeft: 10,
                      marginTop: -5,
                      fontSize: 12,
                    }}>
                    {this.state.genderErr}
                  </Text>
                )}
                <TextInput
                  value={this.state.msg}
                  style={{
                    fontFamily: 'Poppins-Regular',
                    padding: 10,
                    height: 40,
                    marginTop: 5,
                    color: 'white',

                  }}
                  editable
                  maxLength={170}
                  underlineColorAndroid="white"
                  placeholder="Entrez vos messages"
                  onChangeText={msg => this.setState({ msg, msgErr: '' })}
                />
                {!!this.state.msgErr && (
                  <Text
                    style={{
                      color: 'red',
                      marginLeft: 10,
                      marginTop: -5,
                      fontSize: 12,
                    }}>
                    {this.state.msgErr}
                  </Text>
                )}

                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                  }}>
                  <TouchableOpacity
                    style={{
                      width: 250,
                      borderRadius: 5,
                      backgroundColor: 'white',
                      height: 35,
                      marginLeft: 10,
                      marginRight: 0,
                      marginTop: 5,
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}
                    onPress={() => this.validation()}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#6FB8EF',
                        fontFamily: 'Poppins-Medium',
                        padding: 13.5,
                      }}>{Application_Constant.send_request}</Text>

                    <Image
                      source={require('../assets/right-tick_icon.png')}
                      style={{ width: 25, height: 25 }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          onHardwareBackPress={() => {
            this.setState({ modalVisible4: false });
          }}
          // presentationStyle='fullScreen'
          width={0.9}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent',
          }}
          visible={this.state.modalVisible4}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <DialogContent>
              <Image
                source={require('../assets/box.png')}
                width={0.9}
                style={{
                  width: '94%',
                  justifyContent: 'center',
                  height: 250,
                }}>
                <Text
                  style={{
                    color: 'black',
                    fontFamily: 'Poppins-Medium',
                    paddingLeft: 20,
                    paddingRight: 20,
                    marginTop: 20,
                  }}>{Application_Constant.alredy_sent_req}</Text>

                <Text
                  style={{
                    color: '#ff8a40',
                    fontFamily: 'Poppins-SemiBold',
                    marginTop: 10,
                    paddingLeft: 20,
                    paddingRight: 20,
                  }}>
                  {' '}
                  {Application_Constant.waiting}
                </Text>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                  }}>
                  <TouchableOpacity
                    style={{
                      width: 250,
                      borderRadius: 5,
                      backgroundColor: 'white',
                      height: 50,
                      marginLeft: 10,
                      marginRight: 0,
                      marginTop: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}
                    onPress={() => {
                      this.setState({ modalVisible4: false });
                    }}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#6FB8EF',
                        fontFamily: 'Poppins-Medium',
                        padding: 15,
                      }}>
                      {Application_Constant.ok}
                    </Text>
                  </TouchableOpacity>
                </View>
              </Image>
            </DialogContent>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          onHardwareBackPress={() => {
            this.setState({ modalVisible5: false });
          }}
          // presentationStyle='fullScreen'
          width={0.9}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent',
          }}
          visible={this.state.modalVisible5}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <DialogContent>
              <View
                // source={require('../assets/list_bg_time.png')}
                width={0.9}
                style={{
                  width: '94%',
                  justifyContent: 'center',
                  height: normalize(510, 'height'),
                }}>
                <View
                  style={{
                    flex: 1,
                    //flexDirection: "row",
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingTop: normalize(10, 'height'),
                    paddingBottom: normalize(10, 'height'),
                    marginLeft: normalize(5, 'height'),
                    marginRight: normalize(5, 'height'),
                    backgroundColor: '#4092f3',
                    borderRadius: normalize(14, 'height'),
                    // marginTop: 20,
                    // backgroundColor:
                    //PlatformColor:"transform"
                    // backgroundColor:colors.backgroundColor.gray,
                  }}>
                  <View
                    style={{
                      height: normalize(450, 'height'),
                      width: '90%',
                      backgroundColor: '#40f4ea',
                      left: normalize(4, 'height'),
                      right: normalize(4, 'height'),
                      borderRadius: normalize(14, 'height'),
                    }}>
                    <FlatList
                      data={this.state.rdvData}
                      numColumns={4}
                      style={{
                        margin: normalize(7, 'height'),
                      }}
                      renderItem={({ item, index }) => (
                        <View>
                          {item.availibility == 1 ? (
                            <TouchableOpacity
                              style={{
                                width: normalize(70, 'height'),
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: normalize(30, 'height'),
                                zIndex: normalize(5, 'height'),

                                borderWidth: 1,

                                // Set border color.
                                borderColor: 'white',
                              }}
                              onPress={
                                () =>
                                  this.checkValueforSelected(
                                    item.timeslot,
                                    item.availibility,
                                    index,
                                  )
                                // this.setState({
                                //     rdvTime: item,
                                //     selected_index: index,

                                // })
                              }>
                              {item.timeslot == this.state.rdvTime ? (
                                <Text
                                  style={{
                                    color: '#4092f3',
                                    fontSize: 13,
                                    fontFamily: 'Montserrat-Bold',
                                    padding: normalize(3, 'height'),
                                  }}>
                                  {item.text}
                                </Text>
                              ) : (
                                <Text
                                  style={{
                                    color: '#4092f3',
                                    fontSize: 13,
                                    fontFamily: 'Montserrat-Regular',
                                    padding: normalize(3, 'height'),
                                  }}>
                                  {item.text}
                                </Text>
                              )}
                            </TouchableOpacity>
                          ) : (
                            <TouchableOpacity
                              style={{
                                width: normalize(70, 'height'),
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: normalize(30, 'height'),
                                zIndex: normalize(5, 'height'),

                                borderWidth: 1,

                                // Set border color.
                                borderColor: 'white',
                              }}
                              onPress={
                                () =>
                                  this.checkValueforSelected(
                                    item.timeslot,
                                    item.availibility,
                                    index,
                                  )
                                // this.setState({
                                //     rdvTime: item,
                                //     selected_index: index,

                                // })
                              }>
                              <Text
                                style={{
                                  fontSize: 12.5,
                                  fontFamily: 'Montserrat-Regular',
                                  color: '#696969',
                                  padding: normalize(3),
                                }}>
                                {item.text}
                              </Text>
                            </TouchableOpacity>
                          )}

                          {/* {this.state.selected_index == index ?
                                              // <TouchableOpacity style={{ padding: 7, }}
                                              //     onPress={() => this.setState({ rdvTime: item })}
                                              // >

                                              <Text style={{ color: "#000", fontSize: 15, fontFamily: "Poppins-Regular", fontWeight: 'bold', padding: 7, }}>{item}</Text>

                                              //       </TouchableOpacity>

                                              : */}
                        </View>
                      )}
                    />

                    {!!this.state.rdvTimeErr && (
                      <Text
                        style={{
                          color: 'red',
                          marginLeft: normalize(10, 'height'),
                          marginTop: normalize(-5, 'height'),
                          fontSize: normalize(12, 'height'),
                        }}>
                        {this.state.rdvTimeErr}
                      </Text>
                    )}
                  </View>
                  {/* <View style={{ flex: 1, }}>
                                  <FlatList

                                      style={{
                                          flex: 1,
                                          backgroundColor: '#45d5e6'
                                      }}
                                      scrollEnabled
                                      numColumns={4}
                                      data={this.state.subCatdata}
                                      renderItem={({ item }) => (




                                          <View
                                              style={{ marginTop: 5, }}>
                                              <Text style={{ fontFamily: "Poppins-Medium", marginTop: height * 25 / 100, textAlign: 'center', color: 'grey' }}>{item}</Text>





                                          </View>
                                      )}
                                  />
                              </View> */}

                  <View
                    style={{
                      width: '100%',
                      height: normalize(25, 'height'),
                      flexDirection: 'row',
                      marginLeft: normalize(10, 'height'),
                      marginRight: normalize(10, 'height'),
                      backgroundColor: '#4092f3',
                      marginTop: normalize(5, 'height'),
                    }}>
                    <TouchableOpacity
                      style={{
                        width: '30%',
                        height: normalize(25, 'height'),
                        marginLeft: normalize(5, 'height'),
                        marginRight: normalize(5, 'height'),
                        alignSelf: 'flex-start',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      onPress={() => this.hideModal5()}>
                      <Image
                        style={{
                          width: normalize(20, 'height'),
                          height: normalize(20, 'height'),
                        }}
                        source={require('../assets/time_back.png')}
                      />
                    </TouchableOpacity>

                    <View style={{ width: '40%' }} />
                    <TouchableOpacity
                      style={{
                        width: '30%',
                        height: normalize(25, 'height'),
                        marginLeft: normalize(5, 'height'),
                        marginRight: normalize(5, 'height'),
                        alignSelf: 'flex-end',
                        alignItems: 'center',
                      }}
                      onPress={() => this.timeValidation()}>
                      <Image
                        style={{
                          width: normalize(20, 'height'),
                          height: normalize(20, 'height'),
                        }}
                        source={require('../assets/check.png')}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </DialogContent>
          </View>
        </Modal>


        <Modal
          animationType="slide"
          transparent={true}
          onHardwareBackPress={() => {
            this.setState({ opn_map_dlg1: false });
          }}
          width={0.9}
          style={{
            flex: 1,
            marginTop: normalize(10, 'height'),
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'transparent',
          }}
          visible={this.state.opn_map_dlg1}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',

            }}>
            <DialogContent>
              <View
                width={0.9}
                style={{
                  width: '94%',
                  justifyContent: 'center',

                }}>
                <View
                  style={{
                    width: '95%',
                    justifyContent: 'center',
                    padding: 15,
                    marginTop: 2,
                    alignSelf: 'center',
                    backgroundColor: '#4092f3',
                    borderRadius: 15,

                    alignItems: 'center',

                  }}
                >


                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      top: 5,
                      alignSelf: 'center',
                      height: normalize(25, 'height'),
                      width: '100%',
                    }}>
                    <Text
                      style={{
                        width: '90%',
                        fontFamily: 'Poppins-Medium',
                        color: 'white',

                        fontSize: 33,
                        textTransform: 'uppercase',
                      }}>
                    </Text>

                    <TouchableOpacity
                      style={{
                        width: '10%',
                        position: 'absolute',
                        top: 5,
                        right: 2,
                      }}
                      onPress={() => {
                        this.setState({ opn_map_dlg1: false })
                      }
                      }>
                      <Image
                        source={require('../assets/cross_icon.png')}
                        style={{
                          width: 25,
                          height: 25,
                          marginRight: 0,
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={{
                    width: '100%',
                    flexDirection: 'row',
                   
                  }}>
                    <View
                      style={{
                        width: '50%',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                    >
                      <TouchableOpacity
                        style={{
                          width: normalize(75, 'height'),
                          height: normalize(75, 'height'),
                          backgroundColor: 'white',
                          borderRadius: normalize(40, 'height'),
                          justifyContent:'center',
                          alignItems: 'center',
                        }}
                        onPress={() => {
                          Clipboard.setString(this.state.professional_address)
                          alert('Copie de l\'adresse réussie')

                        }}
                      >
                        <Image
                          source={require('../assets/copy_address.png')}
                          style={{
                            width: normalize(45, 'height'),
                            height: normalize(45, 'height'),
                          }}
                        />


                      </TouchableOpacity>
                      <Text
                        style={{
                          fontFamily: 'Poppins-Medium',
                          color: 'white',
                          marginTop: normalize(10),
                          fontSize: 15,
                        }}>{Application_Constant.copy_address}</Text>
                    </View>



                    <View style={{
                      width: '50%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                      <TouchableOpacity
                        style={{
                          width: normalize(75, 'height'),
                          height: normalize(75, 'height'),
                          backgroundColor: 'white',
                          borderRadius: normalize(40, 'height'),
                          justifyContent:'center',
                          alignItems: 'center',

                        }}
                        onPress={() => this.setState({
                          opn_map_dlg1: false,
                          opn_map_dlg2: true
                        })}
                      >
                        <Image
                          source={require('../assets/open_apps_list.png')}
                          style={{
                            width: normalize(55, 'height'),
                            height: normalize(55, 'height'),
                          }}
                        />


                      </TouchableOpacity>
                      <Text
                        style={{
                          fontFamily: 'Poppins-Medium',
                          color: 'white',
                          marginTop: normalize(10),
                          fontSize: 15,
                        }}
                        numberOfLines={1}
                        >{Application_Constant.open_address_on_apps}</Text>
                    </View>
                  </View>



                </View>
              </View>
            </DialogContent>
          </View>
        </Modal>


        <Popup
          isVisible={this.state.opn_map_dlg2}
          onCancelPressed={() => this.setState({ opn_map_dlg2: false })}
          onAppPressed={() => this.setState({ opn_map_dlg2: false })}
          onBackButtonPressed={() => this.setState({ opn_map_dlg2: false })}
          modalProps={{ // you can put all react-native-modal props inside.
            animationIn: 'slideInUp'
          }}


          options={{

            // showLocation({
            latitude: this.state.currentlatitude,
            longitude: this.state.currentlongitude,
            sourceLatitude: this.state.specialist_latitude,  // optionally specify starting location for directions
            sourceLongitude: this.state.specialist_longitude,  // not optional if sourceLatitude is specified
            dialogTitle: Application_Constant.open_in_map, // optional (default: 'Open in Maps')
            dialogMessage:Application_Constant.what_need_to_use_for_map, // optional (default: 'What app would you like to use?')
            cancelText: Application_Constant.cancel // optional (default: 'Cancel')

          }}
          style={{ /* Optional: you can override default style by passing your values. */ }}
        />
      </View>
    );
  }
}
