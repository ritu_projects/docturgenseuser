import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Button,
  Profile,
  FlatList,
  MenuItem,
  ScrollView,
  DrawerLayoutAndroid,
  Dimensions,
  TouchableHighlight,
  AsyncStorage,
  ActivityIndicator,
  Linking,
} from 'react-native';
import styles from '../styles/styles_profile';
import {Card} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import PopupDialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import axios from 'axios';
import Application_Constant from './localization';
import Image from 'react-native-fast-image';

export default class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: 'johndoe@gmail.com',
      telephone: '+33 987654321',
      username: 'John Doe',
      shortname: 'JD',
      animating: false,
      lodingDialog: false,
      animating2: false,

      phonenumber: '+33 998877667',
      userId: '',
      token: '',
      profilePic: '',
      in_charge_doctor: '',
    };

    //   this.getProfile = this.getProfile.bind(this);
  }
  callNumber = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };

  componentDidMount() {
    this.getProfile();

    const {navigation} = this.props;
    navigation.addListener ('willFocus', () =>{
      console.log("willFocus ","willFocus ")
    // do whatever you want to do when focused
    this.getProfile();

  });
  }


  
  // componentWillReceiveProps(nextProps)
  // {
  //   console.log("componentWillReceiveProps ",nextProps)
  //   this.getProfile();
  // }
  getProfile() {
    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);

      var useridd = JSON.parse(userid);
      console.log('User ID^^^' + useridd);

      this.setState({
        userId: useridd,
      });
    });
    AsyncStorage.getItem('token').then(token => {
      var tokenn = JSON.parse(token);
      console.log('User Token^^^' + tokenn);

      this.setState({
        token: tokenn,
      });

      console.log('User Token%%%' + this.state.token);
  
      axios
        .post(
          Application_Constant.BaseUrl + 'api/getProfile',
          {
            user_id: this.state.userId,
          },
          {
            headers: {
              'user-id': this.state.userId,
              token: this.state.token,
              'Content-Type': 'application/json',

              //other header fields
            },
          },
        )
        .then(response => response.data)
        .then(data => {
          console.log(
            'Data GetProfile inside Profile is  ' + JSON.stringify(data),
          );
          var res_data = data.error;
          console.log('Data Res is  ' + res_data);

          console.log(res_data);
          if (res_data == 'true') {
            var res_data_msg = data.errorMessage;
            console.log('Response True is  ' + res_data_msg);

            this.setState({
              animating: false,
              lodingDialog: false,
            });
            alert(res_data_msg);
          } else if (res_data == 'false') {
            var usersDetailss = JSON.stringify(data.usersDetails);
            var usersfullname = data.usersDetails.fullname;
            var userstelephone = data.usersDetails.telephone;
            var usersemail = data.usersDetails.email;
            var usersfullimg = data.usersDetails.fullimage;

            // let fullimagee = { uri: data.usersDetails.fullimage };
            var in_charge_doctor = data.usersDetails.in_charge_doctor;

            this.setState({
              animating: false,
              lodingDialog: false,
              username: usersfullname,
              phonenumber: userstelephone,
              email: usersemail,
              profilePic: usersfullimg,
              in_charge_doctor: in_charge_doctor,
            });
          }
        });
    });
  }
  imageLoadingStart(val) {
    this.setState({
      animating2: val,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View>
            <Image
              source={require('../assets/box_profile_icon.png')}
              style={{
                width: '100%',
                backgroundColor: 'white',
                height: 300,
                flexDirection: 'row',
                justifyContent: 'center',
              }}
              resizeMode="stretch">
              {!!this.state.profilePic &&
                this.state.profilePic !=
                  'http://docturgences.com/public/upload/usersprofile/' &&
                this.state.profilePic !=
                  'http://docturgences.com/public/upload/usersprofile/null' && (
                  <Image
                    style={{
                      width: 158,
                      height: 158,
                      borderRadius: 60,
                      borderWidth: 0,
                      borderColor: 'white',
                      marginBottom: 40,
                      marginTop: 140,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    source={{
                      uri: this.state.profilePic,
                      priority: Image.priority.high,
                    }}
                    onLoadStart={e => {
                      console.log('Loading Start');

                      this.imageLoadingStart(true);
                    }}
                    onProgress={e =>
                      console.log(
                        'Loading Progress ' +
                          e.nativeEvent.loaded / e.nativeEvent.total,
                      )
                    }
                    onLoad={e =>
                      console.log(
                        'Loading Loaded ' + e.nativeEvent.width,
                        e.nativeEvent.height,
                      )
                    }
                    onLoadEnd={e => {
                      console.log('Loading Ended');
                      this.imageLoadingStart(false);
                    }}>
                    <ActivityIndicator
                      animating={this.state.animating2}
                      visible={this.state.animating2}
                      style={[{width: 30, height: 30}]}
                      color="#C00"
                      size="small"
                      hidesWhenStopped={true}
                    />
                  </Image>
                )}
              {this.state.profilePic ==
                'http://docturgences.com/public/upload/usersprofile/' && (
                <Image
                  style={{
                    width: 158,
                    height: 158,
                    borderRadius: 60,
                    borderWidth: 0,
                    borderColor: 'white',
                    marginBottom: 40,
                    marginTop: 140,
                  }}
                  // source={{ uri: 'https://bootdey.com/img/Content/avatar/avatar6.png' }}
                  // source={require('../assets/profile_picture.png')}
                  source={require('../assets/profile_picture.png')}
                />
              )}
            </Image>

            <View
              style={{
                marginTop: 40,
                flexDirection: 'column',
              }}>
              <Text
                style={{
                  width: '100%',
                  textAlign: 'center',
                  fontSize: 16,
                  paddingTop: 5,
                  fontFamily: 'Poppins-Medium',
                }}>
                {this.state.username}
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 13,
                  paddingBottom: 5,
                  color: 'grey',

                  fontFamily: 'Poppins-Regular',
                }}>
                {this.state.phonenumber}
              </Text>

              <Card
                style={{
                  marginLeft: 20,
                  marginRight: 20,
                  paddingLeft: 20,
                  paddingTop: 20,
                  borderRadius: 20,
                  paddingBottom: 20,
                }}>
                <Text
                  style={{
                    fontSize: 12,
                    color: 'grey',

                    fontFamily: 'Poppins-Regular',
                  }}>
                  {Application_Constant.AddresssMail}
                </Text>

                <Text
                  style={{
                    fontFamily: 'Poppins-Regular',
                    fontSize: 15,
                    paddingTop: 4,
                  }}>
                  {this.state.email}
                </Text>

                <Text
                  style={{
                    fontSize: 12,
                    marginTop: 20,
                    color: 'grey',
                    fontFamily: 'Poppins-Regular',
                  }}>
                  {Application_Constant.Telephone}
                </Text>

                <Text
                  style={{
                    fontFamily: 'Poppins-Regular',
                    fontSize: 15,
                    paddingTop: 4,
                  }}>
                  {this.state.phonenumber}
                </Text>

                <Text
                  style={{
                    fontSize: 13,
                    fontFamily: 'Poppins-Medium',
                  }}>
                  {Application_Constant.mon_medecin_traitant}
                </Text>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    marginTop: 10,
                  }}>
                  <TouchableHighlight
                    style={{
                      borderRadius:
                        Math.round(
                          Dimensions.get('window').width +
                            Dimensions.get('window').height,
                        ) / 2,
                      width: 30,
                      height: 30,
                      backgroundColor: '#d3d3d3',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    underlayColor="#ccc"
                    onPress={() => alert(this.state.shortname)}>
                    <Text
                      style={{
                        fontSize: 12,
                        fontFamily: 'Poppins-Regular',
                        color: 'gray',
                      }}>
                      {this.state.shortname}
                    </Text>
                  </TouchableHighlight>
                  <Text
                    style={{
                      fontFamily: 'Poppins-Regular',
                      fontSize: 13,
                      paddingLeft: 11,
                      textAlign: 'center',
                      paddingTop: 5,
                    }}>
                    {this.state.in_charge_doctor}
                  </Text>
                </View>
              </Card>

              <Card
                style={{
                  marginLeft: 20,
                  marginRight: 20,
                  paddingLeft: 20,
                  paddingTop: 20,
                  borderRadius: 20,
                  paddingBottom: 20,
                  marginBottom: 20,
                  marginTop: 20,
                  flexDirection: 'column',
                }}>
                <LinearGradient
                  style={{
                    borderRadius: 15,
                    marginLeft: 10,
                    padding: 15,
                    marginTop: 20,
                    marginRight: 10,
                  }}
                  colors={['#7CB5F8', '#4C94E6']}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('EditProfile')
                    }
                    style={{
                      flexDirection: 'row',
                      marginRight: 5,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {/* <Image style={{ width: 20, height: 20, }}
                                            source={require('../assets/plus_icon.png')} ></Image> */}

                    <Text
                      style={{
                        fontFamily: 'Poppins-Bold',
                        paddingLeft: 12,
                        color: 'white',
                        fontSize: 14,
                        paddingRight: 5,
                        textAlign: 'center',
                      }}>
                      {Application_Constant.update}
                    </Text>
                  </TouchableOpacity>
                </LinearGradient>
              </Card>
            </View>
          </View>
        </ScrollView>

        {/* <TouchableOpacity
                    style={{
                        bottom: 0,
                        height: 70,
                    }}
                    onPress={this.callNumber}
                >
                    <LinearGradient
                        style={{
                            flexDirection: 'row',
                            backgroundColor: 'white',
                            borderTopLeftRadius: 27,
                            borderTopRightRadius: 27,
                            padding: 20,
                            alignItems: "center",

                        }}
                        colors={['#7CB5F8', '#4C94E6']}
                        start={{ x: 0, y: 1 }}
                        end={{ x: 1, y: 1 }}>
                        <TouchableOpacity style={{ width: '100%', flexDirection: 'row', alignItems: "center", marginRight: 5, marginLeft: 5, paddingLeft: 10, }}
                            onPress={this.callNumber}
                        >

                            <Image style={{ width: 40, height: 40, }}
                                source={require('../assets/call_icon.png')} ></Image>
                            <Text style={{ width: "100%", fontFamily: "Poppins-Regular", paddingLeft: 0, marginRight: 5, color: "white", flex: 1, fontSize: 13, paddingRight: 5, marginLeft: 15 }}>Je suis en situation d’urgence </Text>
                        </TouchableOpacity>
                    </LinearGradient>

                </TouchableOpacity> */}
        <PopupDialog
          onHardwareBackPress={() => {
            this.setState({lodingDialog: false});
          }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({slideFrom: 'bottom'})}>
          <DialogContent>
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[
                  {
                    height: 10,
                    marginBottom: 10,
                    marginTop: 30,
                    marginLeft: 20,
                    marginRight: 20,
                  },
                ]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>
      </View>
    );
  }
}
