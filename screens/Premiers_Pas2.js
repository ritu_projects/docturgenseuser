import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Button,
  Profile,
  FlatList,
  AsyncStorage,
  ScrollView,
  Linking,
  Dimensions,
  TouchableHighlight,
} from 'react-native';
import {Card} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import {TextInput} from 'react-native-gesture-handler';
import Colors from '../colors/colors';
import Application_Constant from './localization';
import Image from 'react-native-fast-image';

export default class Premiers_Pas2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phonenumber: '+33 998877667',
      userId: '',
      session: '',
    };
  }

  callNumber = () => {
    // Linking.openURL(this.state.phonenumber);
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + this.state.phonenumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + this.state.phonenumber + '}';
    }

    Linking.openURL(phoneNumber);
  };
  componentDidMount() {
    AsyncStorage.getItem('userid').then(userid => {
      console.log('User ID$$$' + userid);
      var useridd = JSON.parse(userid);
      console.log('User ID@@@' + useridd);

      this.setState({
        userId: userid,
        session: '',
      });
    });
  }

  HomeClick = () => {
    if (this.state.userId == '' || this.state.userId == null) {
      this.props.navigation.navigate('MapExample');
    } else {
      //    this.props.navigation.navigate('AfterLogin');
      this.props.navigation.navigate('AfterLoginNew');
    }
  };
  render() {
    return (
      <ScrollView style={{flex: 1}}>
        <Image
          source={require('../assets/premiers_pas_screen.png')}
          style={{
            flex: 1,
            flexDirection: 'column',
            width: '100%',
            height: '100%',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 350,
            }}>
            <Text
              style={{
                textAlign: 'center',
                color: '#7CB5F8',
                fontFamily: 'Poppins-Medium',
                fontSize: 16,
              }}>
              2.Filtrer par spécialités
            </Text>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'Poppins-Regular',
                marginTop: 10,
                marginLeft: 20,
                marginRight: 20,
                padding: 3,
                fontSize: 15,
                color: 'grey',
              }}>
              Trouvez un médecin proche de vous
            </Text>

            <TouchableOpacity
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 10,
                marginBottom: 20,
              }}
              onPress={() => this.HomeClick()}>
              <LinearGradient
                style={{
                  marginLeft: 20,
                  marginRight: 20,
                  backgroundColor: 'white',
                  borderRadius: 15,
                  padding: 15,
                  marginTop: 20,
                  flexDirection: 'row',
                }}
                colors={['#4C94E6', '#7CB5F8']}>
                <Text
                  style={{
                    width: '100%',
                    fontFamily: 'Poppins-Bold',
                    color: 'white',
                    fontSize: 15,
                    textAlign: 'center',
                  }}>
                  {Application_Constant.next_step}
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </Image>

        <TouchableOpacity
          style={{
            bottom: 0,
            height: 70,
          }}
          onPress={this.callNumber}>
          <LinearGradient
            style={{
              flexDirection: 'row',
              backgroundColor: 'white',
              borderTopLeftRadius: 27,
              borderTopRightRadius: 27,
              padding: 20,
              alignItems: 'center',
            }}
            colors={['#7CB5F8', '#4C94E6']}
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}>
            <TouchableOpacity
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginRight: 5,
                marginLeft: 5,
                paddingLeft: 10,
              }}
              onPress={this.callNumber}>
              <Image
                style={{width: 40, height: 40}}
                source={require('../assets/call_icon.png')}
              />
              <Text
                style={{
                  width: '100%',
                  fontFamily: 'Poppins-Regular',
                  paddingLeft: 0,
                  marginRight: 5,
                  color: 'white',
                  flex: 1,
                  fontSize: 13,
                  paddingRight: 5,
                  marginLeft: 15,
                }}>
                Je suis en situation d’urgence{' '}
              </Text>
            </TouchableOpacity>
          </LinearGradient>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}
