import 'react-native-gesture-handler';
import React from 'react';
import {AppRegistry, Alert, View, AsyncStorage} from 'react-native';
import firebase from 'react-native-firebase';

import AppNavigator from './AppNavigator';
import EditProfile from './screens/EditProfile';

import {EventRegister} from 'react-native-event-listeners';
import PushNotificationIOS from '@react-native-community/push-notification-ios';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      push_val: '',
    };
  }

  async componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners(); //add this line
  }
  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
  }

  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
      this.createNotificationListeners();
    } else {
      this.requestPermission();
    }
  }

  async createNotificationListeners() {

    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {


      const localNotification = new firebase.notifications.Notification({
      //  sound: 'uber',
        
      })
      .setNotificationId(notification.notificationId)
      .setTitle(notification.title)
      .setBody(notification.body)
      .android.setChannelId('notificationchannell') // e.g. the id you chose above
      .android.setSmallIcon('@mipmap/ic_launcher') // create this icon in Android Studio
      .android.setColor('#000000') // you can set a color here
      .android.setPriority(firebase.notifications.Android.Priority.High);

      firebase.notifications()
        .displayNotification(localNotification)
        .catch(err => console.error(err));


        console.log('Notification is###' + notification);

        for (var key in notification) {
          console.log(notification[key]);
        }
        const {title, body} = notification;
        const {fcm_push_response} = notification._data;
        console.log('message  test_key_add::::::' + fcm_push_response);

        if (fcm_push_response == 'specialistSendChtMessagePush') {
          const {specialist_sender_id} = notification._data;
          const {appointment_id} = notification._data;

          console.log(
            'message  test_key_add sender id::::::' + specialist_sender_id,
          );

          console.log(
            'message  test_key_add appointment::::::' + appointment_id,
          );

          this.secondAlrt(specialist_sender_id, appointment_id);
        } else {
          this.showAlert(title, body, fcm_push_response);
        }
      });

      const channel = new firebase.notifications.Android.Channel('notificationchannell', 'notificationchannell', firebase.notifications.Android.Importance.High)
      .setDescription('Demo app description')
     // .setSound('uber.mp3');
    firebase.notifications().android.createChannel(channel); 
  
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        console.log('Notification is Background$$$' + notificationOpen);

        const {title, body} = notificationOpen.notification;
        const {fcm_push_response} = notification._data;
        console.log(
          'message  test_key_add Background::::::' + fcm_push_response,
        );

        if (fcm_push_response == 'specialistSendChtMessagePush') {
          const {specialist_sender_id} = notification._data;
          const {appointment_id} = notification._data;

          this.secondAlrt(specialist_sender_id, appointment_id);
        } else {
          this.showAlert(title, body, fcm_push_response);
        }
      });

    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    console.log('Notification is closed$$$' + notificationOpen);

    if (notificationOpen) {
      console.log('Notification is open$$$' + notificationOpen);
      //  const { title, body } = notificationOpen.notification;
      //  console.log("message  test_key_add open title::::::"+title);

      const {fcm_push_response} = notificationOpen.notification._data;
      console.log(
        'message  test_key_add open response::::::' + fcm_push_response,
      );

      if (fcm_push_response == 'specialistSendChtMessagePush') {
        const {specialist_sender_id} = notificationOpen.notification._data;
        const {appointment_id} = notificationOpen.notification._data;
        console.log(
          'message  test_key_add open senderid::::::' + specialist_sender_id,
        );
        console.log(
          'message  test_key_add open appointmentid::::::' + appointment_id,
        );

        this.secondAlrt(specialist_sender_id, appointment_id);
      } else {
        this.showAlert(title, body, fcm_push_response);
      }
    }

    this.messageListener = firebase.messaging().onMessage(message => {
      console.log('Notification is Triggered$$$' + message);

      console.log(JSON.stringify(message));
    });
  }

  secondAlrt(specialist_sender_id, appointment_id) {
    const details = {
      alertTitle: 'Alerte',
      alertBody: appointment_id,
      soundName: 'uber.wav',
    };
    PushNotificationIOS.presentLocalNotification(details);
    console.log(
      'message  test_key_add dlg senderid::::::' + specialist_sender_id,
    );
    console.log(
      'message  test_key_add dlg appointmentid::::::' + appointment_id,
    );

    const objchatscreens = {
      appointment_id: appointment_id,
      userIdd_receiverId: specialist_sender_id,
    };
    EventRegister.emit('myCustomEvent', objchatscreens);
  }
  showAlert(title, body, fcm_push_response) {
    console.log('message  test_key_add closed::::::' + fcm_push_response);
    const details = {alertTitle: title, alertBody: body, soundName: 'uber.wav'};
    PushNotificationIOS.presentLocalNotification(details);

    if (fcm_push_response == 'hospitalUpdateRequestStatus') {
      Alert.alert(
        title,
        body,
        [
          {
            text: "D'accord",
            onPress: () =>
              this.setState({
                push_val: fcm_push_response,
              }),
          },
        ],
        {cancelable: false},
      );
    } else if (fcm_push_response == 'hospitalUpdateWaitingTime') {
      Alert.alert(
        title,
        body,
        [
          {
            text: "D'accord",
            onPress: () =>
              this.setState({
                push_val: fcm_push_response,
              }),
          },
        ],
        {cancelable: false},
      );
    } else if (fcm_push_response == 'specialistSendChtMessagePush') {
      // Alert.alert(
      //   title, body,
      //   [
      //     { text: "D'accord", onPress: () =>
      //     {

      //     const objchatscreens= {appointment_id: '10',
      //       userIdd_receiverId: '20',
      //     }
      //       EventRegister.emit('myCustomEvent', objchatscreens)
      //     }

      //     }
      //   ],
      //   { cancelable: false },

      // );
      EventRegister.emit('myCustomEvent', objchatscreens);
    }
  }

  //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log('FCM token$$$  ' + fcmToken);
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
      this.createNotificationListeners();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }
  render() {
    // if(this.state.push_val=="hospitalUpdateRequestStatus")
    // {

    //   return <EditProfile/>;

    // }

    // else
    // {
    console.disableYellowBox = true;

    return <AppNavigator />;

    // }
  }
}
