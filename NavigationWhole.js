import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
  Text,
} from 'react-native';
import React, {Component} from 'react';
import {
  createStackNavigator,
  createAppContainer,
  createSwitchNavigator,
  createDrawerNavigator,
} from 'react-navigation';
import AppNavigator from './AppNavigator';
import SideBar from './screens/Sidebar';
//import afterlogin from './screens/AfterLogin';
import afterlogin from './screens/AfterLoginNew';
import {object} from 'prop-types';

import Application_Constant from './screens/localization';
import Image from 'react-native-fast-image';
import normalize from 'react-native-normalize';
import LinearGradient from 'react-native-linear-gradient';

export default class NavigationWhole extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'DoctUrgences',
      visibility: false,
    };
  }
  componentDidMount() {
    // AsyncStorage.getItem("title")
    //   .then(title => {
    //     console.log("Title is###"+title)
    //     this.setState({ title: title });
    //   })
  }
  //Top Navigation Header with Donute Button
  toggleDrawer = () => {
    //Props to open/close the drawer
    this.props.navigationProps.toggleDrawer();
  };

  callLocation() {
    console.log('Toolbar Icon CLick  ' + 'Toolbar Icon Click ');

    //this.props.navigationProps.navigate('AfterLogin');
  }
  render() {
    return (
      <LinearGradient
        style={{
          flexDirection: 'row',
          height: normalize(30),
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: Platform.OS == 'ios' ? normalize(25) : normalize(0),
          marginBottom: normalize(-5),

        }}
        colors={['#e7eef1', 'rgba(203,229,216,255)']}>
        <TouchableOpacity
          style={{width: '10%', flexDirection: 'row', marginRight: 0}}
          onPress={this.toggleDrawer.bind(this)}>
          <Image
            source={require('./assets/drawer.png')}
            style={{width: 30, height: 30, marginRight: 10, marginLeft: 10}}
          />

            </TouchableOpacity>
        <View
          style={{
            width: '85%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',}}
            >

              <Text
            style={{
              textAlign: 'center',
              fontFamily: 'Poppins-Regular',
              color: '#4092f3',
              fontSize: 18,
            }}
            >

{Application_Constant.homeTitle} 
          </Text>
        </View>
      </LinearGradient>
    );
  }
}
